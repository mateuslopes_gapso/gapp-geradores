package WriteCT;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Formatacoes.FormatacaoTable;

public class WriteExcelHistorico {

	public void criaSheetHistorico(XSSFWorkbook workbook, String nomeSistema) {
		// Criar a planilha
		Sheet sheetHistorico = workbook.createSheet("Historico");
		//Variavel para criar as linhas e celulas
		Row row;
		FormatacaoTable formatacao = new FormatacaoTable();

		// Criar Linha e Coluna com a formata��o
		row = sheetHistorico.createRow(0);
		row.setHeight((short)635);//42 pixels, 31,50
		row.createCell(1).setCellValue(nomeSistema);
		row.getCell(1).setCellStyle(formatacao.styleCelulaProjeto(workbook));
		sheetHistorico.addMergedRegion(new CellRangeAddress(0, 0, 1, 4));// mescla celulas
		sheetHistorico.autoSizeColumn(1);//Ajusta largura da coluna ao texto
		
		// Criar Linha e Coluna com a formata��o
		row = sheetHistorico.createRow(1);
		row.createCell(1).setCellValue("Caso de Teste");
		row.getCell(1).setCellStyle(formatacao.styleCelulaCT(workbook));
		sheetHistorico.addMergedRegion(new CellRangeAddress(1, 1, 1, 4));// mescla celulas
		sheetHistorico.autoSizeColumn(1);// Ajusta largura da coluna ao texto

		// h - linha, i - coluna
		for (int h = 4; h <= 11; h++) {
			// Cabe�alho da tabela
			for (int i = 1; i < 5; i++) {
				if (i == 1 && h == 4) {
					// Criar Linha e Coluna com a formata��o
					row = sheetHistorico.createRow(4);
					row.createCell(i).setCellValue("Vers�o");
					row.getCell(i).setCellStyle(formatacao.styleCabecalhoLeftAzulCenter(workbook));
					sheetHistorico.autoSizeColumn(i);// Ajusta largura da coluna ao texto
				}
				if (i == 2 && h == 4) {									
					// Criar Linha e Coluna com a formata��o
					row.createCell(i).setCellValue("data");
					row.getCell(i).setCellStyle(formatacao.styleCabecalhoCentralAzulMedium(workbook));
					sheetHistorico.autoSizeColumn(i);// Ajusta largura da coluna ao texto
				}
				if (i == 3 && h == 4) {
					// Criar Linha e Coluna com a formata��o
					row.createCell(i).setCellValue("Responsavel");
					row.getCell(i).setCellStyle(formatacao.styleCabecalhoCentralAzulMedium(workbook));
					sheetHistorico.autoSizeColumn(i);// Ajusta largura da coluna ao texto
				}
				if (i == 4 && h == 4) {
					// Criar Linha e Coluna com a formata��o
					row.createCell(i).setCellValue("Descri��o");
					row.getCell(i).setCellStyle(formatacao.styleCabecalhoRightAzulCenter(workbook));
					sheetHistorico.autoSizeColumn(i);// Ajusta largura da coluna ao texto
				}
			}
			
			if(h ==5){
				row = sheetHistorico.createRow(h);
				
				row.createCell(1).setCellValue("1.0");
				row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, h));
							
				DateFormat dataFormatada = new SimpleDateFormat("dd/MM/yyyy");
				Calendar data = Calendar.getInstance();
				
				row.createCell(2).setCellValue(dataFormatada.format(data.getTime()));
				row.getCell(2).setCellStyle(formatacao.styleBordaCentral(workbook));
								
				row.createCell(3).setCellValue("Gerador de Casos de Testes");
				row.getCell(3).setCellStyle(formatacao.styleBordaCentral(workbook));

				row.createCell(4).setCellValue("Vers�o Inicial");
				row.getCell(4).setCellStyle(formatacao.styleRightMedium(workbook));
				
			}
			// Constroe mais linhas na tabela controle de vers�o
			if (h > 5) {
				row = sheetHistorico.createRow(h);
				// Coluna da Esquerda
				row.createCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, h));				

				// Colunas Centrais
				row.createCell(2).setCellStyle(formatacao.styleBordaCentral(workbook));
				row.createCell(3).setCellStyle(formatacao.styleBordaCentral(workbook));

				// Coluna da Direita
				row.createCell(4).setCellStyle(formatacao.styleRightMedium(workbook));
			}
			if (h == 11) {
				// Borda inferior				
				for (int f = 1; f <= 4; f++) {
					row.createCell(f).setCellStyle(formatacao.styleBordaInferiorMedium(workbook));
				}
			}
		}
		// Criar Linha e Coluna com a formata��o
		row = sheetHistorico.createRow(12);
		row.createCell(1).setCellValue("Status");
		row.getCell(1).setCellStyle(formatacao.styleCabecalhoLeftAzul(workbook));
		
		// Criar Linha e Coluna com a formata��o
		row.createCell(2).setCellValue("Qtde");
		row.getCell(2).setCellStyle(formatacao.styleCabecalhoRightAzul(workbook));
		
		// Criar Linha e Coluna com a formata��o
		row = sheetHistorico.createRow(13);
		row.createCell(1).setCellValue("Aguardando Teste");
		row.getCell(1).setCellStyle(formatacao.styleRight(workbook, 13));
		sheetHistorico.autoSizeColumn(1);//Ajusta largura da coluna ao texto

		//Cria celula com Qtde de Casos de Testes aguardando para serem executados
		row.createCell(2).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(2).setCellFormula("COUNTIF('Casos de Testes'!G:G,B14)");//Usar virgula(,) no lugar de ;
		row.getCell(2).setCellStyle(formatacao.styleRight(workbook, 13));
		sheetHistorico.autoSizeColumn(2);//Ajusta largura da coluna ao texto

		// Criar Linha e Coluna com a formata��o
		row = sheetHistorico.createRow(14);
		row.createCell(1).setCellValue("OK");
		row.getCell(1).setCellStyle(formatacao.styleRight(workbook, 14));

		//Cria celula com Qtde de Casos de Testes Aprovados
		row.createCell(2).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(2).setCellFormula("COUNTIF('Casos de Testes'!G:G,B15)");//Usar virgula(,) no lugar de ;
		row.getCell(2).setCellStyle(formatacao.styleRight(workbook, 13));
		sheetHistorico.autoSizeColumn(2);//Ajusta largura da coluna ao texto

		row.createCell(4).setCellValue("Quantidade de Tabelas");
		row.getCell(4).setCellStyle(formatacao.styleCabecalhoRightAzul(workbook));
		sheetHistorico.autoSizeColumn(4); //Ajusta a largura da coluna ao texto

		// Criar Linha e Coluna com a formata��o
		row = sheetHistorico.createRow(15);
		row.createCell(1).setCellValue("Falha");
		row.getCell(1).setCellStyle(formatacao.styleCelulaFalha(workbook));
		sheetHistorico.autoSizeColumn(1);//Ajusta largura da coluna ao texto
			
		//Criar celula com Qtde de Casos de Testes com falha
		row.createCell(2).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(2).setCellFormula("COUNTIF('Casos de Testes'!G:G,B16)");//Usar virgula(,) no lugar de ;
		row.getCell(2).setCellStyle(formatacao.styleRight(workbook, 13));
		sheetHistorico.autoSizeColumn(2);//Ajusta largura da coluna ao texto
		
		//Criar celula com Qtde de Tabelas
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("COUNTIF(B:B, \"*Tabela*\")");
		row.getCell(4).setCellStyle(formatacao.styleRight(workbook, 13));
		sheetHistorico.autoSizeColumn(4);//Ajusta largura da coluna ao texto
		
		row = sheetHistorico.createRow(16);
		row.createCell(2).setCellStyle(formatacao.styleBordaInferior(workbook));
		row.createCell(4).setCellStyle(formatacao.styleBordaInferior(workbook));
		
		row = sheetHistorico.createRow(17);
		row.createCell(1).setCellValue("Par�metros");
		row.getCell(1).setCellStyle(formatacao.styleCabecalhoRightAzul(workbook));
		row.createCell(2).setCellStyle(formatacao.styleCabecalhoRightAzul(workbook));
		row.createCell(3).setCellStyle(formatacao.styleCabecalhoRightAzul(workbook));
		sheetHistorico.addMergedRegion(new CellRangeAddress(17, 17, 1, 3));// mescla celulas
		
		row.createCell(4).setCellValue("Valor de SectionCode");
		row.getCell(4).setCellStyle(formatacao.styleCabecalhoRightAzul(workbook));
	}
}
