package WriteCT;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Formatacoes.FormatacaoTable;

public class WriteItensCT {
	FormatacaoTable formatacao = new FormatacaoTable();
	//Enter na celula
	//cell.setCellValue(new HSSFRichTextString("sua string \n tt \n")) ;
	
	public void criarDropDown(XSSFWorkbook workbook, Row row, int linha, int idTable){
		XSSFSheet sheetCT = workbook.getSheet("Casos de Testes");			
				
		//Cria drop down na celula
		row.createCell(6).setCellValue("Aguardando Teste");
		row.getCell(6).setCellStyle(formatacao.styleCentral(workbook, linha));
		sheetCT.autoSizeColumn(6); //Ajusta a largura da coluna ao texto
		DataValidationHelper validationHerper = new XSSFDataValidationHelper(sheetCT);
		CellRangeAddressList addressList = new CellRangeAddressList(linha, linha, 6, 6);
		DataValidationConstraint dvConstraint = validationHerper.createExplicitListConstraint(new String[] { "Aguardando Teste", "Falha", "OK" });
		DataValidation dataValidation = validationHerper.createValidation(dvConstraint, addressList);
		dataValidation.setSuppressDropDownArrow(true);
		sheetCT.addValidationData(dataValidation);
	}
	
	public void writeDadosCT(XSSFWorkbook workbook){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");
		Row row = sheetCT.getRow(1);
		row.createCell(2).setCellFormula("Historico!$B$1");
		row.getCell(2).setCellStyle(formatacao.styleAlinhamentoLeft(workbook));
		sheetCT.addMergedRegion(new CellRangeAddress(1, 1, 2, 3));// mescla celulas
		
		row = sheetCT.getRow(2);
		row.createCell(2).setCellFormula("Historico!$B$3");
		row.getCell(2).setCellStyle(formatacao.styleAlinhamentoLeft(workbook));
		sheetCT.addMergedRegion(new CellRangeAddress(2, 2, 2, 3));// mescla celulas
	}

	public void writeItensCTAcessar(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
				
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
		sheetCT.setColumnWidth(1, 5000);
		
		row.createCell(2).setCellValue("Acessar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
				
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",)");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		sheetCT.setColumnWidth(3, 10000);
				
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\"O usu�rio acessa tabela de \", B"+celula+", \".\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		sheetCT.setColumnWidth(4, 25000);
		
		row.createCell(5).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(5).setCellFormula("CONCATENATE(" +
					"\"Sistema exibe tela do tipo mestre detalhe com a listagem de '\", B"+celula+",\"' cadastradas.\")");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		sheetCT.setColumnWidth(5, 15000);
						
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));		
	}
	
	public void writeItensCTInserirCorreto(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
				
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Inserir");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
				
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\" >> Novo\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
				
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(" +
				"\"O usu�rio aciona o bot�o '\", C"+celula+",\"' na tabela de \", B"+celula+",\" para que o formul�rio de inclus�o seja exibido.\n"+
				"O sistema exibe formul�rio com os campos para inclus�o.\n"+
				"O usu�rio preenche os campos com valores v�lidos e aciona o bot�o 'Salvar'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
				
		row.createCell(5).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(5).setCellFormula("CONCATENATE(\""+
				"Sistema persiste \", B"+celula+",\" criada no banco de dados e exibe a tabela de '\", B"+celula+",\"' atualizada.\")");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
				
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
	}
	
	public void writeItensCTInserirIncorreto(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Inserir");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
				
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\" >> Novo\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio aciona o bot�o '\", C"+celula+",\"' na tabela de \", B"+celula+",\" para que o formul�rio de inclus�o seja exibido.\n"+
				"O sistema exibe formul�rio com os campos para inclus�o.\n"+
				"O usu�rio preenche os campos com valores inv�lidos e/ou deixa os campos em branco e aciona o bot�o 'Salvar'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellValue("Sistema exibe uma mensagem de 'Erro' informando qual tipo de dado que deve ser informado no respectivo campo com erro.");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
	}
	public void writeItensCTEditarCorreto(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Editar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
				
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\" >> Editar\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio seleciona um registro na tabela de \", B"+celula+", \" e aciona o bot�o '\", C"+celula+",\"';\n"+
				"O sistema exibe formul�rio com os campos para edi��o;\n"+
				"O usu�rio edita os campos com valores v�lidos e aciona o bot�o 'Salvar'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(5).setCellFormula("CONCATENATE(\""+
				"Sistema persiste as altera��es realizadas pelo usu�rio na base de dados e exibe a tabela de '\", B"+celula+",\"' atualizada.\")");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
	}
	public void writeItensCTEditarIncorreto(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Editar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\" >> Editar\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio seleciona um registro na tabela de \", B"+celula+", \" e aciona o bot�o '\", C"+celula+",\"';\n"+
				"O sistema exibe formul�rio com os campos para edi��o;\n"+
				"O usu�rio edita os campos com valores inv�lidos e/ou deixa os campos em branco e aciona o bot�o 'Salvar'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellValue("O sistema exibe uma mensagem de 'Erro' informando qual tipo de dado deve ser informado no respectivo campo com erro e n�o persiste a altera��o.");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
	}
	public void writeItensCTEditarCelulaCorreto(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Editar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\" >> Editar (C�lula)\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio seleciona um registro na tabela de \", B"+celula+", \" e efetua um clique sobre cada c�lula para editar;\n"+
				"O sistema exibe os campos no formato de edi��o;\n"+
				"O usu�rio edita os campos com valores v�lidos e aciona o bot�o 'Salvar'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(5).setCellFormula("CONCATENATE(\""+
				"Sistema persiste as altera��es realizadas pelo usu�rio na base de dados e exibe a tabela de '\", B"+celula+",\"' atualizada.\")");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
		
	}
	
	public void writeItensCTEditarCelulaIncorreto(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Editar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\" >> Editar (C�lula)\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio seleciona um registro na tabela de \", B"+celula+", \" e efetua um clique sobre cada c�lula para editar;\n"+
				"O sistema exibe os campos no formato de edi��o;\n"+
				"O usu�rio edita os campos com valores inv�lidos e/ou deixa os campos em branco e aciona o bot�o 'Salvar'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellValue("O sistema exibe uma mensagem de 'Erro' informando qual tipo de dado deve ser informado no respectivo campo com erro, "+
				"n�o persiste no banco a altera��o realizada e volta o campo para o valor anterior.");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
	}
	public void writeItensCTRemover(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Remover");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\"  >> Remover\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio seleciona um registro na tabela de \", B"+celula+", \" e aciona o bot�o '\", C"+celula+", \"';\n"+
				"O sistema exibe mensagem para confirmar a remo��o.\n"+
				"O usu�rio confirma a remo��o acionando o bot�o 'Ok'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(5).setCellFormula("CONCATENATE(\""+
				"Sistema persiste remo��o da \", B"+celula+", \" no banco de dados e exibe a tabela de '\", B"+celula+",\"' atualizada.\")");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
	}
	
	public void writeItensCTFiltrarCorreto(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Filtrar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\"  >> Filtrar\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio aciona o bot�o '\", C"+celula+",\"' na tabela de \", B"+celula+",\".\n"+
				"O sistema exibe formul�rio para configura��o dos filtros.\n"+
				"O usu�rio configura os campos de filtro informando valores v�lidos e aciona o bot�o '\", C"+celula+",\"'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellValue("Sistema aplica o filtro na tabela exibindo apenas as linhas que satisfa�am a configura��o cadastrada pelo usu�rio.");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));		
	}
	public void writeItensCTFiltrarIncorreto(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Filtrar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\"  >> Filtrar\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio aciona o bot�o '\", C"+celula+",\"' na tabela de \", B"+celula+",\".\n"+
				"O sistema exibe formul�rio para configura��o dos filtros.\n"+
				"O usu�rio configura os campos de filtro informando valores inv�lidos e aciona o bot�o '\", C"+celula+",\"'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellValue("Sistema n�o exibe nenhuma linha na tabela com os dados filtrados.");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
	}
	public void writeItensCTFiltrarVazio(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Filtrar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\"  >> Filtrar\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio aciona o bot�o '\", C"+celula+",\"' na tabela de \", B"+celula+",\".\n"+
				"O sistema exibe formul�rio para configura��o dos filtros.\n"+
				"O usu�rio n�o informa nenhum valor para os campos de filtro e aciona o bot�o '\", C"+celula+",\"'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellValue("Sistema exibe na tabela todos os registros cadastrados.");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
	}
	public void writeItensCTLimparFiltro(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Limpar Filtro");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\"  >> Filtrar >> Limpar Filtrar\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio aciona o bot�o '\", C"+(celula - 1)+",\"' na tabela de \", B"+celula+",\" para que o formul�rio de filtros seja exibido.\n"+
				"O sistema exibe formul�rio com os campos de filtro configurados anteriormente.\n"+
				"O usu�rio configura os campos de filtro e aciona o bot�o '\",C"+celula+",\"'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(5).setCellFormula("CONCATENATE(\""+
				"Sistema limpa configura��o de filtro realizada anteriormente pelo usu�rio e passa a exibir todas as \", B"+celula+",\" cadastradas na tabela.\")");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
	}
	public void writeItensCTSalvar(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Salvar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\"  >> Salvar\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\""+
				"O usu�rio realiza edi��o dos dados exibidos na tabela de \", B"+celula+", \" e acionar o bot�o '\", C"+celula+",\"'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(5).setCellFormula("CONCATENATE(\""+
				"Sistema persiste altera��es realizadas pelo usu�rio na base de dados e exibe a tabela de '\", B"+celula+",\"' atualizada.\")");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));
	}
	public void writeItensCTImportarCorreto(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Importar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\"  >> Importar\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\"O usu�rio aciona o bot�o '\", C"+celula+",\"' na tabela de \", B"+celula+",\".\n"+
					"O sistema exibe formu�rio para sele��o da planilha Excel a ser importada.\n"+
					"O usu�rio seleciona um arquivo com dados v�lido a ser importado e aciona o bot�o '\", C"+celula+",\"'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(5).setCellFormula("CONCATENATE(\"Sistema importa a planilha Excel selecionada pelo usu�rio, "+
					"persiste as informa��es na base de dados e exibe a tabela de '\", B"+celula+",\"' atualizada.\")");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));		
	}
	
	public void writeItensCTImportarIncorreto(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Importar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\"  >> Importar\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\"O usu�rio aciona o bot�o '\", C"+celula+",\"' na tabela de \", B"+celula+",\".\n"+
					"O sistema exibe formu�rio para sele��o da planilha Excel a ser importada.\n"+
					"O usu�rio seleciona um arquivo com dados inv�lidos a ser importado e aciona o bot�o '\", C"+celula+",\"'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellValue("Sistema n�o importa o registro inv�lido e exibie mensagem informando qual a coluna com o seu respectivo valor que � inv�lido.");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));		
	}
	public void writeItensCTExportar(XSSFWorkbook workbook, int linha, int idTable){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");			
		Row row;
		int celula = linha + 1;
		
		//Usar virgula(,) no lugar de ; nas celulas de formula
		row = sheetCT.createRow(linha);
		row.setHeight((short)1000);
		
		row.createCell(1).setCellFormula("Historico!$C$"+idTable);
		row.getCell(1).setCellStyle(formatacao.styleLeftMedium(workbook, linha));
				
		row.createCell(2).setCellValue("Exportar");
		row.getCell(2).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(3).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(3).setCellFormula("CONCATENATE(Historico!$C$19, \" >> \", B"+celula+",\"  >> Exportar\")");
		row.getCell(3).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(4).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(4).setCellFormula("CONCATENATE(\"O usu�rio aciona o bot�o '\", C"+celula+",\"' na tabela de '\", B"+celula+",\"'.\")");
		row.getCell(4).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		row.createCell(5).setCellType(HSSFCell.CELL_TYPE_FORMULA);
		row.getCell(5).setCellFormula("CONCATENATE(\"Sistema gera planilha Excel com as informa��es exibidas na tabela '\", B"+celula+",\"'.\")");
		row.getCell(5).setCellStyle(formatacao.styleCentral(workbook, linha));
		
		criarDropDown(workbook, row, linha, idTable);
		
		row.createCell(7).setCellStyle(formatacao.styleRightMedium(workbook));	
	}
	public void writeBordaInferior(XSSFWorkbook workbook, int linha){
		Sheet sheetCT = workbook.getSheet("Casos de Testes");	
		Row row;
		
		row = sheetCT.createRow(linha);
		row.createCell(1).setCellStyle(formatacao.styleBordaInferiorMedium(workbook));
		row.createCell(2).setCellStyle(formatacao.styleBordaInferiorMedium(workbook));
		row.createCell(3).setCellStyle(formatacao.styleBordaInferiorMedium(workbook));
		row.createCell(4).setCellStyle(formatacao.styleBordaInferiorMedium(workbook));
		row.createCell(5).setCellStyle(formatacao.styleBordaInferiorMedium(workbook));
		row.createCell(6).setCellStyle(formatacao.styleBordaInferiorMedium(workbook));
		row.createCell(7).setCellStyle(formatacao.styleBordaInferiorMedium(workbook));
	}
}
