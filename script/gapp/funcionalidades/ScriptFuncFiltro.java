package gapp.funcionalidades;

import gapp.test.main.AbstractScript;

import java.io.File;
import java.util.Formatter;

public class ScriptFuncFiltro extends AbstractScript {

	private static String diretorioArquivoCriado;
	private static String diretorioSalvarArquivos;
	private static Formatter arquivo;
	private static String localCompletoAbstract;
	
	public static void criarFuncionalidadesFiltro (String localArquivo, String nomeSistema) {
		
		criarDiretorios(localArquivo, nomeSistema);

		try {
			
			diretorioSalvarArquivos = diretorioSalvarArquivos.replaceAll("\\s+", "") + "/";
		    diretorioArquivoCriado = diretorioSalvarArquivos.replace("./", "");
			
    	    arquivo = new Formatter(diretorioArquivoCriado + "/FuncionalidadesFiltro.java");
    	    textoDataCriacao(arquivo);
    	    codigoFuncionalidadesFiltro(arquivo, nomeSistema);
    	    arquivo.close();
    	    
    	    localCompletoAbstract = new File(diretorioArquivoCriado).getAbsolutePath();
			logScriptsSucesso += "%nSUCESSO! Arquivo FuncionalidadesFiltro.java criado em " + localCompletoAbstract;
			qtdeScriptSucesso++;
    	    
		} catch (Exception e) {
			logScriptsErro += "%nERRO! N�o foi criado o arquivo \\scr\\FuncionalidadesFiltro.java";
			qtdeScriptErro++;
			e.printStackTrace();
		}
	}
	
	private static void codigoFuncionalidadesFiltro (Formatter f, String nomeSistema) {
		
		f.format("\npackage "+nomeSistema+";");

		f.format("\n\n	import static org.junit.Assert.assertTrue;");

		f.format("\n\nimport org.openqa.selenium.By;");
		f.format("\nimport org.openqa.selenium.WebDriver;");
		f.format("\nimport org.openqa.selenium.support.ui.ExpectedConditions;");
		f.format("\nimport org.openqa.selenium.support.ui.WebDriverWait;");
		f.format("\nimport org.openqa.selenium.support.ui.Select;");

		f.format("\n\npublic class FuncionalidadesFiltro {");

		f.format("\n\n		private WebDriver driver = null;");
		f.format("\n		public FuncionalidadesFiltro (WebDriver driver) {"
				+ "\n			this.driver = driver;"
				+ "\n		}\n");
			
		f.format("\n		private void abrirModalFiltroMestre (String sectionTabelaMestre, String idColuna) {"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n\n			//TODO para avan�ar na tabela. Evelyn"
				+ "\n			driver.findElement(By.xpath(\".//*[@aria-describedby='\"+sectionTabelaMestre+\"_grid_\"+idColuna+\"']\")).click();"
				+ "\n			driver.findElement(By.xpath(\".//*[@id='\"+sectionTabelaMestre+\"_grid_\"+idColuna+\"']/a\")).click();"
				+ "\n			util.aguardarElementoXPath(\".//*/div[contains (@class, 'filter-modal')]\");"
				+ "\n		}\n");
			
		f.format("\n		private void abrirModalFiltroDetalhe (String sectionTabelaMestre, String sectionTabelaDetalhe, String idColuna) {"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n\n			//TODO para avan�ar na tabela. Evelyn"
				+ "\n			driver.findElement(By.xpath(\".//*[@aria-describedby='details_\"+sectionTabelaMestre+\"_\"+sectionTabelaDetalhe+\"_grid_\"+idColuna+\"']\")).click();"	
				+ "\n			driver.findElement(By.xpath(\".//*[@id='details_\"+sectionTabelaMestre+\"_\"+sectionTabelaDetalhe+\"_grid_\"+idColuna+\"']/a\")).click();"
				+ "\n			util.aguardarElementoXPath(\".//*/div[contains (@class, 'filter-modal')]\");"
				+ "\n		}\n");
		
		f.format("\n		private void preencherCampoFiltro (String textoFiltro) {"
				+ "\n			WebDriverWait wait = new WebDriverWait(driver, 3);"
				+ "\n\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n			String xpathElemento = \".//*[@class='search-bar']/input\";"
				+ "\n\n			util.aguardarElementoXPath(xpathElemento);"
				+ "\n			driver.findElement(By.xpath(xpathElemento)).sendKeys(textoFiltro);"
				+ "\n\n			wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath(\".//*/li[contains (@class, 'filter-all no-selection')]\")), \"Selecionar todos os resultados\"));"
				+ "\n		}\n");
		
		f.format("\n		private void clicarBotaoAvan�ado(){"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n\n			util.aguardarElementoXPath(\".//*[contains (@class,'btn-advanced-filter')]\");"
				+ "\n			driver.findElement(By.xpath(\".//*[contains (@class,'btn-advanced-filter')]\")).click();"
				+ "\n		}\n");
		
		f.format("\n		private void selecionaTipoAvancado(String valorCheck){"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n\n				util.aguardarElementoXPath(\".//*[@class='modal-body advanced-filter']/select\");"
				+ "\n				new Select(driver.findElement(By.xpath(\".//*[@class='modal-body advanced-filter']/select\"))).selectByVisibleText(valorCheck);"
				+ "\n		}\n");
		
		f.format("\n		private void preencherCampoFiltroAvancado (String textoFiltro1, String textoFiltro2){"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n\n			if(textoFiltro2 == null){"		
				+ "\n			util.aguardarElementoXPath(\".//*[@class='modal-body advanced-filter']/input\");"
				+ "\n			driver.findElement(By.xpath(\".//*[@class='modal-body advanced-filter']/input\")).sendKeys(textoFiltro1);"
				+ "\n			} else{//Filtro data."
				+ "\n				util.aguardarElementoXPath(\".//*[@class='modal-body advanced-filter']/div[2]/input\");"
				+ "\n				driver.findElement(By.xpath(\".//*[@class='modal-body advanced-filter']/div[2]/input\")).sendKeys(textoFiltro1);"
				+ "\n				driver.findElement(By.xpath(\".//*[@class='modal-body advanced-filter']/div[3]/input\")).sendKeys(textoFiltro2);"
				+ "\n			}"
				+ "\n		}\n");
		
		f.format("\n		private void clicarBotaoOK () {"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n\n			util.aguardarElementoId(\"filter-ok-button\");"
				+ "\n			driver.findElement(By.id(\"filter-ok-button\")).click();"
				+ "\n			util.aguardarTempo(600);"
				+ "\n		}\n");
		
		f.format("\n		private void clicarBotaoLimpar () {"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n\n			driver.findElement(By.xpath(\".//*/a[contains (@class, 'btn-clear-filter')]\")).click();"
				+ "\n			util.aguardarTempo(500);"
				+ "\n		}\n");
		
		f.format("\n		private String pegarConteudoTabelaMestre (String sectionCodeMestre) {"
				+ "\n			WebDriverWait wait = new WebDriverWait(driver,10);"
				+ "\n\n			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(\".//*[@id='gbox_\"+sectionCodeMestre+\"_grid']/div[2]\")));"
				+ "\n			return driver.findElement(By.id(sectionCodeMestre+\"_grid\")).getText();"
				+ "\n		}\n");
		
		f.format("\n		private String pegarConteudoTabelaDetalhe (String sectionCodeMestre, String fieldCodeDetalhe) {"
				+ "\n			WebDriverWait wait = new WebDriverWait(driver,10);"
				+ "\n\n			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(\".//*[@id='gbox_details_\"+sectionCodeMestre+\"_\"+fieldCodeDetalhe+\"_grid']/div[2]\")));"
				+ "\n			return driver.findElement(By.id(\"details_\"+sectionCodeMestre+\"_\"+fieldCodeDetalhe+\"_grid\")).getText();"
				+ "\n		}\n");
		
		f.format("\n		public void aplicarFiltroMestre (String sectionTabelaMestre, String idColuna, String texto, String xpathCheckbox) {"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
			
				+ "\n\n			String antesFiltro;"
				+ "\n			String depoisFiltro;"
				
				+ "\n\n			antesFiltro = pegarConteudoTabelaMestre(sectionTabelaMestre);"
				+ "\n			abrirModalFiltroMestre(sectionTabelaMestre, idColuna);"
				+ "\n			assertTrue(\"\\n ERRO: N�o foi exibido o modal 'Filtrar'.\", util.pegarTituloModalFiltro().equals(\"Filtrar\"));"
				
				+ "\n\n			preencherCampoFiltro(texto);"
				+ "\n			clicarBotaoOK();"
				+ "\n			depoisFiltro = pegarConteudoTabelaMestre(sectionTabelaMestre);"
	
				+ "\n\n			assertTrue(\"\\n ERRO: O fitro retornou os mesmos registros que antes de ser aplicado. Verifique se existe mais de um registro cadastrado.\", !depoisFiltro.equals(antesFiltro));"
				+ "\n			if ( xpathCheckbox == null ) {"
				+ "\n				assertTrue(\"\\n ERRO: O filtro n�o foi aplicado corretamente. N�o foi retornado registro com '\"+texto+\"'.\","
				+ "\n						depoisFiltro.contains(texto));"
				+ "\n			} else {"
				+ "\n				assertTrue(\"\\n ERRO: O filtro n�o foi aplicado corretamente. Foram retornados registros com checkbox marcados e desmarcados.\","
				+ "\n						validarFiltroCheckbox(xpathCheckbox, texto));"
				+ "\n			}"
				
				+ "\n\n			abrirModalFiltroMestre(sectionTabelaMestre, idColuna);"
				+ "\n			clicarBotaoLimpar();"
				+ "\n			depoisFiltro = pegarConteudoTabelaMestre(sectionTabelaMestre);"
				
				+ "\n\n			assertTrue(\"\\n ERRO: N�o foi removido o filtro.\", depoisFiltro.equals(antesFiltro));"
				+ "\n		}\n");
		
		f.format("\n		public void aplicarFiltroAvancadoMestre (String sectionTabelaMestre, String idColuna, String texto1, String texto2, String xpathCheckbox, String opcaoFiltro) {"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+"\n			//TODO Necess�rio melhorar o tempo do teste avan�ado, demorando muito"	
				+ "\n\n			String antesFiltro;"
				+ "\n			String depoisFiltro;"
				
				+ "\n\n			antesFiltro = pegarConteudoTabelaMestre(sectionTabelaMestre);"
				+ "\n			abrirModalFiltroMestre(sectionTabelaMestre, idColuna);"
				+ "\n			assertTrue(\"\\n ERRO: N�o foi exibido o modal 'Filtrar'.\", util.pegarTituloModalFiltro().equals(\"Filtrar\"));"
				
				+ "\n\n			clicarBotaoAvan�ado();"		
				+ "\n			if (xpathCheckbox == null){"
				+ "\n				selecionaTipoAvancado(opcaoFiltro);"
				+ "\n				preencherCampoFiltroAvancado(texto1, texto2);"
				+ "\n			}else{"
				+ "\n				selecionaTipoAvancado(opcaoFiltro);"
				+ "\n			}"
				+ "\n			clicarBotaoOK();"
				+ "\n			depoisFiltro = pegarConteudoTabelaMestre(sectionTabelaMestre);"
	
				+ "\n\n			if(xpathCheckbox != null && opcaoFiltro.equals(\"Todos\")){"
				+ "\n				assertTrue(\"\\n ERRO: O fitro retornou os mesmos registros que antes do filtro ser aplicado. Verifique se existe mais de um registro cadastrado.\", depoisFiltro.equals(antesFiltro));"	
				+ "\n 			}else {"
				+ "\n				assertTrue(\"\\n ERRO: O fitro retornou os mesmos registros que antes do filtro ser aplicado. Verifique se existe mais de um registro cadastrado.\", !depoisFiltro.equals(antesFiltro));"
				+ "\n 			}"
				+ "\n\n			if (xpathCheckbox == null){ "
				+ "\n				if (!opcaoFiltro.equals(\"Diferente de\") && !opcaoFiltro.equals(\"Menor que\")"
				+ "\n						&& !opcaoFiltro.equals(\"Maior que\")){"
				+ "\n					assertTrue(\"\\n ERRO: O filtro n�o foi aplicado corretamente. N�o foi retornado registro com '\"+texto1+\"'.\",	depoisFiltro.contains(texto1));"
				+ "\n				}"
				+ "\n			}else if (!opcaoFiltro.equals(\"Todos\")){"
				+ "\n				assertTrue(\"\\n ERRO: O filtro n�o foi aplicado corretamente. Foram retornados registros com checkbox marcados e desmarcados.\",	validarFiltroCheckbox(xpathCheckbox, texto1));"
				+ "\n			}"
								
				+ "\n\n			abrirModalFiltroMestre(sectionTabelaMestre, idColuna);"
				+ "\n			clicarBotaoLimpar();"
				+ "\n			depoisFiltro = pegarConteudoTabelaMestre(sectionTabelaMestre);"
				
				+ "\n\n			assertTrue(\"\\n ERRO: N�o foi removido o filtro.\", depoisFiltro.equals(antesFiltro));"
				+ "\n		}\n");
		
		f.format("\n		public void aplicarFiltroDetalhe (String sectionTabelaMestre, String sectionTabelaDetalhe, String idColuna, String texto, String xpathCheckbox) {"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				
				+ "\n\n			String antesFiltro;"
				+ "\n			String depoisFiltro;"
				
				+ "\n\n			antesFiltro = pegarConteudoTabelaDetalhe(sectionTabelaMestre, sectionTabelaDetalhe);"
				+ "\n			abrirModalFiltroDetalhe(sectionTabelaMestre, sectionTabelaDetalhe, idColuna);"
				+ "\n			assertTrue(\"\\n ERRO: N�o foi exibido o modal 'Filtrar'.\", util.pegarTituloModalFiltro().equals(\"Filtrar\"));"
				
				+ "\n\n			preencherCampoFiltro(texto);"
				+ "\n			clicarBotaoOK();"
				+ "\n			depoisFiltro = pegarConteudoTabelaDetalhe(sectionTabelaMestre, sectionTabelaDetalhe);"
	
				+ "\n\n			assertTrue(\"\\n ERRO: O fitro retornou os mesmos registros que antes do filtro ser aplicado. Verifique se existe mais de um registro cadastrado.\", !depoisFiltro.equals(antesFiltro));"
				+ "\n			if ( xpathCheckbox == null ) {"
				+ "\n				assertTrue(\"\\n ERRO: O filtro n�o foi aplicado corretamente. N�o foi retornado registro com '\"+texto+\"'.\","
				+ "\n						depoisFiltro.contains(texto));"
				+ "\n			} else {"
				+ "\n				assertTrue(\"\\n ERRO: O filtro n�o foi aplicado corretamente. Foram retornados registros com checkbox marcados e desmarcados.\","
				+ "\n						validarFiltroCheckbox(xpathCheckbox, texto));"
				+ "\n			}"
				
				+ "\n\n			abrirModalFiltroDetalhe(sectionTabelaMestre, sectionTabelaDetalhe, idColuna);"
				+ "\n			clicarBotaoLimpar();"
				+ "\n			depoisFiltro = pegarConteudoTabelaDetalhe(sectionTabelaMestre, sectionTabelaDetalhe);"
				
				+ "\n\n			assertTrue(\"\\n ERRO: N�o foi removido o filtro.\", depoisFiltro.equals(antesFiltro));"
				+ "\n		}\n");
		
		f.format("\n		public void aplicarFiltroAvancadoDetalhe (String sectionTabelaMestre, String sectionTabelaDetalhe, String idColuna, String texto1, String texto2, String xpathCheckbox, String opcaoFiltro) {"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				
				+ "\n\n			String antesFiltro;"
				+ "\n			String depoisFiltro;"
				
				+ "\n\n			antesFiltro = pegarConteudoTabelaDetalhe(sectionTabelaMestre, sectionTabelaDetalhe);"
				+ "\n			abrirModalFiltroDetalhe(sectionTabelaMestre, sectionTabelaDetalhe, idColuna);"
				+ "\n			assertTrue(\"\\n ERRO: N�o foi exibido o modal 'Filtrar'.\", util.pegarTituloModalFiltro().equals(\"Filtrar\"));"
				
				+ "\n\n			clicarBotaoAvan�ado();"		
				+ "\n			if (xpathCheckbox == null){"
				+ "\n				selecionaTipoAvancado(opcaoFiltro);"
				+ "\n				preencherCampoFiltroAvancado(texto1, texto2);"
				+ "\n			}else{"
				+ "\n			selecionaTipoAvancado(opcaoFiltro);"
				+ "\n			}"
				+ "\n			clicarBotaoOK();"
				+ "\n			depoisFiltro = pegarConteudoTabelaDetalhe(sectionTabelaMestre, sectionTabelaDetalhe);"
	
				+ "\n\n			if(xpathCheckbox != null && opcaoFiltro.equals(\"Todos\")){"
				+ "\n				assertTrue(\"\\n ERRO: O fitro retornou os mesmos registros que antes do filtro ser aplicado. Verifique se existe mais de um registro cadastrado.\", depoisFiltro.equals(antesFiltro));"	
				+ "\n 			}else {"
				+ "\n				assertTrue(\"\\n ERRO: O fitro retornou os mesmos registros que antes do filtro ser aplicado. Verifique se existe mais de um registro cadastrado.\", !depoisFiltro.equals(antesFiltro));"
				+ "\n 			}"
				+ "\n\n			if (xpathCheckbox == null){ "
				+ "\n				if (!opcaoFiltro.equals(\"Diferente de\") && !opcaoFiltro.equals(\"Menor que\")"
				+ "\n						&& !opcaoFiltro.equals(\"Maior que\")){"
				+ "\n					assertTrue(\"\\n ERRO: O filtro n�o foi aplicado corretamente. N�o foi retornado registro com '\"+texto1+\"'.\",	depoisFiltro.contains(texto1));"
				+ "\n				}"
				+ "\n			}else if (!opcaoFiltro.equals(\"Todos\")){"
				+ "\n				assertTrue(\"\\n ERRO: O filtro n�o foi aplicado corretamente. Foram retornados registros com checkbox marcados e desmarcados.\",	validarFiltroCheckbox(xpathCheckbox, texto1));"
				+ "\n			}"
				
				+ "\n\n			abrirModalFiltroDetalhe(sectionTabelaMestre, sectionTabelaDetalhe, idColuna);"
				+ "\n			clicarBotaoLimpar();"
				+ "\n			depoisFiltro = pegarConteudoTabelaDetalhe(sectionTabelaMestre, sectionTabelaDetalhe);"
				
				+ "\n\n			assertTrue(\"\\n ERRO: N�o foi removido o filtro.\", depoisFiltro.equals(antesFiltro));"
				+ "\n		}\n");
		
		f.format("\n		private boolean validarFiltroCheckbox(String xpathCheckbox, String marcadoCheckbox) {"
				+ "\n			FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n			String[] xpath = xpathCheckbox.split(\"/\");"
				+ "\n			boolean valido = false;"
				+ "\n\n			if (marcadoCheckbox.equals(\"True\")) { // TODO remover ap�s corre��o no sistema"
				+ "\n				marcadoCheckbox = \"SIM\";"
				+ "\n			} else if (marcadoCheckbox.equals(\"False\")) {"
				+ "\n				marcadoCheckbox = \"N�O\";"
				+ "\n			}"
				+ "\n\n			// Valida as 10 primeiras linhas da tabela"
				+ "\n			try {"
				+ "\n				for (int l = 3; l < 13; l++) {"
				+ "\n					valido = util.validarCheckbox(xpathCheckbox, marcadoCheckbox);"
				+ "\n					if (!valido) {"
				+ "\n						return false;"
				+ "\n					}"
				+ "\n					xpathCheckbox = \"\";"
				+ "\n					xpath[4] = \"tr[\"+l+\"]\";"
				+ "\n					for (int i = 0; i < (xpath.length-1); i++) {"
				+ "\n						xpathCheckbox += xpath[i]+\"/\";"
				+ "\n					}"
				+ "\n					xpathCheckbox += xpath[xpath.length-1];"
				+ "\n				}"
				+ "\n			} catch (Exception e) {"
				+ "\n				// N�o foi encotnrado o elemento 'xpathCheckbox' atual. Ent�o, n�o existe mais linhas para serem validadas ou est� inv�lido."
				+ "\n			}"
				+ "\n			return valido;"
				+ "\n		}\n");

		f.format("\n}\n");
	}

	private static void criarDiretorios (String localArquivo, String nomeSistema) {
		
		diretorioSalvarArquivos = localArquivo + "/src/";
		criarDiretorio(diretorioSalvarArquivos);
		
	    diretorioSalvarArquivos += "/"+nomeSistema+"/";
		criarDiretorio(diretorioSalvarArquivos);
		
	}
}
