package gapp;

import gapp.test.ScriptsFiltrar;
import gapp.test.ScriptsSourcer;
import gapp.test.ScriptsTests;
import gapp.test.crud.ScriptsCadastrar;
import gapp.test.crud.ScriptsEditar;
import gapp.test.crud.ScriptsRemover;
import gapp.test.main.AbstractScript;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GeradorScript extends AbstractScript {

	// Vari�veis referentes ao arquivo a ser lido
	private static String paginaComTabelas = "Historico";
	private static String paginaComFuncionalidades = "Casos de Testes";

	// Vari�veis referentes as c�lulas da planilha de Casos de Testes
	private static int qtdeLinhasPlanilhaCT = 0;
	private static String[] tabela = null;
	private static ArrayList<String> nomeTabelasDetalhes = new ArrayList<>();
	private static ArrayList<String> nomeSectionCode = new ArrayList<>();
	private static ArrayList<String> funcionalidadesTabela = new ArrayList<>();
	private static ArrayList<String> gruposDeTestes = new ArrayList<>();
	
	// Vari�veis indicando se a Tabele possui determinada funcionalidade
	private static int inserir = 0;
	private static int editar = 0;
	private static int remover = 0;
	private static int filtrar = 0;
	private static int importar = 0;
	private static int exportar = 0;
	
	// Vari�vel com o prefixo da Aba que ser� analisada.  
	public static String prefixSheet = "gapp";
	
	private static String diretorioSalvarTestes;
	private static String diretorioSalvarSource;
	private static int nroTabelaDetalhes;
	private static String localCompletoNovoScript;
	
	private static FormulaEvaluator evaluator;				
	private static CellValue cellValue;
	
	public static void criarScriptsDeTestes (String diretorioSalvarArquivos, File[] arquivoCT, File arquivoDados, String nomeSistema) {
		
		boolean scriptCriadoScuesso = false;
		
		int qtdeScriptsCriados = 0; // Armazena a quantidade de Scripts criados de cada arquivoCT. (TabelaMestre + TabelasDetalhes)
		diretorioSalvarTestes = diretorioSalvarArquivos + "/test/";
		criarDiretorio(diretorioSalvarTestes);
		diretorioSalvarTestes += "/"+nomeSistema+"/";
		criarDiretorio(diretorioSalvarTestes);
		
		diretorioSalvarSource = diretorioSalvarArquivos + "/src/";
		criarDiretorio(diretorioSalvarSource);
		diretorioSalvarSource += "/"+nomeSistema+"/";
		criarDiretorio(diretorioSalvarSource);
		
		for (int i = 0; i < arquivoCT.length; i++) {
			
			qtdeScriptsCriados = 0;
			
			try {
				scriptCriadoScuesso = false;
				obterDadosArquivoCT(arquivoCT[i].getAbsolutePath());
				qtdeCTTotal = arquivoCT.length;
				
				/////////////////////////////////////////// Cria o script da tabela MESTRE /////////////////////////////////////////
				tabelaMestre = true;
				obterFuncionalidadesTabelas(arquivoCT[i].getAbsolutePath(), nomeTabelaMestre);
				scriptCriadoScuesso = gerarScritps(arquivoDados.getAbsolutePath(), sectionCodeMestre, nomeSistema);
				if (scriptCriadoScuesso) qtdeScriptsCriados++;
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				/////////////////////////////////////// Cria os scripts das tabelas DETALHES ///////////////////////////////////////
				tabelaMestre =  false;
				
				for (nroTabelaDetalhes = 1; nroTabelaDetalhes<=qtdeTabelasDetalhes; nroTabelaDetalhes++) {
					
					scriptCriadoScuesso =  false;
					
					// Ler as funcionalidades de uma tabela DETALHE e cria o arquivo com script
					
					obterFuncionalidadesTabelas(arquivoCT[i].getAbsolutePath(), gruposDeTestes.get(nroTabelaDetalhes-1));
					if ( !funcionalidadesTabela.isEmpty() ) {
						scriptCriadoScuesso = gerarScritps(arquivoDados.getAbsolutePath(), nomeSectionCode.get(nroTabelaDetalhes-1), nomeSistema);
						if (scriptCriadoScuesso) qtdeScriptsCriados++;
					}
				}
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				if ( (scriptCriadoScuesso) && (qtdeScriptsCriados==qtdeTabelasTotal) ) {
					logResultadoSucesso += "%nSUCESSO! Os scripts de Testes referentes ao arquivo '"+arquivoCT[i].getName()+"' foram gerados.";
					if (qtdeCampoSemRegra>0) {
						logResultadoSucesso += "  OBS.: alguns campos n�o possuem regra cadastrada.";
						qtdeCampoSemRegra = 0;						
					}
					qtdeCTSucesso++;
				} else if (qtdeScriptsCriados==0) { 
					logResultadoErro += "%nERRO! N�o foi poss�vel gerar os scripts de Testes referente ao arquivo '"+arquivoCT[i].getName()+"'.";
					qtdeCTErro++;
				} else {
					logResultadoImcompleto += "%nATEN��O! N�o foram gerados todos os scripts de Testes referente ao arquivo '"+arquivoCT[i].getName()+"'.";
					qtdeCTIncompleto++;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				logResultadoErro += "%nERRO! N�o foi poss�vel carregar os dados do arquivo de Caso de Teste '"+arquivoCT[i].getName()+"'";
				qtdeCTErro++;
			}
		}
	}
	
	private static void verificarFuncionalidades () {
		
		inserir = 0;
		editar = 0;
		remover = 0;
		filtrar = 0;
		importar = 0;
		exportar = 0;
		
		for (int i=0; i<funcionalidadesTabela.size(); i++) {
			if (funcionalidadesTabela.get(i).equals("Inserir")) {
				inserir = 1;
			} else if (funcionalidadesTabela.get(i).equals("Editar")) {
				editar = 1;
			} else if (funcionalidadesTabela.get(i).equals("Remover")) {
				remover = 1;
			} else if (funcionalidadesTabela.get(i).equals("Filtrar")) {
				filtrar = 1;
			} else if (funcionalidadesTabela.get(i).equals("Importar")) {
				importar = 1;
			} else if (funcionalidadesTabela.get(i).equals("Exportar")) {
				exportar = 1;
			}
		}
	}
	
	private static void obterDadosArquivoCT (String enderecoArquivo) throws Exception {
		String textoCelula = null;
		nomeSectionCode.clear();
		nomeAbasIntermediarias.clear();
		nomeTabelasDetalhes.clear();
		gruposDeTestes.clear();
		
		try {

			Workbook workbook = null;
			if (enderecoArquivo.toUpperCase().endsWith("XLS")) {
				workbook = new HSSFWorkbook(new FileInputStream(enderecoArquivo));
			} else if (enderecoArquivo.toUpperCase().endsWith("XLSX")) {
				workbook = new XSSFWorkbook(new FileInputStream(enderecoArquivo));
			} else {
				throw new Exception("Arquivo do formato inv�lido. Deve ser XLS ou XLSX.");
			}
			
			Sheet sheet = workbook.getSheet(paginaComTabelas);
			
			Cell cell = null;
		    
			cell = sheet.getRow(19).getCell(2); // C�lula com o nome da Tabela Mestre
			nomeTabelaMestre = cell.getStringCellValue();
			
			cell = sheet.getRow(19).getCell(4); // C�lula com o nome da Tabela Mestre no BANCO DE DADOS
			sectionCodeMestre = cell.getStringCellValue();
			
			//Para ler o resultado da F�rmula que preenche a c�lula 'Quantidade de Tabelas'
			cell = sheet.getRow(15).getCell(4);
			
			evaluator = workbook.getCreationHelper().createFormulaEvaluator();				
			cellValue = evaluator.evaluate(cell);
			
			qtdeTabelasTotal = cellValue.getNumberValue();
			qtdeTabelasDetalhes = qtdeTabelasTotal-1;

			// Apartir da linha 21 que come�a a ser exibida as tabelas Detalhes.
			for (int linha=0; linha<qtdeTabelasDetalhes; linha++) {
				
				cell = sheet.getRow(linha+20).getCell(2); // Valor na coluna 'Par�metros'
				textoCelula = cell.getStringCellValue();
				
				if (textoCelula.contains("->")) {
					tabela = textoCelula.split(" -> ");
					nomeAbasIntermediarias.add(tabela[0]);
					nomeTabelasDetalhes.add(tabela[1]);
				} else {
					nomeTabelasDetalhes.add(cell.getStringCellValue());
				}
				gruposDeTestes.add(textoCelula);
				
				cell = sheet.getRow(linha+20).getCell(4); 
				nomeSectionCode.add(cell.getStringCellValue());
			}
			
			workbook.close();
           
	     } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }
	}
	
	private static void obterFuncionalidadesTabelas (String enderecoArquivo, String nomeTabela) throws Exception {
		try {

			Workbook workbook = null;
			if (enderecoArquivo.toUpperCase().endsWith("XLS")) {
				workbook = new HSSFWorkbook(new FileInputStream(enderecoArquivo));
			} else if (enderecoArquivo.toUpperCase().endsWith("XLSX")) {
				workbook = new XSSFWorkbook(new FileInputStream(enderecoArquivo));
			} else {
				throw new Exception("Arquivo do formato inv�lido. Deve ser XLS ou XLSX.");
			}
			
			Sheet sheet = workbook.getSheet(paginaComFuncionalidades);
			Cell cell = null;
		    
			qtdeLinhasPlanilhaCT = sheet.getLastRowNum();
			
			funcionalidadesTabela.clear();
			
			// Iniciando na LINHA 5, � verificado o Grupo de Teste e suas Funcionalidades
			for (int linha=5; linha<qtdeLinhasPlanilhaCT;linha++) {
				
				cell = sheet.getRow(linha).getCell(1); // C�lula com o nome do Grupo de Teste

				evaluator = workbook.getCreationHelper().createFormulaEvaluator();				
				cellValue = evaluator.evaluate(cell);
				
				if (cell != null) {
				
					if (cellValue.getStringValue().length()>0) {
						// Armazena todas as funcionalidades da TABELA analisada
						if (cellValue.getStringValue().equals(nomeTabela)) {
							cell = sheet.getRow(linha).getCell(2);
							funcionalidadesTabela.add(cell.getStringCellValue());
						}
					}
				}
			}
			
			workbook.close();
           
	     } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }
	}
	
	private static boolean gerarScritps (String enderecoArquivo, String nomeSessao, String nomeSistema) throws Exception {
		try {

			Workbook workbook = null;
			if (enderecoArquivo.toUpperCase().endsWith("XLS")) {
				workbook = new HSSFWorkbook(new FileInputStream(enderecoArquivo));
			} else if (enderecoArquivo.toUpperCase().endsWith("XLSX")) {
				workbook = new XSSFWorkbook(new FileInputStream(enderecoArquivo));
			} else {
				throw new Exception("Arquivo do formato inv�lido. Deve ser XLS ou XLSX.");
			}
			
			obterNomesTabelas(workbook);
			
			// FOR para percorrer todas as abas (planilhas/Sheet) do arquivo
			for (int s=0; s<workbook.getNumberOfSheets(); s++) {
				
				Sheet sheet = workbook.getSheetAt(s);
				
				// Ler os dados apenas das abas que se iniciam com o prefixo configurado.
				if (sheet.getSheetName().startsWith(prefixSheet)) {
					
					Cell cell = null;
					String valorCelula = null;
					boolean arquivoTesteCriado = false;
					boolean arquivoScrCriado = false;
					
					valorColunaId.clear();
					valorColunaTipo.clear();
					valorColunaEditavel.clear();
					valorColunaDeRegra.clear();
					dadosTabela.clear();
					valorColunaLabel.clear();
					valorColunaUnique.clear();
					valorColunaLabelGroupHeader.clear();
					valorColunaObrigatoria.clear();
					valorColunaJoin.clear();
					
					for (int linha=0; linha<sheet.getLastRowNum(); linha++) {
						
						if ( sheet.getRow(linha) != null ) {
							cell = sheet.getRow(linha).getCell(colunaSectionCode, Row.CREATE_NULL_AS_BLANK);
						} else { cell = null; }
						
						if (cell != null) {
							valorCelula = cell.getStringCellValue();
							
							if (valorCelula.equals(nomeSessao)) {
								
								if (sheet.getRow((linha-1)).getCell(colunaSectionCode).getStringCellValue().equals("SectionCode")) {
									
									cell = sheet.getRow(linha+3).getCell(colunaFieldCodeDB); // Coluna FieldCodeDB
									valorCelula = cell.getStringCellValue();
									
									// Amarzenar os dados da Tabela (Mestre ou Detalhe)
									for (int i=0; i<23; i++) {
										cell = sheet.getRow(linha).getCell(i);
										
										if (cell != null ) {
											dadosTabela.add(cell.getStringCellValue());
										} else {
											dadosTabela.add("null");
										}
									}
									
									if (tabelaMestre) {
										if (dadosTabela.get(colunaSubIndexLabel).isEmpty()) {
											tituloPaginaMestre = dadosTabela.get(colunaIndexLabel);
										} else {
											tituloPaginaMestre = dadosTabela.get(colunaIndexLabel)+" "+dadosTabela.get(colunaSubIndexLabel);
										}
									}

									do {
										valorColunaId.add(valorCelula);
										
										 // Coluna Type
										cell = sheet.getRow(linha+3).getCell(colunaType);
										valorColunaTipo.add(cell.getStringCellValue());
										
										 // Coluna Editable
										cell = sheet.getRow(linha+3).getCell(colunaEditable);
										valorColunaEditavel.add(cell.getStringCellValue());

										// Coluna Label
										cell = sheet.getRow(linha+3).getCell(colunaLabel);
										valorColunaLabel.add(cell.getStringCellValue());
										
										// Coluna Unique
										cell = sheet.getRow(linha+3).getCell(colunaUnique);
										valorColunaUnique.add(cell.getStringCellValue());
										
										// Coluna Mandatory
										cell = sheet.getRow(linha+3).getCell(colunaMandatory);
										valorColunaObrigatoria.add(cell.getStringCellValue());
										
										// Coluna Validation Rules
										if ( sheet.getRow(linha+3) != null ) {
											cell = sheet.getRow(linha+3).getCell(colunaValidationRules, Row.CREATE_NULL_AS_BLANK); 
										} else { cell = null; }
										
										if (cell != null) {
											valorColunaDeRegra.add(cell.getStringCellValue());
										} else {
											valorColunaDeRegra.add("null");
										}
										
										// Coluna Join
										if ( sheet.getRow(linha+3) != null ) {
											cell = sheet.getRow(linha+3).getCell(colunaJoin, Row.CREATE_NULL_AS_BLANK); 
										} else { cell = null; }
										
										if (cell != null) {
											valorColunaJoin.add(cell.getStringCellValue());
										} else {
											valorColunaJoin.add("");
										}
										
										// TODO Remover ap�s corre��o de LOOM-203
										// Coluna LabelGroupHeader
										if ( sheet.getRow(linha+3) != null ) {
											cell = sheet.getRow(linha+3).getCell(colunaLabelGroupHeader, Row.CREATE_NULL_AS_BLANK); 
										} else { cell = null; }
										
										if (cell != null) {
											valorColunaLabelGroupHeader.add(cell.getStringCellValue());
										} else {
											valorColunaLabelGroupHeader.add("null");
										}
										// FIM Coluna LabelGroupHeader
										
										linha++;
										
										cell = sheet.getRow(linha+3).getCell(colunaFieldCodeDB); // Coluna FieldCodeDB
										valorCelula = cell.getStringCellValue();
										
									} while (!valorCelula.equals("</Fields>"));
									
									workbook.close();
									
									arquivoTesteCriado = criarArquivoTestes(nomeSessao, diretorioSalvarTestes, nomeSistema);
									arquivoScrCriado = criarArquivoSource(nomeSessao, diretorioSalvarSource, nomeSistema);
									
									if (arquivoTesteCriado && arquivoScrCriado) {
										return true;
									} else
										return false;
								}
							}
						}
					}
				}
			}
			workbook.close();
	     } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }
		return false;
	}

	private static void obterNomesTabelas(Workbook workbook) {
		String sectionCodeMestre = "";
		todosSectionCodeTabelas.clear();
		todosDBTableName.clear();
		todosDatilOf.clear();
		
		for (int s=0; s<workbook.getNumberOfSheets(); s++) { // Percorre todas as abas
			
			Sheet sheet = workbook.getSheetAt(s);
			
			if (sheet.getSheetName().startsWith(prefixSheet)) { // Acessa os dados apenas das abas que se iniciam com o prefixo configurado.
				
				Cell cell = null;
				String valorCelula = null;
				
				for (int linha=0; linha<sheet.getLastRowNum(); linha++) {
					
					if ( sheet.getRow(linha) != null ) {
						cell = sheet.getRow(linha).getCell(colunaSectionCode, Row.CREATE_NULL_AS_BLANK);
					} else { cell = null; }
					
					if (cell != null) {
						valorCelula = cell.getStringCellValue();
						
						if ( (valorCelula.equals("<Section>")) || (valorCelula.equals("<DetailSection>")) ) {
							cell = sheet.getRow(linha+2).getCell(colunaSectionCode); // Coluna 'SectionCode' que consta o nome da tabela.
							
							if (valorCelula.equals("<Section>")) { // Se for tabela Mestre
								sectionCodeMestre = cell.getStringCellValue();
							}
							
							todosSectionCodeTabelas.add(cell.getStringCellValue());
							
							cell = sheet.getRow(linha+2).getCell(colunaDBTableName); // Coluna 'DBTableName' que consta o nome da tabela gravada em banco.
							todosDBTableName.add(cell.getStringCellValue());

							todosDatilOf.add(sectionCodeMestre); // Coluna DatailOf. Para o caso da tabela mestre, ser� a pr�pria tabela.
						}
					}
				}
			}
		}
	}
	
	private static boolean criarArquivoTestes(String nomeSessao, String localArquivo, String nomeSistema) {

		String arquivoCriado; //Diret�rio e Nome do arquivo de script criado.
		
		ScriptsTests tst = new ScriptsTests();
		ScriptsEditar edit = new ScriptsEditar();
		ScriptsCadastrar cad = new ScriptsCadastrar();
		ScriptsRemover rem = new ScriptsRemover();
		GeradorValores valores = new GeradorValores();
		ScriptsFiltrar fil = new ScriptsFiltrar();
		
		nomeSessao = nomeSessao.replaceAll("\\s+", "");
		nomeSessao += "Test";
		
		String diretorioSalvarArquivos = localArquivo;
		
		// Cria um subdiret�rio para salvar os Scripts de Testes
	    File diretorio = new File(diretorioSalvarArquivos);
	    	if(!diretorio.exists()){ diretorio.mkdir(); }
	    
	    // Cria um diret�rio (package) com o nome da Tabela Mestre para adiconar os scripts de Testes
		diretorioSalvarArquivos += sectionCodeMestre.replaceAll("\\s+", "") + "/";
	    diretorio = new File(diretorioSalvarArquivos);
	    if(!diretorio.exists()){ diretorio.mkdir(); }
		
	    // Cria o arquivo (.java) com o Script de Testes 
		arquivoCriado = diretorioSalvarArquivos.replace("./", "") + nomeSessao + ".java";
		
		//////////////////////////////////////// IN�CIO DO ARQUIVO //////////////////////////////////////
		try {
			Formatter arquivoScript = new Formatter(arquivoCriado);
			verificarFuncionalidades();
			
			textoDataCriacao(arquivoScript);
			tst.headerScript(arquivoScript, sectionCodeMestre, nomeSessao, nomeSistema);
			tst.acessarPagina(arquivoScript);
			
			if (inserir == 1) {
				valores.gerarValoresParaCadastro();
				cad.cadastrarTest (arquivoScript);
			}
			
			if (editar == 1) {
				valores.gerarValoresParaEditar();
				edit.editarTest(arquivoScript); 
			}
			
			if (filtrar == 1) {
				fil.filtrarBasicoTest(arquivoScript, sectionCodeMestre);
				fil.filtrarAvancadoTest(arquivoScript, sectionCodeMestre);
			}
			
			if (exportar == 1) {
				tst.exportar(arquivoScript);
			}
			
			if (importar == 1) {
				tst.importar(arquivoScript);
			}
			
			if (remover == 1) {
				rem.remover(arquivoScript);
			}
			
//			if (existeTabelaDependente) {
//				cadastrarRegistrosDependentes(arquivoScript);
//			}
			
			arquivoScript.format("}"); // Fecha a classe de Test
			//////////////////////////////////////// FINAL DO ARQUIVO //////////////////////////////////////
			
			arquivoScript.close();
			
			localCompletoNovoScript = new File(arquivoCriado).getAbsolutePath();
			logScriptsSucesso += "%nSUCESSO! Script criado em " + localCompletoNovoScript;
			qtdeScriptSucesso++;
			
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			logScriptsErro += "%nERRO! Script n�o gerado ou incompleto: " + localCompletoNovoScript;
			qtdeScriptErro++;
			
			return false;
		}
	}
	
	private static boolean criarArquivoSource(String nomeSessao, String localArquivo, String nomeSistema) {

		String arquivoCriado; //Diret�rio e Nome do arquivo de script criado.
		
		ScriptsCadastrar cad = new ScriptsCadastrar();
		ScriptsEditar edit = new ScriptsEditar();
		ScriptsSourcer src = new ScriptsSourcer();
		ScriptsFiltrar fil = new ScriptsFiltrar();
		
		nomeSessao = nomeSessao.replaceAll("\\s+", "");
		
		String diretorioSalvarArquivos = localArquivo;
		
		// Cria um subdiret�rio para salvar os Scripts Source
	    File diretorio = new File(diretorioSalvarArquivos);
	    	if(!diretorio.exists()){ diretorio.mkdir(); }
	    
	    // Cria um diret�rio (package) com o nome da Tabela Mestre para adiconar os scripts de Testes
		diretorioSalvarArquivos += sectionCodeMestre.replaceAll("\\s+", "") + "/";
	    diretorio = new File(diretorioSalvarArquivos);
	    if(!diretorio.exists()){ diretorio.mkdir(); }
		
	    // Cria o arquivo (.java) com o Script de Testes 
		arquivoCriado = diretorioSalvarArquivos.replace("./", "") + nomeSessao + ".java";
		
		//////////////////////////////////////// IN�CIO DO ARQUIVO //////////////////////////////////////
		try {
			Formatter arquivoScript = new Formatter(arquivoCriado);
			verificarFuncionalidades();
			
			textoDataCriacao(arquivoScript);
			src.headerScript(arquivoScript, sectionCodeMestre, nomeSessao, nomeSistema);
			
			if (inserir == 1) {
				cad.adicionarRegistro(arquivoScript, nomeSessao, nomeSistema);
			}

			if (editar == 1) {
				edit.editarRegistro(arquivoScript);
			}
			
			if (filtrar == 1) {
				fil.metodoClicarBotaoFiltro(arquivoScript, nroTabelaDetalhes);
			}
			
			if (exportar == 1) {
//				tst.testExportar(arquivoScript, tipoTabela);
			}
			
			if (importar == 1) {
//				tst.testImportar(arquivoScript, tipoTabela);
			}
			
			if (tabelaMestre) {
				src.metodosTabelaMestre(arquivoScript);
			} else {
				src.metodosTabelaDetalhes(arquivoScript, nroTabelaDetalhes);
			}
			
			src.metodoValidarRegistroTabela(arquivoScript);
			
			arquivoScript.format("}");
			//////////////////////////////////////// FINAL DO ARQUIVO //////////////////////////////////////
			
			arquivoScript.close();
			
			localCompletoNovoScript = new File(arquivoCriado).getAbsolutePath();
			logScriptsSucesso += "%nSUCESSO! Script criado em " + localCompletoNovoScript;
			qtdeScriptSucesso++;
			
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			logScriptsErro += "%nERRO! Script n�o gerado ou incompleto: " + localCompletoNovoScript;
			qtdeScriptErro++;
			
			return false;
		}
	}
	
//	// Cadastra registros nas tabelas dependentes para realiza��o dos testes
//	private static void cadastrarRegistrosDependentes(Formatter f) {
//		String nomeTabela;
//		
//		f.format("\n	private void cadastrarDependencias() {");
//		for (int j = 0; j < tabelasDependentesBD.size(); j++) {
//			for (int i = 0; i < todosDBTableName.size(); i++) {
//				if (tabelasDependentesBD.get(j).equals(todosDBTableName.get(i))) {
//					nomeTabela = todosSectionCodeTabelas.get(i);
//					
//					f.format("\n\n		// Tabela dependente: "+nomeTabela);
//					f.format("\n		"+nomeTabela+"Test "+nomeTabela.toLowerCase()+"test = new "+nomeTabela+"Test();");
//					f.format("\n		"+nomeTabela+" "+nomeTabela.toLowerCase()+" = new "+nomeTabela+"(driver);");
//					f.format("\n		"+nomeTabela.toLowerCase()+"test.acessarPagina();");
//					f.format("\n		"+nomeTabela.toLowerCase()+".adicionarRegistroCadastro();");
//					f.format("\n		"+nomeTabela.toLowerCase()+".adicionarRegistrosFiltros();");
//				}
//			}
//		}
//		f.format("\n	}\n");
//	}
	
	@SuppressWarnings("resource")
	public static boolean validarArquivoCT (File[] arquivoCT) {
		String enderecoArquivo;
		Workbook workbook = null;
		Sheet sheet;
		Cell cell = null;
		
		for (int i = 0; i < arquivoCT.length; i++) {
			enderecoArquivo = arquivoCT[i].getAbsolutePath();
			try {
				
				if (enderecoArquivo.toUpperCase().endsWith("XLS")) {
					workbook = new HSSFWorkbook(new FileInputStream(enderecoArquivo));
				} else if (enderecoArquivo.toUpperCase().endsWith("XLSX")) {
					workbook = new XSSFWorkbook(new FileInputStream(enderecoArquivo));
				} else {
					throw new Exception("Arquivo do formato inv�lido. Deve ser XLS ou XLSX.");
				}
				
				sheet = workbook.getSheet(paginaComTabelas);
				cell = sheet.getRow(1).getCell(1); // C�lula com o texto "Caso de Teste"
				
				if (cell != null) {
					if (!(cell.toString().toUpperCase().equals("CASO DE TESTE"))) {
						return false;
					}
				} else {
					return false;
				}
			} catch (Exception e) {
				return false;
			}
		}
		return true;
	}
	
}
