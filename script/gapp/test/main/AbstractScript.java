package gapp.test.main;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;

import javax.swing.JOptionPane;

public abstract class AbstractScript {

	// Dados da Tabela analisada
	public static ArrayList<String> dadosTabela = new ArrayList<>();
	public static ArrayList<String> nomeAbasIntermediarias = new ArrayList<>();
	public static String nomeTabelaMestre;
	public static String sectionCodeMestre;
	public static String tituloPaginaMestre;
	public static boolean tabelaMestre;
	
	public static ArrayList<String> valorColunaId = new ArrayList<>(); //Coluna 'FieldCode'
	public static ArrayList<String> valorColunaTipo = new ArrayList<>(); //Coluna 'Type'
	public static ArrayList<String> valorColunaEditavel = new ArrayList<>(); //Coluna 'Editable'
	public static ArrayList<String> valorColunaDeRegra = new ArrayList<>(); //Coluna 'Validation Rules'
	public static ArrayList<String> valorColunaLabel = new ArrayList<>(); //Coluna 'Label'
	public static ArrayList<String> valorColunaUnique = new ArrayList<>(); //Coluna 'Unique'
	public static ArrayList<String> valorColunaLabelGroupHeader = new ArrayList<>(); //Coluna 'LabelGroupHeader'
	public static ArrayList<String> valorColunaObrigatoria = new ArrayList<>(); //Coluna 'Mandatory'
	public static ArrayList<String> valorColunaJoin = new ArrayList<>(); // Coluna 'Join' com informa��es de quais tabelas dependentes para o campo
	
	// Armazenas os valores das colunas 'SectionCode', 'DBTabelName' e 'DetailOf' (Tabela Mestre) de todas as tabelas do sistema.
	public static ArrayList<String> todosSectionCodeTabelas = new ArrayList<>();
	public static ArrayList<String> todosDBTableName = new ArrayList<>();
	public static ArrayList<String> todosDatilOf = new ArrayList<>();
	
	// INDEX das respectivas colunas na planilha de Template (sess�o <Section> ou <DetailSection>)
	public static int colunaSectionCode = 0;
	public static int colunaTransaction = 1;
	public static int colunaIndexLabel = 2;
	public static int colunaLabelSingular = 3;
	public static int colunaLabelPlural = 4;
	public static int colunaSubIndexLabel = 5;
	public static int colunaCreateLabel = 6;
	public static int colunaEditLabel = 7;
	public static int colunaMenuLabel = 8;
	public static int colunaDetailsName = 9;
	public static int colunaGroupHeaderFirst = 10;
	public static int colunaPath = 11;
	public static int colunaDependencies = 12;
	public static int colunaInsert = 13;
	public static int colunaRemove = 14;
	public static int colunaFilter = 15;
	public static int colunaDetailOf = 16;
	public static int colunaDBTableName = 17;
	public static int colunaOrderBy = 18;
	public static int colunaOrderType = 19;
	public static int colunaMasterIdColumn = 20;
	public static int colunaWorkbook = 21;
	public static int colunaWorksheet = 22;
	public static int colunaStartline = 23;
	
	// INDEX das respectiva coluans na planilha de Template (sess�o <Fields>)
	public static int colunaFieldCodeDB = 0;
	public static int colunaLabel = 1;
	public static int colunaLabelGroupHeader = 2;
	public static int colunaType = 3;
	public static int colunaUnique = 4;
	public static int colunaEditable = 5;
	public static int colunaMandatory = 6;
	public static int colunaFieldDescription = 7;
	public static int colunaValidationRules = 8;
	public static int colunaJoin = 9;
	public static int colunaTrigger = 10;
	public static int colunaTriggerQuery = 11;
	
	// O caminho (menu) para acessar uma determinada p�gina
	public static String[] menus;
	
	// Valores finais ap�s o Cadastro/Edi��o de um registro
	public static ArrayList<String> valoresCadastrados = new ArrayList<>(); // Valores utilizados para cadastrar o registro principal
	public static ArrayList<String> valoresCadastradosMestre = new ArrayList<>(); //Valores utilizados para cadastrar o registro mestre
	public static ArrayList<String> valoresCadastradosFiltro1 = new ArrayList<>(); // Valores utilizados para cadastrar os registros para o filtro
	public static ArrayList<String> valoresCadastradosFiltro2 = new ArrayList<>();
	public static ArrayList<String> valoresInvalidos = new ArrayList<>();
	public static ArrayList<String> valoresEditadosModal = new ArrayList<>();
	public static ArrayList<String> valoresEditadosTabela = new ArrayList<>();
	
	// Vari�veis para o Log com o resutlado da execu��o
	public static String logScriptsSucesso = "";
	public static String logScriptsErro = "";
	public static String logResultadoImcompleto = "";
	public static String logResultadoSucesso = "";
	public static String logResultadoErro = "";
	public static int qtdeScriptSucesso = 0;
	public static int qtdeScriptErro = 0;
	public static int qtdeCTTotal;
	public static int qtdeCTSucesso = 0;
	public static int qtdeCTErro = 0;
	public static int qtdeCTIncompleto = 0; // Alguma(s) da(s) tabela(s) do CT n�o foi encontrada na planilha com os Dados
	public static int qtdeCampoSemRegra = 0; // Todos os campos devem ter uma regra especificada
	public static Double qtdeTabelasTotal = 0.0;
	public static Double qtdeTabelasDetalhes = 0.0;
	
	public static boolean arquivosCTValidos = false;
	
	public static DateFormat formatoDataHora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	public static DateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
	public static Date dataAtual = new Date();
	
	// Dados das tabelas dependentes
	public static boolean existeTabelaDependente;
	public static ArrayList<String> tabelasDependentesBD = new ArrayList<>();
	
	public static boolean criarDiretorio (String diretorioScripts) {
		try {
			File diretorioArquivo = new File(diretorioScripts);
			if (!diretorioArquivo.exists()) {
				diretorioArquivo.mkdir();
			}
			return true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "N�o foi poss�vel criar o diret�rio \n"+ diretorioScripts 
					+ "\n\nFavor verificar permis�o para grava��o no diret�rio.", "", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
	
	public static void textoDataCriacao (Formatter f) {
		DateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date data = new Date(System.currentTimeMillis());
		String dataAtual = formatoData.format(data);
		
		f.format("// Script gerado automaticamente em " + dataAtual + "\n");
	}
	
	public static void zerarQtdeLog () {
		qtdeScriptSucesso = 0;
		qtdeScriptErro = 0;
		qtdeCTTotal = 0;
		qtdeCTSucesso = 0;
		qtdeCTErro = 0;
		qtdeCTIncompleto = 0;
		qtdeCampoSemRegra = 0;
		logScriptsSucesso = "";
		logScriptsErro = "";
		logResultadoImcompleto = "";
		logResultadoSucesso = "";
		logResultadoErro = "";
	}

}
