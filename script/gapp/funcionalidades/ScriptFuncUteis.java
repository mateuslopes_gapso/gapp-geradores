package gapp.funcionalidades;

import gapp.test.main.AbstractScript;

import java.io.File;
import java.util.Formatter;

public class ScriptFuncUteis extends AbstractScript {

	private static String diretorioArquivoCriado;
	private static String diretorioSalvarArquivos;
	private static Formatter arquivo;
	private static String localCompletoAbstract;
	
	public static void criarFuncionalidadesUteis (String localArquivo, String nomeSistema) {
		
		criarDiretorios(localArquivo, nomeSistema);

		try {
			
			diretorioSalvarArquivos = diretorioSalvarArquivos.replaceAll("\\s+", "") + "/";
		    diretorioArquivoCriado = diretorioSalvarArquivos.replace("./", "");
			
    	    arquivo = new Formatter(diretorioArquivoCriado + "/FuncionalidadesUteis.java");
    	    textoDataCriacao(arquivo);
    	    codigoFuncionalidadesUteis(arquivo, nomeSistema); // TODO Pegar vari�vel de uma classe Abstract
    	    arquivo.close();
    	    
    	    localCompletoAbstract = new File(diretorioArquivoCriado).getAbsolutePath();
			logScriptsSucesso += "%nSUCESSO! Arquivo AbstractTest.java criado em " + localCompletoAbstract;
			qtdeScriptSucesso++;
    	    
		} catch (Exception e) {
			logScriptsErro += "%nERRO! N�o foi criado o arquivo \\src\\FuncionalidadesUteis.java";
			qtdeScriptErro++;
			e.printStackTrace();
		}
	}
	
	private static void codigoFuncionalidadesUteis (Formatter f, String sistema) {
		f.format("package "+sistema+";");
		f.format("\n");
		f.format("\nimport org.openqa.selenium.By;");
		f.format("\nimport org.openqa.selenium.Keys;");
		f.format("\nimport org.openqa.selenium.WebDriver;");
		f.format("\nimport org.openqa.selenium.WebElement;");
		f.format("\nimport org.openqa.selenium.interactions.Actions;");
		f.format("\nimport org.openqa.selenium.support.ui.ExpectedConditions;");
		f.format("\nimport org.openqa.selenium.support.ui.WebDriverWait;");
		f.format("\n");
		f.format("\npublic class FuncionalidadesUteis {"
				+ "\n"
				+ "\n	private WebDriver driver = null;"
				+ "\n	public String titulo = null;"
				+ "\n");
		
		f.format("\n	public FuncionalidadesUteis (WebDriver driver) {"
				+ "\n		this.driver = driver;"
				+ "\n	}"
				+ "\n");
		
		f.format("\n	public void aguardarElementoXPath (String xpathElemento) {"
				+ "\n		WebDriverWait wait = new WebDriverWait(driver, 10);"
				+ "\n		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(\"div[class='ajaxContentImage']\")));"
				+ "\n		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathElemento)));"
				+ "\n		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathElemento)));"
				+ "\n		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathElemento)));"
				+ "\n		aguardarTempo(700);"
				+ "\n	}"
				+ "\n");
		
		f.format("\n	public void aguardarElementoCSS (String CssElemento) {"
				+ "\n		WebDriverWait wait = new WebDriverWait(driver, 10);"
				+ "\n		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(\"div[class='ajaxContentImage']\")));"
				+ "\n		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(CssElemento)));"
				+ "\n		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(CssElemento)));"
				+ "\n		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(CssElemento)));"
				+ "\n		aguardarTempo(700);"
				+ "\n	}"
				+ "\n");
		
		f.format("\n	public void aguardarElementoId (String IdElemento) {"
				+ "\n		WebDriverWait wait = new WebDriverWait(driver, 10);"
				+ "\n		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(IdElemento)));"
				+ "\n		wait.until(ExpectedConditions.elementToBeClickable(By.id(IdElemento)));"
				+ "\n	}"
				+ "\n");
		
		f.format("\n	public void acessarFavorito () {"
				+ "\n		driver.findElement(By.xpath(\"//div[1]/div/a[1]\")).click();"
				+ "\n	}"
				+ "\n");
		
		f.format("\n	public void pegarTituloModal () {"
				+ "\n		aguardarElementoId(\"modalRemote\");"
				+ "\n		titulo = driver.findElement(By.xpath(\".//*[@id='modalRemote']/div/h2/em\")).getText();"
				+ "\n	}"
				+ "\n");
		
		f.format("\n	public String pegarTituloModalFiltro () {"
				+ "\n		aguardarElementoXPath(\".//*[contains(@class, 'filter-modal')]\");"
				+ "\n		return driver.findElement(By.xpath(\".//*[contains(@class, 'filter-modal')]/div[1]/h3\")).getText();"
				+ "\n	}\n");
		
		f.format("\n	public String pegarTituloPagina (String sectionCode) {"
				+ "\n		aguardarElementoId(\"contentTable_\"+sectionCode);"
				+ "\n		return driver.findElement(By.xpath(\".//*[@id='contentTable_\"+sectionCode+\"']/h2/em\")).getText();"
				+ "\n	}\n");
		
		f.format("\n	public String pegarTextoNavegacao (String sectionCode) {"
				+ "\n		aguardarElementoId(\"contentTable_\"+sectionCode);"
				+ "\n		return driver.findElement(By.xpath(\".//*[@id='contentTable_\"+sectionCode+\"']/div[1]\")).getText();"
				+ "\n	}\n");
		
		f.format("\n	public void limparCampoXPath (String xpathElemento) {"
				+ "\n		WebElement elemento = driver.findElement(By.xpath(xpathElemento));"
				+ "\n		int tamanhoTexto = elemento.getAttribute(\"value\").length();"
				+ "\n		for (int i=0; i<tamanhoTexto; i++) {"
				+ "\n			elemento.sendKeys(Keys.HOME);"
				+ "\n			elemento.sendKeys(Keys.DELETE);"
				+ "\n		}"
				+ "\n	}\n");
		
		f.format("\n	public void limparCampoID (String idElemento) {"
				+ "\n		WebElement elemento = driver.findElement(By.id(idElemento));"
				+ "\n		int tamanhoTexto = elemento.getAttribute(\"value\").length();"
				+ "\n		for (int i=0; i<tamanhoTexto; i++) {"
				+ "\n			elemento.sendKeys(Keys.HOME);"
				+ "\n			elemento.sendKeys(Keys.DELETE);"
				+ "\n		}"
				+ "\n	}\n");
		
		f.format("\n	public void visualizarMenu (String linkText) {"
				+ "\n		WebElement elemento = driver.findElement(By.linkText(linkText));"
				+ "\n"
				+ "\n		Actions mouse = new Actions(driver);"
				+ "\n		mouse.moveToElement(elemento).build().perform();"
				+ "\n	}\n");
		
		f.format("\n	public void clicarMenu (String linkText) {"
				+ "\n		driver.findElement(By.linkText(linkText)).click();"
				+ "\n	}\n");
		
		f.format("\n	public void aguardarTempo (int milisegundos) {"
				+ "\n		try { Thread.sleep(milisegundos);"
				+ "\n		     } catch (Exception e) {}"
				+ "\n	}\n");
		
		f.format("\n	public void aguardarTelaBloqueio () {"
				+ "\n		WebDriverWait wait = new WebDriverWait(driver, 10);"
				+ "\n		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(\"div[class*='modal-backdrop']\")));"
				+ "\n	}\n");
		
		f.format("\n	public String pegarMensagemSucesso () {"
				+ "\n		aguardarElementoCSS(\"div[class='jGrowl-message']\");"
				+ "\n		return driver.findElement(By.cssSelector(\"div[class='jGrowl-message']\")).getText();"
				+ "\n	}\n");
		
		f.format("\n	public String pegarMensagemErro () {"
				+ "\n		aguardarElementoCSS(\"div[class='jGrowl-message']\");"
				+ "\n		// System.out.println(\"Mensagem Erro: \"+driver.findElement(By.cssSelector(\"div[class='jGrowl-message']\")).getText());"
				+ "\n		return driver.findElement(By.cssSelector(\"div[class='jGrowl-message']\")).getText();"
				+ "\n	}\n");
		
		f.format("\n	public String pegarNovaMensagemErro () {"
				+ "\n		aguardarElementoId(\"infocnt\");"
				+ "\n		String msg = driver.findElement(By.id(\"infocnt\")).getText();"
				+ "\n		return msg;"
				+ "\n	}\n");
		
		//Fechar as mensagens de alerta e sucesso, para n�o interferir nos asserts
		f.format("\n	public void fecharMensagem () {"
				+ "\n		driver.findElement(By.cssSelector(\"div[class='jGrowl-close']\")).click();"
				+ "\n	}\n");
		
		f.format("\n	public boolean checkboxMarcarDesmarcarId (String idElemento, String marcarCheckbox) {"
				+ "\n		WebElement elementoCheckbox = driver.findElement(By.id(idElemento));"
				+ "\n		return marcarDesmarcarCheckbox(marcarCheckbox, elementoCheckbox);"
				+ "\n	}\n");
		
		f.format("\n	public boolean checkboxMarcarDesmarcarXpath (String xpathElemento, String marcarCheckbox) {"
				+ "\n		WebElement elementoCheckbox = driver.findElement(By.xpath(xpathElemento));"
				+ "\n		return marcarDesmarcarCheckbox(marcarCheckbox, elementoCheckbox);"
				+ "\n	}\n");
		
		f.format("\n	private boolean marcarDesmarcarCheckbox(String marcarCheckbox, WebElement elementoCheckbox) {"
				+ "\n		if (marcarCheckbox.toUpperCase().equals(\"SIM\")) {"
				+ "\n			if (!elementoCheckbox.isSelected()) {"
				+ "\n				elementoCheckbox.click();"
				+ "\n				return elementoCheckbox.isSelected();"
				+ "\n			}"
				+ "\n		return elementoCheckbox.isSelected();"
				+ "\n		} else {"
				+ "\n			if (elementoCheckbox.isSelected()) {"
				+ "\n				elementoCheckbox.click();"
				+ "\n				return !elementoCheckbox.isSelected();"
				+ "\n			}"
				+ "\n			return !elementoCheckbox.isSelected();"
				+ "\n		}"
				+ "\n	}\n");
		
		f.format("\n	public boolean validarCheckbox (String xpathCheckbox, String marcadoCheckbox) {"
				+ "\n		WebElement elementoCheckbox = driver.findElement(By.xpath(xpathCheckbox));"
				+ "\n\n		if (marcadoCheckbox.toUpperCase().equals(\"SIM\")) {"
				+ "\n			if (elementoCheckbox.isSelected()) {"
				+ "\n				return true;"
				+ "\n			}"
				+ "\n		} else {"
				+ "\n			if (!elementoCheckbox.isSelected()) {"
				+ "\n				return true;"
				+ "\n			}"
				+ "\n		}"
				+ "\n		return false;"
				+ "\n	}\n");
		
		f.format("\n	public boolean validarRemocaoDados (String xpathBtnRemover) {"
				+ "\n		try { "
				+ "\n			return !(driver.findElement(By.xpath(xpathBtnRemover)).isDisplayed());"
				+ "\n		} catch (Exception e) {"
				+ "\n			return  true;"
				+ "\n		}"
				+ "\n	}\n");
		
		f.format("\n	public void clicarBotaoCancelarModalCadastrar () {"
				+ "\n		driver.findElement(By.cssSelector(\"#cancelCreateAction\")).click();"
				+ "\n		aguardarTelaBloqueio();"
				+ "\n	}\n");

		f.format("\n	public void clicarBotaoCancelarModalEditar () {"
				+ "\n		driver.findElement(By.cssSelector(\"#cancelEditAction\")).click();"
				+ "\n		aguardarTelaBloqueio();"
				+ "\n	}\n");
		
		f.format("\n}");
	}
	
	private static void criarDiretorios (String localArquivo, String nomeSistema) {
		
		diretorioSalvarArquivos = localArquivo + "/src/";
		criarDiretorio(diretorioSalvarArquivos);
		
	    diretorioSalvarArquivos += "/"+nomeSistema+"/";
		criarDiretorio(diretorioSalvarArquivos);
		
	}
	
}
