package gapp.test;

import gapp.test.main.AbstractScript;

import java.util.Formatter;

public class ScriptsTests extends AbstractScript {
	
	public void headerScript (Formatter f, String nomeTabelaMestre, String nomeTabela, String sistema) {
		
		f.format("package "+sistema+"."+ nomeTabelaMestre.replaceAll("\\s+", "") +";\n"
				
				+"\n import static org.junit.Assert.assertTrue;"
				+"\n import org.junit.Ignore;" // TODO Remover ap�s todos os m�todos terem sidos implementados
				+"\n import org.junit.Test;"
				+"\n import org.junit.runners.MethodSorters;"
				+"\n import org.junit.FixMethodOrder;"
				+"\n import org.openqa.selenium.By;"
				+"\n import org.openqa.selenium.support.ui.Select;"
				
				+"\n import "+sistema+".FuncionalidadesCrud;"
				+"\n import "+sistema+".FuncionalidadesUteis;"
				+"\n import "+sistema+".FuncionalidadesFiltro;"
				+"\n import "+sistema+".MensagensRetornadas;"
				+"\n import "+sistema+".main.AbstractTest;");
				
//		obterTabelasDependentes();
//		importTabelasDependentes(f, sistema, nomeTabelaMestre.replaceAll("\\s+", ""));
		
		f.format("\n\n@FixMethodOrder(MethodSorters.NAME_ASCENDING)"
				+"\npublic class "+ nomeTabela.replace(" ", "") +" extends AbstractTest {");
		
		f.format("\n	static boolean paginaAcessada = false;");
		f.format("\n	static boolean cadastroSucesso = false; // Indica se um Teste de CADASTRO foi executado com sucesso.");
		f.format("\n	static boolean edicaoSucesso = false; // Indica se um Teste de EDI��O foi executado com sucesso.");
		f.format("\n	static boolean filtroSucesso = false; // Indica se o Teste de FILTRO foi executado com sucesso.\n");
	}
	
	public void acessarPagina (Formatter f) {
		
		if (tabelaMestre) {
			String texto = dadosTabela.get(colunaPath);
			menus = texto.split(" -> ");
		}
		
		f.format("\n	@Test\n");
		f.format("	public void acessarPagina () {\n");
		f.format("		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);");
		
		for (int i=0; i<menus.length; i++) {
			if (i+1 == menus.length) {
				f.format("\n		util.clicarMenu(\""+menus[i]+"\");");
			} else {
				f.format("\n		util.visualizarMenu(\""+menus[i]+"\");");
			}
		}
		
		// Caso o t�tulo da p�gina seja na navega��o, utiliza-se o m�todo "pegarTituloPaginaNavegacao", caso tenha um t�tulo realmente, utilize "pegarTituloPagina"
		f.format("\n		assertTrue(\" ERRO: N�o foi carregado a p�gina '"+tituloPaginaMestre+"'\", util.pegarTextoNavegacao(\""+sectionCodeMestre+"\").contains(\""+tituloPaginaMestre+"\"));"
				+ "\n\n		System.out.println(\"\\n P�gina '"+tituloPaginaMestre+"' acessada com sucesso.\");"
				+ "\n		paginaAcessada = true;");
				
		//Criando registro na tabela mestre data caso n�o houver
		if(!tabelaMestre){
		f.format("\n		//Tabela possui um tr, mesmo sem nenhum registro");
		f.format("\n		if(driver.findElements(By.xpath(\".//*[@id='"+sectionCodeMestre+"_grid']/tbody/tr\")).size() <= 1){");
		f.format("\n			"+sectionCodeMestre+" "+sectionCodeMestre.toLowerCase()+" = new "+sectionCodeMestre+"(driver);");
		f.format("\n			"+sectionCodeMestre.toLowerCase()+".adicionarRegistroCadastro();");
		f.format("\n		}");
		}
		f.format("\n	}\n" );
	}

//	private void obterTabelasDependentes () {
//		String[] parametrosTabelasDependentes;
//		tabelasDependentesBD.clear();
//		existeTabelaDependente = false;
//		
//		for (int i = 0; (i < valorColunaJoin.size()) && (!existeTabelaDependente) ; i++) {
//			if (!valorColunaJoin.get(i).equals("")) {
//				existeTabelaDependente = true;
//			}
//		}
//		
//		if (existeTabelaDependente) {
//			for (int i = 0; i < valorColunaJoin.size(); i++) {
//				if (!valorColunaJoin.get(i).equals("")) {
//					parametrosTabelasDependentes = valorColunaJoin.get(i).split("#");
//					tabelasDependentesBD.add(parametrosTabelasDependentes[0]);
//				}
//			}
//		}
//	}
//	
//	private void importTabelasDependentes(Formatter f, String sistema, String tabelaMestre) {
//		for (int dep = 0; dep < tabelasDependentesBD.size(); dep++) {
//			for (int bd = 0; bd < todosDBTableName.size(); bd++) {
//				if  ( (tabelasDependentesBD.get(dep).equals(todosDBTableName.get(bd))) && (!todosDatilOf.get(bd).equals(sectionCodeMestre)) )  {
//					// Importa apenas se tiver em pacote diferente da tabela atual.
//					f.format("\n import "+sistema+"."+todosDatilOf.get(bd)+"."+todosSectionCodeTabelas.get(bd)+";");
//					f.format("\n import "+sistema+"."+todosDatilOf.get(bd)+"."+todosSectionCodeTabelas.get(bd)+"Test;");
//				}
//			}
//		}
//	}
	
	public void importar (Formatter f) {
		f.format("\n	@Ignore"); // TODO Remover ap�s implementa��o
		f.format("\n	@Test"
				+"\n	public void importarRegistros() {"
				+"\n		// TODO Implementar teste de Importa��o"
				+"\n		System.out.println(\" Teste de IMPORTA��O executado ** N�O IMPLEMENTADO\");"
				+"\n	}\n");
	}
	
	public void exportar(Formatter f) {
		f.format("\n	@Ignore"); // TODO Remover ap�s implementa��o
		f.format("\n	@Test"
				+"\n	public void exportarRegistros() {"
				+"\n		// TODO Implementar teste de exporta��o"
				+"\n		System.out.println(\" Teste de EXPORTA��O executado ** N�O IMPLEMENTADO\");"
				+"\n	}\n");
	}

}
