package WriteCT;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Formatacoes.FormatacaoTable;

public class WriteExcelCasoDeTeste {

	public void criaSheetCT(XSSFWorkbook workbook) {
		// Criar a planilha
		Sheet sheetCT = null;
		sheetCT = workbook.createSheet("Casos de Testes");
		// Variavel para criar as linhas e celulas
		Row row;
		FormatacaoTable formatacao = new FormatacaoTable();

		// Criar a linha e celula com a formatação
		row = sheetCT.createRow(1);
		row.createCell(1).setCellValue("Projeto:");
		row.getCell(1).setCellStyle(formatacao.styleFundoVerdeRight(workbook));
		
		// Criar a linha e celula com a formatação
		row = sheetCT.createRow(2);
		row.createCell(1).setCellValue("Esp. Func.:");
		row.getCell(1).setCellStyle(formatacao.styleFundoVerdeRight(workbook));
		
		// Criar a linha e celula com a formatação
		row = sheetCT.createRow(4);
		row.setHeight((short)540);
		row.createCell(1).setCellValue("Grupo de Teste");
		row.getCell(1).setCellStyle(formatacao.styleFundoVerdeLeftMedium(workbook));
		sheetCT.autoSizeColumn(1); //Ajusta largura da coluna de acordo com o texto		
		
		// Criar a linha e celula com a formatação
		row.createCell(2).setCellValue("Funcionalidade");
		row.getCell(2).setCellStyle(formatacao.styleFundoVerdeCentralMedium(workbook));
		sheetCT.autoSizeColumn(2); //Ajusta largura da coluna de acordo com o texto
		
		row.createCell(3).setCellValue("Caminho");
		row.getCell(3).setCellStyle(formatacao.styleFundoVerdeCentralMedium(workbook));
		sheetCT.autoSizeColumn(3); //Ajusta largura da coluna de acordo com o texto
		
		row.createCell(4).setCellValue("Descrição");
		row.getCell(4).setCellStyle(formatacao.styleFundoVerdeCentralMedium(workbook));
		sheetCT.autoSizeColumn(4); //Ajusta largura da coluna de acordo com o texto
		
		row.createCell(5).setCellValue("Resultados Esperados");
		row.getCell(5).setCellStyle(formatacao.styleFundoVerdeCentralMedium(workbook));
		sheetCT.autoSizeColumn(5); //Ajusta largura da coluna de acordo com o texto
		
		row.createCell(6).setCellValue("Status");
		row.getCell(6).setCellStyle(formatacao.styleFundoVerdeCentralMedium(workbook));
		sheetCT.autoSizeColumn(6); //Ajusta largura da coluna de acordo com o texto
				
		row.createCell(7).setCellValue("Descrição do Problema ou melhoria");
		row.getCell(7).setCellStyle(formatacao.styleFundoVerdeRightMedium(workbook));
		sheetCT.setColumnWidth(7, 6000); //Ajusta largura da coluna de acordo com o texto
		
		//Congela coluna e linha
		sheetCT.createFreezePane(4, 5);
	}
}
