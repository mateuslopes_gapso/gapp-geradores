package gapp.test;

import gapp.GeradorValores;
import gapp.test.main.AbstractScript;

import java.util.Formatter;

public class ScriptsFiltrar  extends AbstractScript {

	String idTabelaDetalhe = dadosTabela.get(colunaSectionCode).toLowerCase();
	String valorFiltro;
	String xpathCheckbox; // xpath do elemento caso seja um Checkbox
	String idGrid; // ID do grid que é exibida a tabela (utilizado para Checkbox)
	String nomeColuna;
	
	public void filtrarBasicoTest (Formatter f, String nomeSessaoMestre) {
		metodoFiltrarRegistro(f, nomeSessaoMestre, "BÁSICO");
		f.format("\n		System.out.println(\" Teste de FILTROS executado\");"
				+ "\n		filtroSucesso = true;"
				+"\n	}\n");
	}
	
	public void filtrarAvancadoTest (Formatter f, String nomeSessaoMestre) {
		metodoFiltrarRegistro(f, nomeSessaoMestre, "AVANÇADO");
		f.format("\n		System.out.println(\" Teste de FILTROS AVANÇADOS executado\");"
				+ "\n		filtroSucesso = true;"
				+"\n	}\n");
	}

	private void metodoFiltrarRegistro(Formatter f, String nomeSessaoMestre, String basicoAvancado) {
		String nomeMetodoTest;
		String nomeMetodoFiltro;
		
		GeradorValores valores = new GeradorValores();
		
		if (basicoAvancado.equals("AVANÇADO")) {
			nomeMetodoTest = "filtrarRegistrosAvancado";
			nomeMetodoFiltro = "aplicarFiltroAvancado";
		} else {
			nomeMetodoTest = "filtrarBasicoRegistros";
			nomeMetodoFiltro = "aplicarFiltro";
		}
		
		f.format("\n	@Test"
				+"\n	public void "+nomeMetodoTest+"() {");
		
		f.format("\n		assertTrue(\"\\n ATENÇÃO: Não é possível executar o Teste porque a página não foi carregada.\", paginaAcessada);"
				+ "\n		assertTrue(\"\\n ATENÇÃO: O Teste não foi executado porque a edição pela tabela não teve sucesso e é utilizados os dados na edição.\", edicaoSucesso);"
				+ "\n		filtroSucesso = false;\n");
		
		f.format("\n		FuncionalidadesFiltro filtro = new FuncionalidadesFiltro(driver);");

		if (tabelaMestre) {
			f.format("\n		"+nomeSessaoMestre+" "+nomeSessaoMestre.toLowerCase()+" = new "+nomeSessaoMestre+"(driver);");
			
			// É no método filtrarBasicoRegistros que são cadatrados os registros utilizados para validar os filtros 
			if (basicoAvancado.equals("BÁSICO")) {
				f.format("\n\n		// Adiciona novos registros para validação dos filtros (Básico e Avançado)");
				f.format("\n		"+nomeSessaoMestre.toLowerCase()+".adicionarRegistrosFiltros();");
			}
			f.format("\n\n		"+nomeSessaoMestre.toLowerCase()+".clicarBotaoFiltrarMestre();");
		} else {
			f.format("\n		"+dadosTabela.get(colunaSectionCode)+ " "+idTabelaDetalhe+" = new "+dadosTabela.get(colunaSectionCode)+"(driver);");
			
			if (basicoAvancado.equals("BÁSICO")) {
				f.format("\n\n		// Adiciona novos registros para validação dos filtros (Básico e Avançado)");
				f.format("\n		"+dadosTabela.get(colunaSectionCode).toLowerCase()+".adicionarRegistrosFiltros();");
			}

			f.format("\n\n		"+idTabelaDetalhe+".clicarBotaoFiltrarDetalhe();\n");
		}

		valores.criarVariavelComboboxTabela(f, "FILTRO");
		f.format("\n");
		
		for (int i = 0; i < valorColunaId.size(); i++) {
			
			if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")){					
				valorFiltro = "c"+valorColunaId.get(i).toLowerCase();
			} else if (valorColunaEditavel.get(i).toUpperCase().equals("YES")) {
				valorFiltro = "\""+valoresEditadosTabela.get(i)+"\"";
			} else {
				valorFiltro = "\""+valoresCadastrados.get(i)+"\"";
			}
			
			String valorFiltro2 = null;
			if (valorColunaTipo.get(i).toUpperCase().equals("DATE")) {
				if (valorColunaId.get(i).toUpperCase().contains("INIT")){
					valorFiltro = "c"+valorColunaId.get(i).toLowerCase();
					if (valorColunaId.get(i+1).toUpperCase().contains("END")){
						valorFiltro2 = "c"+valorColunaId.get(i+1).toLowerCase();
					}else {
						GeradorValores gerarValor = new GeradorValores();
						valorFiltro2 = "\""+gerarValor.dataAtualMaisDias(10+i)+"\"";
					}
				}else if (valorColunaId.get(i).toUpperCase().contains("END")){
					valorFiltro = "c"+valorColunaId.get(i-1).toLowerCase();
					valorFiltro2 = "c"+valorColunaId.get(i).toLowerCase();
					if (basicoAvancado.equals("BÁSICO")){
						valorFiltro = "c"+valorColunaId.get(i).toLowerCase();
					}
				}				
			}
			
			if (valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")){
				if (valorFiltro.equals("\"SIM\"")) { // TODO remover esta condição IF após correção no sistema
					valorFiltro = "\"True\"";
				} else {
					valorFiltro = "\"False\"";
				}
				
				if (tabelaMestre) {
					idGrid = dadosTabela.get(colunaSectionCode)+"_grid";
				} else {
					idGrid = "details_"+sectionCodeMestre+"_"+dadosTabela.get(colunaSectionCode)+"_grid";
				}
				xpathCheckbox = "\".//*[@id='"+idGrid+"']/tbody/tr[2]/td["+(i+2)+"]/input\"";
			} else {
				xpathCheckbox = "null";
			}
	
			nomeColuna = valorColunaLabel.get(i).replace("%", "%%");
			f.format("\n		// Aplicar filtro na coluna "+nomeColuna);
			if (tabelaMestre) {
				if(basicoAvancado.equals("AVANÇADO")){
					if(valorColunaTipo.get(i).toUpperCase().equals("STRING") || 
							valorColunaTipo.get(i).toUpperCase().equals("SELECT")){
					f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Contém\");");
					f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Contém (Multi)\");");
					f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Igual a\");");
					f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Igual a (Multi)\");\n\n");
					}
					else if(valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")){
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", null,  "+valorFiltro2+", "+xpathCheckbox+", \"Todos\");");
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", \"True\",  "+valorFiltro2+", "+xpathCheckbox+", \"Marcado\");");
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", \"False\",  "+valorFiltro2+", "+xpathCheckbox+", \"Desmarcado\");\n\n");
					}else if(valorColunaTipo.get(i).toUpperCase().equals("DATE")){
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Intervalo\");");
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Após\");");
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Antes\");\n\n");
					}else if(valorColunaTipo.get(i).toUpperCase().equals("NUMERIC") || valorColunaTipo.get(i).toUpperCase().equals("INTEGER")){
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Igual a\");");
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Diferente de\");");
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Menor que\");");
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Menor ou igual a\");");
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Maior que\");");
						f.format("\n		filtro.aplicarFiltroAvancadoMestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Maior ou igual a\");\n\n");
					}
				}else {
					f.format("\n		filtro."+nomeMetodoFiltro+"Mestre(\""+nomeSessaoMestre+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+xpathCheckbox+");\n");
				}
			} else {
				if(basicoAvancado.equals("AVANÇADO")){
					if(valorColunaTipo.get(i).toUpperCase().equals("STRING") || 
							valorColunaTipo.get(i).toUpperCase().equals("SELECT")){
					f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Contém\");");
					f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Contém (Multi)\");");
					f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Igual a\");");
					f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Igual a (Multi)\");\n\n");
					}
					else if(valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")){
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Marcado\");");
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Desmarcado\");\n\n");
					}else if(valorColunaTipo.get(i).toUpperCase().equals("DATE")){
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Intervalo\");");
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Após\");");
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Antes\");\n\n");
					}else if(valorColunaTipo.get(i).toUpperCase().equals("NUMERIC") || valorColunaTipo.get(i).toUpperCase().equals("INTEGER")){
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Igual a\");");
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Diferente de\");");
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Menor que\");");
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Menor ou igual a\");");
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Maior que\");");
						f.format("\n		filtro.aplicarFiltroAvancadoDetalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+",  "+valorFiltro2+", "+xpathCheckbox+", \"Maior ou igual a\");\n\n");
					}
				}else {
					f.format("\n		filtro."+nomeMetodoFiltro+"Detalhe(\""+nomeSessaoMestre+"\", \""+dadosTabela.get(colunaFieldCodeDB)+"\", \""+valorColunaId.get(i)+"\", "+valorFiltro+", "+xpathCheckbox+");\n");
				}
			}
		}
	}
	
	public void metodoClicarBotaoFiltro (Formatter f, int nroTabelaDetalhes) {
		if (tabelaMestre) {
			f.format( "\n	public void clicarBotaoFiltrarMestre () {"
					+ "\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
					+ "\n		util.aguardarElementoXPath(\".//*[@id='gview_"+sectionCodeMestre+"_grid']/div/ul/li[4]/a[@id='btnFilterMaster']/i\");"
					+ "\n		driver.findElement(By.xpath(\".//*[@id='gview_"+sectionCodeMestre+"_grid']/div/ul/li[4]/a[@id='btnFilterMaster']/i\")).click();"
					+ "\n	}\n");
 		} else {
 			f.format( "\n	public void clicarBotaoFiltrarDetalhe () {"
 					+ "\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
					+ "\n		util.aguardarElementoXPath(\".//*[@id='gview_details_"+sectionCodeMestre+"_"+dadosTabela.get(colunaSectionCode)+"_grid']/div/ul/li/a[@id='btnFilterDetail_"+nroTabelaDetalhes+"']/i\");"
					+ "\n		driver.findElement(By.xpath(\".//*[@id='gview_details_"+sectionCodeMestre+"_"+dadosTabela.get(colunaSectionCode)+"_grid']/div/ul/li/a[@id='btnFilterDetail_"+nroTabelaDetalhes+"']/i\")).click();"
 					+ "\n	}\n");
 		}
	}
	
//	private void cadastrarRegistrosParaFiltros(Formatter f, String tabela) {
//		GeradorValores valores = new GeradorValores();
//		
//		f.format("\n\n		// Adiciona novos registros para validação dos filtros (Básico e Avançado)");
//		
//		f.format("\n		"+tabela+".adicionarRegistroValido(2, ");
//		valores.preencherValoresParaCadastro(f, valoresCadastradosFiltro1);
//		f.format(");");
//		
//		f.format("\n		"+tabela+".adicionarRegistroValido(3, ");
//		valores.preencherValoresParaCadastro(f, valoresCadastradosFiltro2);
//		f.format(");\n");
//	}
}
