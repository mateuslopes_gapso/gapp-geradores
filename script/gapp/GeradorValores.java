package gapp;

import gapp.test.main.AbstractScript;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Formatter;

public class GeradorValores extends AbstractScript {

	public void preencherValoresParaCadastro (Formatter f, ArrayList<String> valores) {
		if(tabelaMestre){
			valoresCadastradosMestre.clear();
		}
		for (int i = 0; i < valores.size(); i++) {
			if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")) {
				f.format(valores.get(i));
				if(tabelaMestre){
					valoresCadastradosMestre.add(valores.get(i));
				}
				
			} else {
				f.format("\""+valores.get(i)+"\"");		
				if(tabelaMestre){
					valoresCadastradosMestre.add("\""+valores.get(i)+"\"");
				}
			}
			if ( i <valores.size()-1 ) {
				f.format(", ");
				if(tabelaMestre){
					valoresCadastradosMestre.add(", ");
				}
			}			
		}
	}
	
	public void preencherValoresParaEditar (Formatter f, ArrayList<String> valores) {
		boolean paramentroPreenchido =  false;
		for (int i = 0; i < valores.size(); i++) {
			if (valorColunaEditavel.get(i).toUpperCase().equals("YES") ) {
				if (paramentroPreenchido) {
					f.format(", ");
				}
				if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")) {
					f.format(valores.get(i));
				} else {
					f.format("\""+valores.get(i)+"\"");
				}
				paramentroPreenchido = true;
			}
		}
	}
	
	public void gerarValoresParaCadastro () {
		String[] regra = null;
		
		// Valores para cadastrar o registro principal
		String texto;
		String numero;
		String numeroErro;
		String inteiro; 
		String inteiroErro;
		
		// Valores para cadastrar o registro para validar o filtro
		String textoFiltro1 = "WW";
		String textoFiltro2 = "ZZ";
		String numeroFiltro1;
		String numeroFiltro2;
		
		valoresCadastrados.clear();
		valoresCadastradosFiltro1.clear();
		valoresCadastradosFiltro2.clear();
		valoresInvalidos.clear();
		
		for (int i=0; i<valorColunaId.size(); i++) {
			
			texto = "";
			numero = "";
			inteiro = "10";
			numeroErro = "";
			inteiroErro = "";
			
			// Gera os valores conforme o Tipo e Regra de cada coluna
			if (!valorColunaDeRegra.get(i).isEmpty()) {
				regra = valorColunaDeRegra.get(i).split("#");
				if (regra[0].equals("string")) {
					for (int m = 1; m < Integer.parseInt(regra[2]) ; m++) {
						texto += "A";
					}
					texto += "B"; // O ultimo caracter no texto
				} else if (regra[0].equals("spin")) {
					if (regra.length == 1) {
						numero = "-10,97";
						inteiro = "-10";
						numeroErro = "ERRO";
						inteiroErro = "ERRO";
					} else if (regra.length == 2) {
						numero = Integer.toString(Integer.parseInt(regra[1])+100+i)+",97";
						inteiro = Integer.toString(Integer.parseInt(regra[1])+100+i);
						numeroErro = Integer.toString(Integer.parseInt(regra[1])-1000+i);
						inteiroErro = numeroErro;
					} else {
						numero = Integer.toString(Integer.parseInt(regra[2])-1);
						inteiro = Integer.toString(Integer.parseInt(regra[2])-1);
						numeroErro = Integer.toString(Integer.parseInt(regra[2])+1);
						inteiroErro = numeroErro;
					}
				}
			} else if (valorColunaTipo.get(i).toUpperCase().equals("STRING")) {
				texto = "AATEXTOCADASTRO"+i; 
				qtdeCampoSemRegra++;
			} else if (valorColunaTipo.get(i).toUpperCase().equals("NUMERIC")) {
				numero = Integer.toString(100+i)+",97";
				numeroErro = "ERRO";
				qtdeCampoSemRegra++;
			} else if (valorColunaTipo.get(i).equals("INTEGER")) {
				inteiro = Integer.toString(100+i);
				inteiroErro = "ERRO";
				qtdeCampoSemRegra++;
			}
			
			numeroFiltro1 = Integer.toString(Integer.parseInt(inteiro)-1);
			numeroFiltro2 = Integer.toString(Integer.parseInt(inteiro)+1);

			// Adiciona os valores gerados no Array.
			if (valorColunaTipo.get(i).toUpperCase().equals("STRING")) {
				valoresCadastrados.add(texto);
				valoresCadastradosFiltro1.add(textoFiltro1);
				valoresCadastradosFiltro2.add(textoFiltro2);
				valoresInvalidos.add(texto+"_ERRO");
			}
			else if (valorColunaTipo.get(i).toUpperCase().equals("NUMERIC")) {
				valoresCadastrados.add(numero);
				valoresCadastradosFiltro1.add(numeroFiltro1);
				valoresCadastradosFiltro2.add(numeroFiltro2);
				valoresInvalidos.add(numeroErro);
			}
			else if (valorColunaTipo.get(i).equals("INTEGER")) {
				valoresCadastrados.add(inteiro);
				valoresCadastradosFiltro1.add(numeroFiltro1);
				valoresCadastradosFiltro2.add(numeroFiltro2);
				valoresInvalidos.add(inteiroErro);
			}
			else if (valorColunaTipo.get(i).toUpperCase().equals("DATE")) {
				valoresCadastrados.add(dataAtualMaisDias(15+i));
				valoresCadastradosFiltro1.add(dataAtualMaisDias(5+i));
				valoresCadastradosFiltro2.add(dataAtualMaisDias(25+i));
				valoresInvalidos.add(Integer.toString(10+i)+"/40/9999");
			}
			else if (valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")) {
				valoresCadastrados.add("SIM");
				valoresCadastradosFiltro1.add("N�O");
				valoresCadastradosFiltro2.add("N�O");
				valoresInvalidos.add("SIM");
			}
			else if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")) {
				valoresCadastrados.add("0");
				valoresCadastradosFiltro1.add("1");
				valoresCadastradosFiltro2.add("1");
				valoresInvalidos.add("0");
			}
		}
	}
	
	public void gerarValoresParaEditar () {
		
		ArrayList<String> valores = new ArrayList<>();
		String caracter; // Caracter utilizado no texto
		String texto; // Texto que ser� utilizado para preencher um campo do tipo Texto
		String valorNumero = ""; // N�mero (float) que ser� utilizado para preencher um campo do tipo Num�rico
		String valorInteiro = ""; // N�mero (inteiro) que ser� utilizado para preencher um campo do tipo Integer
		String local; // Local que ser� editado (Modal ou pela Tabela)
		int qtde; // Quantidade que ser� alterado para a vari�vel tipo num�rico
		String comboboxIndex; // Index que ser� selecionado em um combobox
		String checkbox; // Informa se o checkbox ser� Marcado ou Desmarcado
		int diaInicial; // Indica qual ser� o DIA inicial para gerar o valor dos campos tipo Data
		String[] regra;
		
		valoresEditadosModal.clear();
		valoresEditadosTabela.clear();
		
		local = "MODAL";
		// FOR para gerar os valores que ser�o utilizados durante a edi��o pelo MODAL e pela TABELA
		for (int i = 0; i < 2; i++) {
			valores.clear();
			texto = "";
			
			if (local.equals("MODAL")) {
				caracter = "M";
				qtde = 2;
				comboboxIndex = "1";
				checkbox = "N�O";
				diaInicial = 15;
			} else { 
				caracter = "T";
				qtde = 3;
				comboboxIndex = "2";
				checkbox = "SIM";
				diaInicial = 20;
			}
			
			// Gerar os valores conforme o TIPO e o N�MERO de caractesres permitidos
			for (int c=0; c<valorColunaId.size(); c++) {
				if (valorColunaEditavel.get(c).toUpperCase().equals("YES") ) {
					
					if (!valorColunaDeRegra.get(c).isEmpty()) {
						regra = valorColunaDeRegra.get(c).split("#");
						if (regra[0].equals("string")) {
							for (int m = 1; m < Integer.parseInt(regra[2]) ; m++) {
								texto += caracter;
							}
							texto += "E"; // O ultimo caracter no texto
						} else if (regra[0].equals("spin")) {
							if (regra.length == 1) {
								valorNumero = "-10,25";
								valorInteiro = "-10";
							} else if (regra.length == 2) {
								valorNumero = Integer.toString(Integer.parseInt(regra[1])+qtde+i+c+100)+",35";;
								valorInteiro = Integer.toString(Integer.parseInt(regra[1])+qtde+i+c+100);
							} else {
								valorNumero = Integer.toString(Integer.parseInt(regra[2])-qtde-i-c)+",45";
								valorInteiro = Integer.toString(Integer.parseInt(regra[2])-qtde-i-c);
							}
						}
					} else if (valorColunaTipo.get(c).toUpperCase().equals("STRING")) {
						texto = "TEXTOEDITADO"+local+c;
						qtdeCampoSemRegra++;
					} else if (valorColunaTipo.get(c).toUpperCase().equals("NUMERIC")) {
						valorNumero = Integer.toString(100+c)+",55";
						qtdeCampoSemRegra++;
					} else if (valorColunaTipo.get(c).equals("INTEGER")) {
						valorInteiro = Integer.toString(100+c);
						qtdeCampoSemRegra++;
					} 
					
					if (valorColunaTipo.get(c).toUpperCase().equals("STRING")) {
						valores.add(texto);
					}
					else if ( (valorColunaTipo.get(c).toUpperCase().equals("NUMERIC")))  {
						valores.add(valorNumero);
					}
					else if (valorColunaTipo.get(c).equals("INTEGER")) {
						valores.add(valorInteiro);
					}
					else if (valorColunaTipo.get(c).toUpperCase().equals("DATE")) {
						valores.add(dataAtualMaisDias(diaInicial+c));
					}
					else if (valorColunaTipo.get(c).toUpperCase().equals("CHECKBOX")) {
						valores.add(checkbox);
					}
					else if (valorColunaTipo.get(c).toUpperCase().equals("SELECT")) {
						valores.add(comboboxIndex);
					} else {
						valores.add("TIPO INV�LIDO");
					}
				
				} else {
					valores.add("X"); // Caso a coluna n�o seja edit�vel
				}
			}
			
			// Armazenar os valores que ser�o utlizados durante a Valida��o
			for (int v = 0; v < valores.size(); v++) {
				
				if (valores.get(v).equals("X")) {
					if (local.equals("MODAL")) {
						valoresEditadosModal.add(valoresCadastrados.get(v));
					} else {
						valoresEditadosTabela.add(valoresCadastrados.get(v));
					}
				} else {
					if (local.equals("MODAL")) {
						valoresEditadosModal.add(valores.get(v));
					} else {
						valoresEditadosTabela.add(valores.get(v));
					}
				}
			}
			local = "TABELA";
		}
	}
	
	public void criarVariavelComboboxModal (Formatter f) {
		// Cria uma vari�vel com o conte�do do Combobx ap�s selecionar a op��o desejada
		for (int i = 0; i < valorColunaTipo.size(); i++) {
			if (valorColunaTipo.get(i).equals("SELECT")) {
				f.format("\n		String c"+valorColunaId.get(i).toLowerCase()+";");
				f.format("\n		Select sel"+valorColunaId.get(i)+" = new Select(driver.findElement(By.id(\""+valorColunaId.get(i)+"\")));");
				f.format("\n		c"+valorColunaId.get(i).toLowerCase()+" = sel"+valorColunaId.get(i)+".getFirstSelectedOption().getText();\n");
			}
		}
	}

	public void criarVariavelComboboxTabela (Formatter f, String operacao) {
		// Cria uma vari�vel com o valor do Combobx ap�s selecionar a op��o desejada	
		String idGrid;
		if (tabelaMestre) {
			idGrid = dadosTabela.get(colunaSectionCode)+"_grid";
		} else {
			idGrid = "details_"+sectionCodeMestre+"_"+dadosTabela.get(colunaSectionCode)+"_grid";
		}
		
		for (int i = 0; i < valorColunaTipo.size(); i++) {
			if (valorColunaTipo.get(i).equals("SELECT")) {
				f.format("\n		// Armazena o valor da coluna '"+valorColunaLabel.get(i)+"'");
				f.format("\n		String c"+valorColunaId.get(i).toLowerCase()+" = "
						+ "driver.findElement(By.xpath(\".//*[@id='"+idGrid+"']/tbody/tr[2]/td["+(i+2)+"]\")).getText();");
			}else if (valorColunaTipo.get(i).equals("DATE") && operacao.toUpperCase().equals("FILTRO")) {//TODO para filtro intervalo avan�ado. Evelyn
				f.format("\n		// Armazena o valor da coluna '"+valorColunaLabel.get(i)+"'");
				f.format("\n		String c"+valorColunaId.get(i).toLowerCase()+" = "
						+ "driver.findElement(By.xpath(\".//*[@id='"+idGrid+"']/tbody/tr[2]/td["+(i+2)+"]\")).getText();");
			}
		}
	}
	
	public void preencherValoresSalvos (Formatter f, ArrayList<String> valores) {
		for (int i = 0; i < valores.size(); i++) {
			if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")) {
				f.format("c"+valorColunaId.get(i).toLowerCase());
			} else {
				f.format("\""+valores.get(i)+"\"");
			}
			
			if ( i < valores.size()-1 ) {
				f.format(", ");
			}
		}
	}
	
	public void preencherValoresNulosCadastrar (Formatter f) {
		for (int i = 0; i < valorColunaTipo.size(); i++) {
			if (valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")) {
				f.format("\"SIM\"");
			}  else if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")) {
				f.format("0");
			} else {
				f.format("\"\"");
			}
			
			if ( i < valorColunaTipo.size()-1 ) {
				f.format(", ");
			}
		}
	}
	
	public void preencherValoresNulosEditar (Formatter f) {
		boolean paramentroPreenchido =  false;
		for (int i = 0; i < valorColunaTipo.size(); i++) {
			if (valorColunaEditavel.get(i).toUpperCase().equals("YES") ) {
				if (paramentroPreenchido) {
					f.format(", ");
				}
				if (valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")) {
					f.format("\"SIM\"");
				}  else if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")) {
					f.format("0");
				} else {
					f.format("\"\"");
				}
				paramentroPreenchido = true;
			}
		}
	}

	public String dataAtualMaisDias (int diasSomados) {
		
		Calendar c = Calendar.getInstance();
		c.setTime(dataAtual);
		c.add(Calendar.DATE, +diasSomados);
		
		return formatoData.format(c.getTime());
	}
}
