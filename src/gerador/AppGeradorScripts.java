package gerador;

import gapp.GeradorScript;
import gapp.funcionalidades.ScriptFuncCRUD;
import gapp.funcionalidades.ScriptFuncFiltro;
import gapp.funcionalidades.ScriptFuncUteis;
import gapp.funcionalidades.ScriptMsgRetornadas;
import gapp.test.ScriptPaginaLogin;
import gapp.test.ScriptsMain;
import gapp.test.main.AbstractScript;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Date;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import ReadDetalhesProjeto.ReadExcelProjeto;
import ReadDetalhesProjeto.ReadFileDiretorio;

public class AppGeradorScripts extends JFrame {

	private static String diretorioArquivos = "\\ArquivosGerados";
	private static boolean CTcriadoComSucesso = false;
	private static boolean arquivosCTValidos = false;
	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppGeradorScripts frame = new AppGeradorScripts();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null); // Centraliza a tela
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public File[] arquivoCT = null;
	private boolean arquivoCTInformado = false;
	public File arquivoDados = null;
	private boolean arquivoDadosInformado = false;
	public JFileChooser chooser = new JFileChooser();
	private JTextField textLocalSalvar = new JTextField();
	private JTextField textArquivoDados;
	private JTextField textNomeSistema;
	private boolean nomeSistemaInformado = false;
	
	public AppGeradorScripts() {
		
		setTitle("Gerador de Scritps de Testes");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 465, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		final JButton btnGerarScripts = new JButton("Gerar Scripts");
		btnGerarScripts.setEnabled(false);
		btnGerarScripts.setFont(new Font("Tahoma", Font.PLAIN, 10));
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		final JButton btnGerarCTScripts = new JButton("Gerar Casos de Testes + Scripts");
		btnGerarCTScripts.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnGerarCTScripts.setEnabled(false);

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 424, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(panel_1, 0, 0, Short.MAX_VALUE)
							.addGap(13))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(panel, GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE)
							.addGap(13))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 426, Short.MAX_VALUE)
							.addGap(13))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnGerarCTScripts, GroupLayout.PREFERRED_SIZE, 426, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(13, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnGerarScripts, GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE)
							.addGap(13))))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnGerarCTScripts, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnGerarScripts, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(98, Short.MAX_VALUE))
		);
		panel_3.setLayout(null);
		
		textNomeSistema = new JTextField();
		// Restrige entradas de alguns caractesres no campo "Sigla do Sistema"
		textNomeSistema.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped (KeyEvent ev) {
				String caracteres="0987654321 -!@#$%�&*()_+`{^}<>:=[]|�~.,:;������������������������������";
				if(     (caracteres.contains(ev.getKeyChar()+"")) || (textNomeSistema.getText().length() > 15) ){
		              ev.consume();
				}
			}
		});

		// Habilitar o bot�o [Gerar Scripts] apenas ao preencher o campo "Sigla do Sistema"
		textNomeSistema.getDocument().addDocumentListener(new DocumentListener() {
		    @Override
		    public void insertUpdate(DocumentEvent e) { validaQtdeCaracteres(); }

		    @Override
		    public void removeUpdate(DocumentEvent e) { validaQtdeCaracteres(); }

		    @Override
		    public void changedUpdate(DocumentEvent e) { validaQtdeCaracteres(); }
		    
			public void validaQtdeCaracteres () {
			if (textNomeSistema.getText().length()>2) {
				nomeSistemaInformado = true;
				
				if (arquivoDadosInformado) {
					btnGerarCTScripts.setEnabled(true);
				}
				if (arquivoDadosInformado && arquivoCTInformado) {
					btnGerarScripts.setEnabled(true);
				}
			} else if (textNomeSistema.getText().length()<=2) {
				nomeSistemaInformado = false;
				btnGerarScripts.setEnabled(false);
				btnGerarCTScripts.setEnabled(false);
			}
		}
		});
		
		textNomeSistema.setBounds(99, 8, 315, 19);
		textNomeSistema.setFont(new Font("Tahoma", Font.PLAIN, 10));
		textNomeSistema.setColumns(10);
		panel_3.add(textNomeSistema);
		
		JLabel lblNomeDoSistema = new JLabel("Sigla do Sistema:");
		lblNomeDoSistema.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNomeDoSistema.setBounds(11, 10, 90, 14);
		panel_3.add(lblNomeDoSistema);
		
		JLabel lblScriptSalvos = new JLabel("Os arquivos ser\u00E3o salvos em:");
		lblScriptSalvos.setFont(new Font("Tahoma", Font.PLAIN, 10));
		
		textLocalSalvar.setFont(new Font("Tahoma", Font.PLAIN, 10));
		textLocalSalvar.setEditable(false);
		textLocalSalvar.setBackground(UIManager.getColor("Button.background"));
		textLocalSalvar.setColumns(10);
		
		final JButton btnAlterar = new JButton("Alterar");
		btnAlterar.setEnabled(false);
		btnAlterar.setFont(new Font("Tahoma", Font.PLAIN, 10));
		
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
					JFileChooser chooser = new JFileChooser();
					chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int retorno = chooser.showOpenDialog(AppGeradorScripts.this);
					
					if (retorno == JFileChooser.APPROVE_OPTION) {
						
						textLocalSalvar.setText(((chooser.getSelectedFile() != null) ? chooser .getSelectedFile().getAbsolutePath()+diretorioArquivos : "N�o selecionado"));
						
						if (nomeSistemaInformado) {
							if (arquivoCTInformado && arquivoDadosInformado) {
								btnGerarScripts.setEnabled(true);
							} else if (arquivoDadosInformado) {
								btnGerarCTScripts.setEnabled(true);
							}
						}
					}
				}
		});
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(lblScriptSalvos, GroupLayout.PREFERRED_SIZE, 179, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(textLocalSalvar, GroupLayout.PREFERRED_SIZE, 319, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnAlterar, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(15, Short.MAX_VALUE))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(4)
					.addComponent(lblScriptSalvos)
					.addGap(2)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(textLocalSalvar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnAlterar, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_2.setLayout(gl_panel_2);
		
		JButton btnSelecionarArquivoDados = new JButton("Selecionar");
		btnSelecionarArquivoDados.setFont(new Font("Tahoma", Font.PLAIN, 10));
		
		btnSelecionarArquivoDados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (  !chooser.getFileFilter().toString().contains("Microsoft Excel")    ) {
					chooser.addChoosableFileFilter(new FileNameExtensionFilter("Microsoft Excel", "xls", "xlsx"));
				}
			    chooser.setAcceptAllFileFilterUsed(false);
				chooser.setMultiSelectionEnabled(false);
				chooser.setDialogTitle("Selecione o arquivo com as colunas das tabelas");
				
				int option = chooser.showOpenDialog(AppGeradorScripts.this);
				
				if (option == JFileChooser.APPROVE_OPTION) {
					
					arquivoDados = chooser.getSelectedFile();
					textArquivoDados.setText(arquivoDados.getName());

					textLocalSalvar.setText(arquivoDados.getParentFile().toString()+diretorioArquivos);

					btnAlterar.setEnabled(true);
					arquivoDadosInformado = true;
					
					if (nomeSistemaInformado) {
						btnGerarCTScripts.setEnabled(true);
					}
				}
			}
		});
		
		textArquivoDados = new JTextField();
		textArquivoDados.setEditable(false);
		textArquivoDados.setFont(new Font("Tahoma", Font.PLAIN, 10));
		textArquivoDados.setBackground(UIManager.getColor("Button.background"));
		textArquivoDados.setColumns(10);
		
		JLabel lblArquivoComAs = new JLabel("Arquivo com os dados das Tabelas:");
		lblArquivoComAs.setFont(new Font("Tahoma", Font.PLAIN, 10));
		
		JLabel lblObsAPlanilha = new JLabel("Obs.: o nome da planilha (sheet) com os dados deve iniciar com '"+GeradorScript.prefixSheet+"'.");
		lblObsAPlanilha.setForeground(Color.GRAY);
		lblObsAPlanilha.setFont(new Font("Tahoma", Font.ITALIC, 9));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
							.addComponent(btnSelecionarArquivoDados)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(textArquivoDados, GroupLayout.DEFAULT_SIZE, 317, Short.MAX_VALUE))
						.addComponent(lblObsAPlanilha, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
						.addComponent(lblArquivoComAs))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(4)
					.addComponent(lblArquivoComAs)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSelecionarArquivoDados, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
						.addComponent(textArquivoDados, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblObsAPlanilha, GroupLayout.PREFERRED_SIZE, 13, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(21, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		
		JButton btnSelecionarArquivosCTs = new JButton("Selecionar");
		btnSelecionarArquivosCTs.setFont(new Font("Tahoma", Font.PLAIN, 10));
		
		JLabel lblCasosTestes = new JLabel("Casos de Testes selecionados:");
		lblCasosTestes.setFont(new Font("Tahoma", Font.PLAIN, 10));
		
		JScrollPane scrollPane = new JScrollPane();
		
		final JList list = new JList();
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setFont(new Font("Tahoma", Font.PLAIN, 10));
		list.setVisibleRowCount(-1);
		list.setValueIsAdjusting(true);
		list.setLayoutOrientation(JList.VERTICAL_WRAP);
		list.setBorder(null);
		list.setBackground(UIManager.getColor("Button.background"));
		scrollPane.setViewportView(list);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnSelecionarArquivosCTs)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblCasosTestes))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(9)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(7)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSelecionarArquivosCTs, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCasosTestes))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		
		btnSelecionarArquivosCTs.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
//				if (  !chooser.getFileFilter().toString().contains("Microsoft Excel")    ) {
//					chooser.addChoosableFileFilter(new FileNameExtensionFilter("Microsoft Excel", "xls", "xlsx"));
//				}
			    chooser.setAcceptAllFileFilterUsed(false);
				chooser.setMultiSelectionEnabled(true);
				chooser.setDialogTitle("Selecione o(s) arquivo(s) de Caso de Testes");
				
				int option = chooser.showOpenDialog(AppGeradorScripts.this);
				
				if (option == JFileChooser.APPROVE_OPTION) {
					
					arquivoCT = chooser.getSelectedFiles();

					String[] listaArquivo = new String[arquivoCT.length];
					for (int i = 0; i < arquivoCT.length; i++) {
						listaArquivo[i] = "   "+arquivoCT[i].getName()+"   ";
					}
					
					list.setListData(listaArquivo);					
					arquivoCTInformado = true;
					
					if (arquivoDadosInformado && nomeSistemaInformado) {
						btnGerarScripts.setEnabled(true);
					}
				}
			}
		});
		contentPane.setLayout(gl_contentPane);
		
		btnGerarScripts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AbstractScript.zerarQtdeLog();
				
				Date dataInicioGerador;
				Date dataTerminoGerador;
				
				dataInicioGerador = new Date(System.currentTimeMillis());

				AbstractScript.criarDiretorio(textLocalSalvar.getText());
				
				gerarScriptsDeTestes();
				
				dataTerminoGerador = new Date(System.currentTimeMillis());
				
				LogResultado.criarLogResultado(textLocalSalvar.getText(), dataInicioGerador, dataTerminoGerador);
				
				mensagemRetornoUsuario();
			}
		});
		
		btnGerarCTScripts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AbstractScript.zerarQtdeLog();
				
				Date dataInicioGerador;
				Date dataTerminoGerador;
				
				dataInicioGerador = new Date(System.currentTimeMillis());

				AbstractScript.criarDiretorio(textLocalSalvar.getText());
				
				gerarCasosDeTestes();
				if (CTcriadoComSucesso) {
					gerarScriptsDeTestes();
				}
				
				dataTerminoGerador = new Date(System.currentTimeMillis());

				LogResultado.criarLogResultado(textLocalSalvar.getText(), dataInicioGerador, dataTerminoGerador);
				
				mensagemRetornoUsuario();
				
			}
		});
		
	}
	
	private void gerarCasosDeTestes () {
		
		try {
			CTcriadoComSucesso = false;
			String diretorioSalvarCT = textLocalSalvar.getText()+"/CasosDeTestes/";
			AbstractScript.criarDiretorio(diretorioSalvarCT);
			
			System.out.println("Gerando Casos de Testes...");
			ReadExcelProjeto readProjetoWriteCt = new ReadExcelProjeto();
			readProjetoWriteCt.readProjetoWriteCT(diretorioSalvarCT, arquivoDados, textNomeSistema.getText().toUpperCase());
			
			ReadFileDiretorio readFiles = new ReadFileDiretorio();
			arquivoCT = readFiles.readFiles(diretorioSalvarCT);
			
			System.out.println("Casos de Testes criados.\n");
			CTcriadoComSucesso = true;
			
		} catch (Exception e) {
			AbstractScript.qtdeCTErro++;
			System.out.println("N�o foi poss�vel gerar os Casos de Testes. \nFavor verificar os dados no arquivo.\n");
			JOptionPane.showMessageDialog(null, "N�o foi poss�vel gerar os Casos de Testes. \nFavor verificar os dados no arquivo.", "", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void gerarScriptsDeTestes () {
		try {
			String diretorioSalvarScripts = textLocalSalvar.getText()+"/ScriptsDeTestes/";
			
			arquivosCTValidos = GeradorScript.validarArquivoCT(arquivoCT);
			AbstractScript.arquivosCTValidos = arquivosCTValidos;
			
			if (!arquivosCTValidos) {
				throw new Exception();
			}
			
			AbstractScript.criarDiretorio(diretorioSalvarScripts);
			
			System.out.println("Criando scripts de testes...");
			GeradorScript.criarScriptsDeTestes(diretorioSalvarScripts, arquivoCT, arquivoDados, textNomeSistema.getText().toLowerCase());
			
			System.out.println("Criando scripts auxiliares...");
			ScriptsMain.criarAbstractTest(diretorioSalvarScripts, textNomeSistema.getText().toLowerCase());
			ScriptFuncCRUD.criarFuncionalidadesCrud(diretorioSalvarScripts, textNomeSistema.getText().toLowerCase());
			ScriptFuncUteis.criarFuncionalidadesUteis(diretorioSalvarScripts, textNomeSistema.getText().toLowerCase());
			ScriptFuncFiltro.criarFuncionalidadesFiltro(diretorioSalvarScripts, textNomeSistema.getText().toLowerCase());
			ScriptPaginaLogin.criarPaginaLogin(diretorioSalvarScripts, textNomeSistema.getText().toLowerCase());
			ScriptMsgRetornadas.criarMensagensRetornadas(diretorioSalvarScripts, textNomeSistema.getText().toLowerCase());
			
			System.out.println("Scripts de testes e auxiliares criados.\n");
			
		} catch (Exception e) {
			if (arquivosCTValidos) {
				System.out.println("N�o foi poss�vel gerar os Scripts de Testes. \nFavor verificar os dados nos arquivos informados.\n");
				JOptionPane.showMessageDialog(null, "N�o foi poss�vel gerar os Casos de Testes. \nFavor verificar os dados nos arquivos informados.", "", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private void mensagemRetornoUsuario() {
		if (arquivosCTValidos) {
			if (AbstractScript.qtdeScriptErro > 0) {
				System.out.println("Processo finalizado com alguns erros! \nFavor verificar os Logs.");
				JOptionPane.showMessageDialog(null, "Processo finalizado com alguns erros! \nFavor verificar os Logs.", "", JOptionPane.WARNING_MESSAGE);
			} else {
				System.out.println("Processo finalizado com Sucesso!");
				JOptionPane.showMessageDialog(null, "Processo finalizado com Sucesso!", "", 1);
			}
		} else {
			System.out.println("Um dos arquivos de Casos de Testes informados n�o est� conforme o modelo.\n");
			JOptionPane.showMessageDialog(null, "Um dos arquivos de Casos de Testes informados n�o est� conforme o modelo.\n", "", JOptionPane.ERROR_MESSAGE);
		}
	}
}
