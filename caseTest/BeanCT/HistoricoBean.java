package BeanCT;

public class HistoricoBean {
	private String menuAcesso;
	private String tabMestre;
	private String tabDetalheSection;
	private String tabDetalheIndex;
	
	public String getMenuAcesso() {
		return menuAcesso;
	}
	public void setMenuAcesso(String menuAcesso) {
		this.menuAcesso = menuAcesso;
	}
	public String getTabMestre() {
		return tabMestre;
	}
	public void setTabMestre(String tabMestre) {
		this.tabMestre = tabMestre;
	}
	public String getTabDetalheIndex() {
		return tabDetalheIndex;
	}
	public void setTabDetalheIndex(String tabDetalheIndex) {
		this.tabDetalheIndex = tabDetalheIndex;
	}
	public String getTabDetalheSection() {
		return tabDetalheSection;
	}
	public void setTabDetalheSection(String tabDetalheSection) {
		this.tabDetalheSection = tabDetalheSection;
	}
}
