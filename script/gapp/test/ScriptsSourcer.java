package gapp.test;

import gapp.test.main.AbstractScript;

import java.util.Formatter;

public class ScriptsSourcer extends AbstractScript {

	public void headerScript (Formatter f, String nomeTabelaMestre, String nomeTabela, String sistema) {
		
		f.format("package "+sistema+"."+ nomeTabelaMestre.replaceAll("\\s+", "") +";\n"
				
				+"\nimport static org.junit.Assert.assertTrue;"
				+ "\nimport "+sistema+".FuncionalidadesCrud;"
				+ "\nimport "+sistema+".FuncionalidadesUteis;"
				+ "\nimport "+sistema+".MensagensRetornadas;"
				+ "\n\nimport org.openqa.selenium.By;"
				+ "\nimport org.openqa.selenium.Keys;"
				+"\nimport org.openqa.selenium.WebDriver;"
				+ "\nimport org.openqa.selenium.support.ui.Select;"
				+ "\nimport org.openqa.selenium.NoSuchElementException;"
				+ "\nimport java.util.ArrayList;"
				+ "\n");
		
		obterTabelasDependentes();
		importTabelasDependentes(f, sistema, nomeTabelaMestre.replaceAll("\\s+", ""));
				
		f.format("\n\n  public class "+ nomeTabela.replace(" ", "") +" {"				
				+"\n\n		private WebDriver driver = null;"
				+"\n		public "+ nomeTabela.replace(" ", "") +" (WebDriver driver) {"
				+"\n		this.driver = driver;"
				+"\n	}\n");		
	}
	
	public void metodosTabelaMestre (Formatter f) {
		
		int posicaoBotao = 1; // Indica qual a posi��o dos bot�es no registro da tabela: [Selecionar Registro] [Editar] e [Excluir]
		
		if (qtdeTabelasDetalhes > 0) {
			f.format("\n	public void selecionarRegistroTabela () {"
					+ "\n		driver.findElement(By.xpath(\".//*/tr[2]/td[@aria-describedby='"+sectionCodeMestre+"_grid_actions']/a["+posicaoBotao+"]\")).click();"
					+ "\n	}\n");
			posicaoBotao++;
		}
		
		f.format("\n	public void clicarBotaoEditarMestre () {"
				+ "\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		util.aguardarElementoXPath (\".//*/tr[2]/td[@aria-describedby='"+sectionCodeMestre+"_grid_actions']/a["+posicaoBotao+"]\");"
				+ "\n		driver.findElement(By.xpath(\".//*/tr[2]/td[@aria-describedby='"+sectionCodeMestre+"_grid_actions']/a["+posicaoBotao+"]\")).click();"
				+ "\n	}\n");
		posicaoBotao++;
		
		f.format("\n	public void clicarBotaoExcluirMestre () {"
				+ "\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		util.aguardarElementoXPath (\".//*/tr[2]/td[@aria-describedby='"+sectionCodeMestre+"_grid_actions']/a["+posicaoBotao+"]\");"
				+ "\n		driver.findElement(By.xpath(\".//*/tr[2]/td[@aria-describedby='"+sectionCodeMestre+"_grid_actions']/a["+posicaoBotao+"]\")).click();"
				+ "\n	}\n");
		
		metodoRemoverRegistroTabela(f);
		cadastrarRegistrosDependentes(f);
	}
	
	public void metodosTabelaDetalhes (Formatter f,  int nroTabelaDetalhes) {
		
		String iSessaoTabela = dadosTabela.get(colunaSectionCode);
		
		f.format("\n	public void selecionarAba () {"
				+ "\n		driver.findElement(By.cssSelector(\"a[href='#"+dadosTabela.get(colunaSectionCode)+"']\")).click(); // Clicar na aba"
				+ "\n		assertTrue(\" ERRO: N�o foi carragado a aba '"+dadosTabela.get(colunaIndexLabel)+"'.\", pegarTituloTabelaDetalhe().equals(\""+dadosTabela.get(colunaIndexLabel)+"\"));"
				+ "\n	}\n");
		
		if (nomeAbasIntermediarias.size()>0) {
			String abaIntermediaria = nomeAbasIntermediarias.get(nroTabelaDetalhes-1).toLowerCase().replace(" ", "");
			f.format("\n	public void selecionarAbaIntermediaria () {"
					+ "\n		driver.findElement(By.cssSelector(\".details-group-btn.group-"+abaIntermediaria+"\")).click(); // Clicar na aba Intermedi�ria"
					+ "\n	}\n");
		}
		
		f.format("\n	public String pegarTituloTabelaDetalhe () {"
				+ "\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		util.aguardarElementoCSS(\"#gview_details_"+sectionCodeMestre+"_"+iSessaoTabela+"_grid .ui-jqgrid-title\");"
				+ "\n		return driver.findElement(By.cssSelector(\"#gview_details_"+sectionCodeMestre+"_"+iSessaoTabela+"_grid .ui-jqgrid-title\")).getText();"
				+ "\n	}\n");
		
		f.format("\n	public void clicarBotaoSalvarDetalhe () {"
//				Exitem dois ids btnSaveDetail_4 na tela. * Sendo verificado pelo Munir
//				+ "\n		driver.findElement(By.id(\"btnSaveDetail_"+nroTabelaDetalhes+"\")).click();"
				+ "\n		driver.findElement(By.xpath(\".//*[@id='gview_details_"+sectionCodeMestre+"_"+iSessaoTabela+"_grid']/div/ul/li/a[@id='btnSaveDetail_"+nroTabelaDetalhes+"']/i\")).click();"
				+ "\n	}\n");
		
		f.format("\n	public void clicarBotaoNovoDetalhe () {"
//				Exitem dois ids btnNewDetail_4 na tela. * Sendo verificado pelo Munir
//				+ "\n		driver.findElement(By.id(\"btnNewDetail_"+nroTabelaDetalhes+"\")).click();"
				+ "\n		driver.findElement(By.xpath(\".//*[@id='gview_details_"+sectionCodeMestre+"_"+iSessaoTabela+"_grid']/div/ul/li/a[@id='btnNewDetail_"+nroTabelaDetalhes+"']/i\")).click();"
				+ "\n	}\n");
		
		f.format("\n	public void clicarBotaoEditarDetalhe () {"
				+ "\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		util.aguardarElementoXPath (\".//*/tr[2]/td[@aria-describedby='details_"+sectionCodeMestre+"_"+iSessaoTabela+"_grid_actions']/a[1]\");"
				+ "\n		driver.findElement(By.xpath(\".//*/tr[2]/td[@aria-describedby='details_"+sectionCodeMestre+"_"+iSessaoTabela+"_grid_actions']/a[1]\")).click();"
				+ "\n	}\n");
		
		f.format("\n	public void clicarBotaoExcluirDetalhe () {"
				+ "\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		util.aguardarElementoXPath (\".//*/tr[2]/td[@aria-describedby='details_"+sectionCodeMestre+"_"+iSessaoTabela+"_grid_actions']/a[2]\");"
				+ "\n		driver.findElement(By.xpath(\".//*/tr[2]/td[@aria-describedby='details_"+sectionCodeMestre+"_"+iSessaoTabela+"_grid_actions']/a[2]\")).click();"
				+ "\n	}\n");
		
		metodoRemoverRegistroTabela(f);
		cadastrarRegistrosDependentes(f);
	}
	
	public void metodoValidarRegistroTabela (Formatter f) {
		
		f.format("\n	public void validarRegistroTabela (int nroRegistro, ");
		
		// Preencher os par�metros do m�todo
		for (int i=0; i<valorColunaId.size(); i++) {
			f.format("String "+valorColunaId.get(i).toLowerCase());
			if ( i<valorColunaId.size()-1 ) {
				f.format(", ");
			}
		}
		f.format(") {");
		
		// C�digo com as valida��es dos valores
		f.format("\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		String valorEncontrado;");
		
		String idGrid;
		if (tabelaMestre) {
			idGrid = dadosTabela.get(colunaSectionCode)+"_grid";
		} else {
			idGrid = "details_"+sectionCodeMestre+"_"+dadosTabela.get(colunaSectionCode)+"_grid";
		}
		
		f.format("\n		util.aguardarElementoXPath(\".//*[@id='"+idGrid+"']/tbody/tr[\"+(nroRegistro+1)+\"]/td["+(2)+"]\");\n");
		
		for (int i = 0; i < valorColunaId.size(); i++) {
			f.format("\n		// Validar coluna '"+valorColunaLabel.get(i).replace("%", "%%")+"'");
			if (valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")) {
				f.format("\n		if ("+valorColunaId.get(i).toLowerCase()+".equals(\"SIM\"))"
						+ "\n			assertTrue(\" ERRO: A coluna '"+valorColunaLabel.get(i).replace("%", "%%")+"' n�o est� marcada. \", util.validarCheckbox(\".//*[@id='"+idGrid+"']/tbody/tr[\"+(nroRegistro+1)+\"]/td["+(i+2)+"]/input\", "+valorColunaId.get(i).toLowerCase()+"));"
						+ "\n		else assertTrue(\" ERRO: A coluna '"+valorColunaLabel.get(i).replace("%", "%%")+"' n�o est� desmarcada. \", util.validarCheckbox(\".//*[@id='"+idGrid+"']/tbody/tr[\"+(nroRegistro+1)+\"]/td["+(i+2)+"]/input\", "+valorColunaId.get(i).toLowerCase()+"));");
			} else {
				f.format("\n		valorEncontrado = driver.findElement(By.xpath(\".//*[@id='"+idGrid+"']/tbody/tr[\"+(nroRegistro+1)+\"]/td["+(i+2)+"]\")).getText();");
				f.format("\n		assertTrue(\" ERRO: O valor da coluna '"+valorColunaLabel.get(i).replace("%", "%%")+"' n�o foi salvo corretamente. Esperado: '\"+"+valorColunaId.get(i).toLowerCase()+"+\"' / Encontrado: '\"+valorEncontrado+\"'\", valorEncontrado.equals("+valorColunaId.get(i).toLowerCase()+"));");
			}
			f.format("\n");
		}
		f.format("\n	}\n");
	}

	private void metodoRemoverRegistroTabela (Formatter f) {
		
		String idTabelaDetalhe = dadosTabela.get(colunaSectionCode).toLowerCase();
		String idTabelaMestre = sectionCodeMestre.toLowerCase();
		
		f.format("\n	public void removerRegistroTabela () {"
				+"\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		MensagensRetornadas msg = new MensagensRetornadas();");
		
		if (tabelaMestre) {
			f.format("\n		String xpathBtnRemover = \".//*[@id='btnDeleteMaster']\";");
			
		} else {
			f.format("\n		"+sectionCodeMestre+" "+idTabelaMestre+" = new "+sectionCodeMestre+"(driver);");
			
			// TODO Atualizar de CLASS para ID ap�s atualiza��o no sistema (Munir ciente)
			f.format("\n\n		String xpathBtnRemover = \".//*[@id='details_"+ sectionCodeMestre +"_"+ dadosTabela.get(colunaSectionCode) + "_grid']//*[contains(@class, 'appRemove')]\";");
			
			f.format("\n		"+dadosTabela.get(colunaSectionCode)+ " "+idTabelaDetalhe+" = new "+dadosTabela.get(colunaSectionCode)+"(driver);");
			f.format("\n\n		"+idTabelaMestre+".selecionarRegistroTabela();");
			if (nomeAbasIntermediarias.size()>0) {
				f.format("\n		"+idTabelaDetalhe+".selecionarAbaIntermediaria();");
			}
			f.format("\n		"+idTabelaDetalhe+".selecionarAba();");
		}
		
		f.format("\n\n		driver.findElement(By.xpath(xpathBtnRemover)).click();");
		f.format("\n		util.aguardarTempo(300);");
		
		f.format("\n\n		assertTrue(\"\\n ERRO: N�o foi exibido o alert de confirma��o de remo��o com o texto '\"+msg.msgConfirmacaoRemover+\"'.\", driver.switchTo().alert().getText().equals(msg.msgConfirmacaoRemover));");
		f.format("\n		driver.switchTo().alert().accept();");
		
		f.format("\n\n		assertTrue(\"\\n ERRO: N�o foi exibido a mensagem '\"+msg.msgSucessoItemRemovido+\"'.\", util.pegarMensagemSucesso().equals(msg.msgSucessoItemRemovido)); ");		
		f.format("\n	}\n");
		
	}
	
	
	private void obterTabelasDependentes () {
	String[] parametrosTabelasDependentes;
	tabelasDependentesBD.clear();
	existeTabelaDependente = false;
	
	for (int i = 0; (i < valorColunaJoin.size()) && (!existeTabelaDependente) ; i++) {
		if (!valorColunaJoin.get(i).equals("")) {
			existeTabelaDependente = true;
		}
	}
	
	if (existeTabelaDependente) {
		for (int i = 0; i < valorColunaJoin.size(); i++) {
			if (!valorColunaJoin.get(i).equals("")) {
				parametrosTabelasDependentes = valorColunaJoin.get(i).split("#");
				tabelasDependentesBD.add(parametrosTabelasDependentes[0]);
			}
		}
	}
	}

	private void importTabelasDependentes(Formatter f, String sistema, String tabelaMestre) {
		for (int dep = 0; dep < tabelasDependentesBD.size(); dep++) {
			for (int bd = 0; bd < todosDBTableName.size(); bd++) {
				if  ( (tabelasDependentesBD.get(dep).equals(todosDBTableName.get(bd))) && (!todosDatilOf.get(bd).equals(sectionCodeMestre)) )  {
					// Importa apenas se tiver em pacote diferente da tabela atual.
					f.format("\nimport "+sistema+"."+todosDatilOf.get(bd)+"."+todosSectionCodeTabelas.get(bd)+";");
					f.format("\nimport "+sistema+"."+todosDatilOf.get(bd)+"."+todosSectionCodeTabelas.get(bd)+"Test;");
				}
			}
		}
	}
	
	// Cadastra registros nas tabelas dependentes para realiza��o dos testes
	private static void cadastrarRegistrosDependentes(Formatter f) {
		String nomeTabela;
		
		f.format("\n	private void cadastrarDependencias() {");
		for (int j = 0; j < tabelasDependentesBD.size(); j++) {
			for (int i = 0; i < todosDBTableName.size(); i++) {
				if (tabelasDependentesBD.get(j).equals(todosDBTableName.get(i))) {
					nomeTabela = todosSectionCodeTabelas.get(i);
					
					f.format("\n\n		// Tabela dependente: "+nomeTabela);
					f.format("\n		"+nomeTabela+"Test "+nomeTabela.toLowerCase()+"test = new "+nomeTabela+"Test();");
					f.format("\n		"+nomeTabela+" "+nomeTabela.toLowerCase()+" = new "+nomeTabela+"(driver);");
					f.format("\n		"+nomeTabela.toLowerCase()+"test.acessarPagina();");
					f.format("\n		"+nomeTabela.toLowerCase()+".adicionarRegistroCadastro();");
					f.format("\n		"+nomeTabela.toLowerCase()+".adicionarRegistrosFiltros();");
				}
			}
		}
		f.format("\n	}\n");
	}
}
