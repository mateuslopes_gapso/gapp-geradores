package gerador;

import gapp.test.main.AbstractScript;

import java.io.File;
import java.util.Date;
import java.util.Formatter;

import javax.swing.JOptionPane;

public class LogResultado extends AbstractScript {

	private static String diretorioArquivoCriado;
	private static String diretorioSalvarArquivos;
	private static Formatter arquivo;
	
	public static void criarLogResultado (String localArquivo, Date dataInicioGerador, Date dataTerminoGerador) {
		
		diretorioSalvarArquivos = localArquivo + "/";
		criarDiretorio(diretorioSalvarArquivos);
		
		try {
			File diretorio = new File(diretorioSalvarArquivos);
			if (!diretorio.exists()) diretorio.mkdir();
			
			diretorioSalvarArquivos = diretorioSalvarArquivos.replaceAll("\\s+", "") + "/";
		    diretorioArquivoCriado = diretorioSalvarArquivos.replace("./", "");

    	    arquivo = new Formatter(diretorioArquivoCriado  + "/LogResultado.txt");
    	    salvarLogResultados(arquivo, dataInicioGerador, dataTerminoGerador);
    	    arquivo.close();
		    
    	    if (arquivosCTValidos) {
	    	    arquivo = new Formatter(diretorioArquivoCriado  + "/LogScriptsGerados.txt");
	    	    salvarLogScripts(arquivo, dataInicioGerador, dataTerminoGerador);
	    	    arquivo.close();
    	    }
    	    
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "N�o foi criado algum dos arquivos de LOG.", "", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private static void salvarLogScripts (Formatter f, Date dataInicioGerador, Date dataTerminoGerador) {
		
		long total = ((dataTerminoGerador.getTime() - dataInicioGerador.getTime()) / 1000);
		
		f.format("%nInicio da execu��o: 	"+formatoDataHora.format(dataInicioGerador));
		f.format("%nTempo de execu��o:	"+ total + " segundos");
		
		f.format("%n");
		
		if (qtdeScriptSucesso > 0) 
			f.format("%nTotal de arquivos de scripts gerados com Sucesso: "+ qtdeScriptSucesso);			
		if (qtdeScriptErro > 0)
			f.format("%nTotal de arquivos de scripts com Erros e/ou incompletos: "+ qtdeScriptErro);
		
		f.format("%n%n");
		f.format(logScriptsSucesso);
		f.format("%n%n");
		f.format(logScriptsErro);
	}

	private static void salvarLogResultados (Formatter f, Date dataInicioGerador, Date dataTerminoGerador) {
		
		long total = ((dataTerminoGerador.getTime() - dataInicioGerador.getTime()) / 1000);
		
		f.format("%nInicio da execu��o: 	"+formatoDataHora.format(dataInicioGerador));
		f.format("%nTempo de execu��o:	"+ total + " segundos");
		
		f.format("%n");
		
		if (arquivosCTValidos) {
			f.format("%nTotal de Casos de Testes: "+ qtdeCTTotal);
			if (qtdeCTSucesso > 0) 
				f.format("%nTotal de Casos de Testes com scripts gerados com Sucesso: "+ qtdeCTSucesso);			
			if (qtdeCTIncompleto > 0)
				f.format("%nTotal de Casos de Testes que n�o teve scripts gerados para todas as tabelas: "+ qtdeCTIncompleto);
			if (qtdeCTErro > 0)
				f.format("%nTotal de Casos de Testes que n�o foi poss�vel gerar os scripts: "+ qtdeCTErro);
			
			f.format("%n%n");
			f.format(logResultadoSucesso);
			f.format("%n%n");
			f.format(logResultadoImcompleto);
			f.format("%n%n");
			f.format(logResultadoErro);
		} else {
			f.format("%nUm dos arquivos de Casos de Testes informados n�o est� conforme o modelo.");
		}
	}

}
