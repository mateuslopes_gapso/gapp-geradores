package gapp.test.crud;

import gapp.test.main.AbstractScript;

import java.util.Formatter;

public class ScriptsRemover extends AbstractScript  {

	public void remover(Formatter f) {
		String sectionCode = dadosTabela.get(colunaSectionCode);
		
		f.format("\n	@Test"
				+"\n	public void removerRegistros () {");
		
		f.format("\n		assertTrue(\"\\n ATEN??O: N?o ? poss?vel executar o Teste porque a p?gina n?o foi carregada.\", paginaAcessada);"
				+ "\n		assertTrue(\"\\n ATEN??O: O Teste n?o foi executado porque o Teste de filtro n?o teve sucesso, podendo ser deletados registros errados.\", filtroSucesso);");
		
		f.format("\n\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);");
		
		f.format("\n		"+sectionCode+" "+sectionCode.toLowerCase()+" = new "+sectionCode+"(driver);\n");
		
		f.format("\n		"+sectionCode.toLowerCase()+".removerRegistroTabela(); // Remove o registro adicionado/editado");
		f.format("\n		"+sectionCode.toLowerCase()+".removerRegistroTabela(); // Remove o registro adicionado para o filtro");
				
		if (tabelaMestre) {
			f.format("\n		assertTrue(\"\\n ERRO: Os registros de '"+dadosTabela.get(colunaIndexLabel)+"' n?o for?o removidos.\", util.validarRemocaoDados(\".//*[@id='btnDeleteMaster']\")); ");
		} else {
			// TODO Atualizar de CLASS para ID ap�s atualiza��o no sistema (Munir ciente)
			f.format("\n		assertTrue(\"\\n ERRO: Os registros de '"+dadosTabela.get(colunaIndexLabel)+"' n�o for�o removidos.\", util.validarRemocaoDados(\".//*[@id='details_"+ sectionCodeMestre +"_"+ sectionCode + "_grid']//*[contains(@class, 'appRemove')]\"));");
		}
		
		if(!tabelaMestre){
			f.format("\n\n		"+sectionCodeMestre+" "+sectionCodeMestre.toLowerCase()+" = new "+sectionCodeMestre+"(driver);");
			f.format("\n		"+sectionCodeMestre.toLowerCase()+".removerRegistroTabela();//Remove registro mestre");
			f.format("\n		assertTrue(\"\\n ERRO: Os registros de '"+sectionCodeMestre+"' n?o for?o removidos.\", util.validarRemocaoDados(\".//*[@id='btnDeleteMaster']\")); ");
		}
		f.format("\n\n		System.out.println(\" "+sectionCode+": Teste de REMO��O executado\");");
		f.format("\n	}\n");
	}
	
}
