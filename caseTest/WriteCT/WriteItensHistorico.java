package WriteCT;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import Formatacoes.FormatacaoTable;

public class WriteItensHistorico {
	FormatacaoTable formatcaoTable = new FormatacaoTable();
	
	public void writeNomeCT(Workbook workbook, String nomeSheet, String nomeSistema){
		Sheet sheetHistorico = workbook.getSheet("Historico");			
		Row row;
		
		// Criar Linha e Coluna com a formatação
		row = sheetHistorico.createRow(2);
		row.setHeight((short)420); //28 pixels, 28,00
		row.createCell(1).setCellValue(nomeSistema+" - <NumUC> - " + nomeSheet);
		row.getCell(1).setCellStyle(formatcaoTable.styleCelulaNomeCT(workbook));
		sheetHistorico.addMergedRegion(new CellRangeAddress(2, 2, 1, 4));// mescla celulas
		
	}
	
	public void writeItemMenu(Workbook workbook, int linha, String valorCelula){		
		Sheet sheetHistorico = workbook.getSheet("Historico");			
		Row row;
						
		row = sheetHistorico.createRow(linha);
		row.createCell(1).setCellValue("Menu de Acesso");
		row.getCell(1).setCellStyle(formatcaoTable.styleLeftNegrito(workbook, linha));
		row.createCell(2).setCellValue(valorCelula);
		row.getCell(2).setCellStyle(formatcaoTable.styleCentralParametros(workbook, linha));
		row.createCell(3).setCellStyle(formatcaoTable.styleBordaSuperior(workbook));
		row.createCell(4).setCellValue(" -");
		row.getCell(4).setCellStyle(formatcaoTable.styleRight(workbook, linha));
		sheetHistorico.addMergedRegion(new CellRangeAddress(linha, linha, 2, 3));// mescla celulas
		sheetHistorico.autoSizeColumn(1); //Ajusta largura da coluna de acordo com o texto
		sheetHistorico.autoSizeColumn(2); //Ajusta largura da coluna de acordo com o texto
		sheetHistorico.autoSizeColumn(3); //Ajusta largura da coluna de acordo com o texto
		sheetHistorico.autoSizeColumn(4); //Ajusta largura da coluna de acordo com o texto
	}
	
	public void writeItemTabMestre(Workbook workbook, int linha, String valorSectionCode, String valorIndexLabel){
		Sheet sheetHistorico = workbook.getSheet("Historico");			
		Row row;	
		
		row = sheetHistorico.createRow(linha);
		row.createCell(1).setCellValue("Tabela Mestre");
		row.getCell(1).setCellStyle(formatcaoTable.styleLeftNegrito(workbook, linha));
		row.createCell(2).setCellValue(valorIndexLabel);
		row.getCell(2).setCellStyle(formatcaoTable.styleCentral(workbook, linha));
		row.createCell(3).setCellStyle(formatcaoTable.styleBordaSuperior(workbook));
		row.createCell(4).setCellValue(valorSectionCode);
		row.getCell(4).setCellStyle(formatcaoTable.styleRight(workbook, linha));
		sheetHistorico.addMergedRegion(new CellRangeAddress(linha, linha, 2, 3));// mescla celulas
		sheetHistorico.autoSizeColumn(1); //Ajusta largura da coluna de acordo com o texto
		sheetHistorico.autoSizeColumn(2); //Ajusta largura da coluna de acordo com o texto
		sheetHistorico.autoSizeColumn(3); //Ajusta largura da coluna de acordo com o texto
		sheetHistorico.autoSizeColumn(4); //Ajusta largura da coluna de acordo com o texto
	}
	
	public void writeItemTabDetalhe(Workbook workbook, int linha, String valorSectionCode, String valorIndexLabel, int numTabDetalhe){
		Sheet sheetHistorico = workbook.getSheet("Historico");			
		Row row;	
		
		row = sheetHistorico.createRow(linha);
		row.createCell(1).setCellValue("Tabela Detalhe "+numTabDetalhe);
		row.getCell(1).setCellStyle(formatcaoTable.styleLeftNegrito(workbook, linha));
		row.createCell(2).setCellValue(valorIndexLabel);
		row.getCell(2).setCellStyle(formatcaoTable.styleCentral(workbook, linha));
		row.createCell(3).setCellStyle(formatcaoTable.styleBordaSuperior(workbook));
		row.createCell(4).setCellValue(valorSectionCode);
		row.getCell(4).setCellStyle(formatcaoTable.styleRight(workbook, linha));
		sheetHistorico.addMergedRegion(new CellRangeAddress(linha, linha, 2, 3));// mescla celulas
		sheetHistorico.autoSizeColumn(1); //Ajusta largura da coluna de acordo com o texto
		sheetHistorico.autoSizeColumn(2); //Ajusta largura da coluna de acordo com o texto
		sheetHistorico.autoSizeColumn(3); //Ajusta largura da coluna de acordo com o texto
		sheetHistorico.autoSizeColumn(4); //Ajusta largura da coluna de acordo com o texto
	}
	
	public void writeLinhaInferior(Workbook workbook, int linha){
		Sheet sheetHistorico = workbook.getSheet("Historico");			
		Row row;	
		
		row = sheetHistorico.createRow(linha);
		row.createCell(1).setCellStyle(formatcaoTable.styleBordaInferior(workbook));
		row.createCell(2).setCellStyle(formatcaoTable.styleBordaInferior(workbook));
		row.createCell(3).setCellStyle(formatcaoTable.styleBordaInferior(workbook));
		row.createCell(4).setCellStyle(formatcaoTable.styleBordaInferior(workbook));
		
	}
}
