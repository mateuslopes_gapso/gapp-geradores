package gapp.test.crud;

import gapp.GeradorValores;
import gapp.test.AssertMensagensErro;
import gapp.test.main.AbstractScript;

import java.util.Formatter;

public class ScriptsEditar extends AbstractScript  {

	////////////////////////////////////////////////////////////// M�todos TEST ///////////////////////////////////////////////////////////////////
	
	public void editarTest (Formatter f) {
		if (permiteEdicao()) {
			
			boolean campoObrigatorio = false; // Indica se existe algum campo na tabela que o preenchimento � obrigat�rio. Se existir, ir� validar o cadastro Nulo
			boolean permiteInvalido = false; // Indica se existe algum campo que permite dados inv�lidos. Se existir, ir� validar o cadastro Inv�lido. * Exce��es: campos tipo Select e Checkbox

			for (int i = 0; (i < valorColunaEditavel.size() && !campoObrigatorio) ; i++) {
				if (valorColunaEditavel.get(i).toUpperCase().equals("YES")) {
					if ( !(valorColunaTipo.get(i).toUpperCase().equals("SELECT")) && !(valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")) ) {
						permiteInvalido = true;
						if (valorColunaObrigatoria.get(i).toUpperCase().equals("YES")) {
							campoObrigatorio = true;
						}
					}
				}
			}
			
			///////////////////// Edi��o pelo MODAL ////////////////////////
			if (campoObrigatorio) testEditarComErroNuloModal(f);
			if (permiteInvalido) testEditarComErroInvalidoModal(f);
			editarSucessoModalTest(f);
			
			///////////////////// Edi��o pela TABELA ///////////////////////
			if (campoObrigatorio) testEditarComErroNuloTabela(f);
			if (permiteInvalido) testEditarComErroInvalidoTabela(f);
			editarSucessoTabelaTest(f);
			
		}
	}
	
	private void editarSucessoModalTest (Formatter f) {
		
		GeradorValores valores = new GeradorValores();
		
		f.format("\n	@Test"
				+"\n	public void editarModalComSucesso () {");
		
		f.format("\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque a p�gina n�o foi carregada.\", paginaAcessada);"
				+ "\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque o cadastro n�o teve sucesso.\", cadastroSucesso);"
				+ "\n		assertTrue(\"\\n ATEN��O: O teste n�o foi executado porque o edi��o anterior n�o teve sucesso.\", edicaoSucesso);"
				+ "\n		edicaoSucesso = false;");
		
		f.format("\n\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		FuncionalidadesCrud crud = new FuncionalidadesCrud(driver);"
				+ "\n		MensagensRetornadas msg = new MensagensRetornadas();");
		
		f.format("\n		"+sectionCodeMestre+" "+sectionCodeMestre.toLowerCase()+" = new "+sectionCodeMestre+"(driver);");
		if (!tabelaMestre) 
			f.format("\n		"+dadosTabela.get(colunaSectionCode)+ " "+dadosTabela.get(colunaSectionCode).toLowerCase()+" = new "+dadosTabela.get(colunaSectionCode)+"(driver);");
		
		abrirModalEditar(f);
				
		valores.criarVariavelComboboxModal(f);

		preencherMetodoEditarSucesso(f, "Modal");
				
		f.format("\n		crud.clicarBotaoSalvarModalEdicao();");
		f.format("\n		assertTrue(\"\\n ERRO: N�o foi exibido a mensagem '\"+msg.msgSucessoItemAtualizado+\"'. \\n Mensagem retornada: \"+util.pegarMensagemSucesso(), util.pegarMensagemSucesso().equals(msg.msgSucessoItemAtualizado));");
		
		f.format("\n		"+dadosTabela.get(colunaSectionCode).toLowerCase()+".validarRegistroTabela(1, ");
		valores.preencherValoresSalvos(f, valoresEditadosModal);
		f.format(");");
		
		f.format("\n\n		util.fecharMensagem();");
		f.format("\n		System.out.println(\" "+dadosTabela.get(colunaSectionCode)+": Teste de EDI��O SUCESSO (Modal) executado\");"
				+ "\n		edicaoSucesso = true;");
		
		f.format("\n	}\n");
		
	}

	private void editarSucessoTabelaTest (Formatter f) {
		GeradorValores valores = new GeradorValores();
		String idTabelaDetalhe = dadosTabela.get(colunaSectionCode).toLowerCase();
		
		f.format("\n	@Test"
				+"\n	public void editarTabelaComSucesso () {");
		
		f.format("\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque a p�gina n�o foi carregada.\", paginaAcessada);"
				+ "\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque o cadastro n�o teve sucesso.\", cadastroSucesso);"
				+ "\n		assertTrue(\"\\n ATEN��O: O teste n�o foi executado porque o edi��o anterior n�o teve sucesso.\", edicaoSucesso);"
				+ "\n		edicaoSucesso = false;");
		
		f.format("\n		MensagensRetornadas msg = new MensagensRetornadas();");
		f.format("\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"); // TODO Remover ap�s corre��o de LOOM-199
		
		selecionarRegistroTabela(f);
		
		preencherMetodoEditarSucesso(f, "Tabela");
		
		valores.criarVariavelComboboxTabela(f, "EDITAR");
		
		clicarBotaoSalvarTabela(f, idTabelaDetalhe);
		
		f.format("\n		assertTrue(\"\\n ERRO: N�o foi exibido a mensagem '\"+msg.msgSucessoItemAtualizado+\"'. \\n Mensagem retornada: \"+util.pegarMensagemSucesso(), util.pegarMensagemSucesso().equals(msg.msgSucessoItemAtualizado));");
		f.format("\n		"+idTabelaDetalhe+".validarRegistroTabela(1, ");
		valores.preencherValoresSalvos(f, valoresEditadosTabela);
		f.format(");");
		
		f.format("\n\n		util.fecharMensagem();");
		f.format("\n		System.out.println(\" "+dadosTabela.get(colunaSectionCode)+": Teste de EDI��O SUCESSO (Tabela) executado\");"
				+ "\n		edicaoSucesso = true;");
		f.format("\n	}\n");
	}
	
	private void testEditarComErroNuloModal(Formatter f) {
		AssertMensagensErro msgErro = new AssertMensagensErro();
		
		f.format("\n	@Test"
				+"\n	public void editarModalComErro1Nulo() {");
		
		f.format("\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque a p�gina n�o foi carregada.\", paginaAcessada);"
				+ "\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque o cadastro n�o teve sucesso.\", cadastroSucesso);"
				+ "\n		edicaoSucesso = false;");
		
		f.format("\n\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+"\n		FuncionalidadesCrud crud = new FuncionalidadesCrud(driver);"
				+ "\n		String mensagemErro;");
		
		f.format("\n		"+sectionCodeMestre+" "+sectionCodeMestre.toLowerCase()+" = new "+sectionCodeMestre+"(driver);");
		if (!tabelaMestre) 
			f.format("\n		"+dadosTabela.get(colunaSectionCode)+ " "+dadosTabela.get(colunaSectionCode).toLowerCase()+" = new "+dadosTabela.get(colunaSectionCode)+"(driver);");
		
		abrirModalEditar(f);
		
		preencherMetodoEditarNulo(f, "Modal");
		
		f.format("\n		crud.clicarBotaoSalvarModalEdicao();\n");
		
		msgErro.assertMensagemErroNulo(f, "Edi��o");
		
		f.format("\n\n		util.fecharMensagem();");
		
		f.format("\n\n		System.out.println(\" "+dadosTabela.get(colunaSectionCode)+": Teste de EDI��O DADOS NULOS (Modal) executado\");"
				+ "\n		edicaoSucesso = true;"
				+"\n	}\n");
	}

	private void testEditarComErroNuloTabela(Formatter f) {
		AssertMensagensErro msgErro = new AssertMensagensErro();
		String idTabelaDetalhe = dadosTabela.get(colunaSectionCode).toLowerCase();
		
		f.format("\n	@Test"
				+"\n	public void editarTabelaComErro1Nulo() {");
		
		f.format("\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque a p�gina n�o foi carregada.\", paginaAcessada);"
				+ "\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque o cadastro n�o teve sucesso.\", cadastroSucesso);"
				+ "\n		assertTrue(\"\\n ATEN��O: O teste n�o foi executado porque o edi��o anterior n�o teve sucesso.\", edicaoSucesso);"
				+ "\n		edicaoSucesso = false;");
		
		f.format("\n\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		String mensagemErro;");
		
		selecionarRegistroTabela(f);
		
		preencherMetodoEditarNulo(f, "Tabela");
		
		clicarBotaoSalvarTabela(f, idTabelaDetalhe);
		
		msgErro.assertMensagemErroNulo(f, "Edi��o");

		f.format("\n\n		util.fecharMensagem(); // Fechar mensagens de sucesso ou alerta");
		f.format("\n		System.out.println(\" "+dadosTabela.get(colunaSectionCode)+": Teste de EDI��O DADOS NULOS (Tabela) executado\");"
				+ "\n		edicaoSucesso = true;"
				+"\n	}\n");
	}

	private void testEditarComErroInvalidoModal (Formatter f) {
		AssertMensagensErro msgErro = new AssertMensagensErro();
		
		f.format("\n	@Test"
				+"\n	public void editarModalComErro2Invalido () {");
		
		f.format("\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque a p�gina n�o foi carregada.\", paginaAcessada);"
				+ "\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque o cadastro n�o teve sucesso.\", cadastroSucesso);"
				+ "\n		assertTrue(\"\\n ATEN��O: O teste n�o foi executado porque o edi��o anterior n�o teve sucesso.\", edicaoSucesso);"
				+ "\n		edicaoSucesso = false;");
		
		f.format("\n\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		FuncionalidadesCrud crud = new FuncionalidadesCrud(driver);"
				+ "\n		String mensagemErro;");
		
		f.format("\n		"+sectionCodeMestre+" "+sectionCodeMestre.toLowerCase()+" = new "+sectionCodeMestre+"(driver);");
		if (!tabelaMestre) 
			f.format("\n		"+dadosTabela.get(colunaSectionCode)+ " "+dadosTabela.get(colunaSectionCode).toLowerCase()+" = new "+dadosTabela.get(colunaSectionCode)+"(driver);");
		
		preencherMetodoEditarInvalido(f, "Modal");
		
		f.format("\n		crud.clicarBotaoSalvarModalEdicao();\n");
		
		msgErro.assertMensagemErroInvalido(f, "Edi��o");

		f.format("\n\n		util.fecharMensagem();");
		f.format("\n		util.clicarBotaoCancelarModalEditar();");
		
		f.format("\n\n		System.out.println(\" "+dadosTabela.get(colunaSectionCode)+": Teste de EDI��O DADOS INV�LIDO (Modal) executado\");"
				+ "\n		edicaoSucesso = true;"
				+"\n	}\n");
	}
	
	private void testEditarComErroInvalidoTabela (Formatter f) {
//		AssertMensagensErro msgErro = new AssertMensagensErro(); // TODO Revomver ap�s ser confirmado a forma da valida��o (por popup ou mensagem)
//		String idTabelaDetalhe = dadosTabela.get(colunaSectionCode).toLowerCase();
		
		f.format("\n	@Test"
				+"\n	public void editarTabelaComErro2Invalido() {");
		
		f.format("\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque a p�gina n�o foi carregada.\", paginaAcessada);"
				+ "\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o teste porque o cadastro n�o teve sucesso.\", cadastroSucesso);"
				+ "\n		assertTrue(\"\\n ATEN��O: O teste n�o foi executado porque o edi��o anterior n�o teve sucesso.\", edicaoSucesso);"
				+ "\n		edicaoSucesso = false;");
		
		selecionarRegistroTabela(f);
		
		preencherMetodoEditarInvalido(f, "Tabela");
		
		// A valida��o � realizada durante a edi��o. // TODO Revomver ap�s ser confirmado a forma da valida��o (por popup ou mensagem)
		//clicarBotaoSalvarTabela(f, idTabelaDetalhe);
		//msgErro.assertMensagemErroInvalido(f, "Edi��o");
		//f.format("\n\n		util.fecharMensagem();");
		
		f.format("\n    	System.out.println(\" "+dadosTabela.get(colunaSectionCode)+": Teste de EDI��O DADOS INV�LIDO (Tabela) executado\");"
				+ "\n		edicaoSucesso = true;"
				+"\n	}\n");
	}
	
	// Fim /////////////////////////////////////////////////////// M�todos TEST ///////////////////////////////////////////////////////////////////
	
	
	// Inicio ////////////////////////////////////////////////// M�todos SOURCER /////////////////////////////////////////////////////////////////
	
	public void editarRegistro (Formatter f) {
		if (permiteEdicao()) {
			editarRegistroModal(f);
			editarRegistroTabela(f);
			metodoValidarEdicaoTabela(f);
		}
	}

	private void editarRegistroModal (Formatter f) {
		f.format("\n	public void editarRegistroModal (boolean dadosComErro, ");
		declararParametrosEditar(f);
		f.format(") {");
		
		editarModal(f);

		f.format("\n	}\n");
	}
	
	private void editarRegistroTabela (Formatter f) {
		f.format("\n	public void editarRegistroTabela (boolean dadosComErro, ");
		declararParametrosEditar(f);
		f.format(") {");
		
		editarTabela(f);
		
		f.format("\n	}\n");
	}
	
	private void declararParametrosEditar (Formatter f) {
		String vColuna; // Vari�vel com o nome da coluna
		boolean paramentroPreenchido =  false;
		
		try {
			for (int i = 0; i < valorColunaId.size(); i++) {
				vColuna = valorColunaId.get(i).toLowerCase();
				if (valorColunaEditavel.get(i).toUpperCase().equals("YES") ) {
					if (paramentroPreenchido) {
						f.format(", ");
					}
					if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")) {
						f.format("int "+vColuna);
					} else {
						f.format("String "+vColuna);
					}
					paramentroPreenchido =  true;
				}
			}
		} catch (Exception e) {
			// N�o existe mais colunas para serem verificadas
		}

	}
	
	private void editarModal (Formatter f) {
		f.format("\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);");
		f.format("\n		String idElemento = null;\n");
		
		for (int l=0; l<valorColunaId.size(); l++) {
			
			if (valorColunaEditavel.get(l).toUpperCase().equals("YES")) {
				
				f.format("\n		idElemento = \""+valorColunaId.get(l)+"\";");
				
				if (	valorColunaTipo.get(l).toUpperCase().equals("STRING") ||
						valorColunaTipo.get(l).toUpperCase().equals("NUMERIC") ||
						valorColunaTipo.get(l).toUpperCase().equals("INTEGER")) {
					f.format("\n		util.limparCampoID (idElemento);"
							+ "\n		driver.findElement(By.id(idElemento)).sendKeys("+valorColunaId.get(l).toLowerCase()+");");				
				} 
				else if (valorColunaTipo.get(l).toUpperCase().equals("DATE")) {
					f.format("\n		util.limparCampoID (idElemento);"
							+ "\n		driver.findElement(By.id(idElemento)).sendKeys("+valorColunaId.get(l).toLowerCase()+");");
					f.format("\n		driver.findElement(By.id(\""+valorColunaId.get(l)+"\")).sendKeys(Keys.TAB);");
					f.format("\n		if (dadosComErro) {"
							+ "\n			assertTrue( \" ERRO: N�o foi exibido o alert de Erro de formato para o campo '"+valorColunaLabel.get(l)+"'.\", driver.switchTo().alert().getText().contains(\"Erro de formato\"));"
							+ "\n			driver.switchTo().alert().accept();"
							+ "\n		}");
				}
				else if (valorColunaTipo.get(l).toUpperCase().equals("CHECKBOX")) {
					f.format("\n		assertTrue(\" ERRO: O checkbox '"+valorColunaLabel.get(l)+"' n�o foi Marcado/Desmarcado.\", util.checkboxMarcarDesmarcarId(\""+valorColunaId.get(l)+"\", "+valorColunaId.get(l).toLowerCase()+"));");
				}
				else if (valorColunaTipo.get(l).toUpperCase().equals("SELECT")) {
					f.format("\n		new Select(driver.findElement(By.id(idElemento))).selectByIndex("+valorColunaId.get(l).toLowerCase()+");");
				}
				f.format("\n");
			}
		}
	}
	
	private void editarTabela (Formatter f) {
		String xpathCelula;
		f.format("\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);");
		
		String idGrid;
		if (tabelaMestre) {
			idGrid = dadosTabela.get(colunaSectionCode)+"_grid";
		} else {
			idGrid = "details_"+sectionCodeMestre+"_"+dadosTabela.get(colunaSectionCode)+"_grid";
		}
		
		f.format("\n		String xpathElemento =  \".//*[@id='"+idGrid+"']/tbody/tr[2]/td["+(2)+"]\";");
		f.format("\n		util.aguardarElementoXPath(xpathElemento);\n");
		
		for (int l = 0; l<valorColunaId.size(); l++) {
			String label; 
			if (valorColunaLabelGroupHeader.get(l).isEmpty()) {
				label = valorColunaLabel.get(l).replace("%", "%%"); // TODO remover o IF e deixar apenas este c�digo ap�s corre��o de LOOM-203
			} else {
				label = valorColunaLabelGroupHeader.get(l);
			}
			
			if (valorColunaEditavel.get(l).toUpperCase().equals("YES")) {
				f.format("\n		// Editar coluna '"+label+"'");
				
				xpathCelula = "\".//*[@id='"+idGrid+"']/tbody/tr[2]/td["+(l+2)+"]\"";
				
				if (	valorColunaTipo.get(l).toUpperCase().equals("STRING") ||
						valorColunaTipo.get(l).toUpperCase().equals("NUMERIC") ||
						valorColunaTipo.get(l).toUpperCase().equals("INTEGER") ||
						valorColunaTipo.get(l).toUpperCase().equals("DATE")) {
					f.format("\n		driver.findElement(By.xpath("+xpathCelula+")).click();");
					f.format("\n		xpathElemento = \".//*[@id='1_"+valorColunaId.get(l)+"']\";");
					f.format("\n		util.limparCampoXPath(xpathElemento);"
							+"\n		driver.findElement(By.xpath(xpathElemento)).sendKeys("+valorColunaId.get(l).toLowerCase()+");");
					
					// Valida mensagem de caracteres maior ou menor, String e Numeric que o permitido. Evelyn
					if(valorColunaTipo.get(l).toUpperCase().equals("STRING")||
							valorColunaTipo.get(l).toUpperCase().equals("NUMERIC")&&
							!valorColunaDeRegra.get(l).equals("")){
						String regra [] = null;
						regra = valorColunaDeRegra.get(l).split("#");
						if(regra[0].equals("string")){
							f.format("\n		driver.findElement(By.xpath(\".//*[@id='"+idGrid+"']/tbody/tr[2]/td["+(l+1)+"]\")).click();");
							f.format("\n		validarEdicaoTabela(dadosComErro, "+valorColunaId.get(l).toLowerCase()+", \"O tamanho do campo "+label+" deve ser menor que "+ regra[2] +"\");");
						}
						if(regra[0].equals("spin")){
							if (regra.length == 2) {
								f.format("\n		driver.findElement(By.xpath(\".//*[@id='"+idGrid+"']/tbody/tr[2]/td["+(l+1)+"]\")).click();");
								f.format("\n		validarEdicaoTabela(dadosComErro, "+valorColunaId.get(l).toLowerCase()+", \"O valor do campo "+label+" n�o pode ser menor que "+ regra[1] +"\");");
							}else if (regra.length == 3){
								f.format("\n		driver.findElement(By.xpath(\".//*[@id='"+idGrid+"']/tbody/tr[2]/td["+(l+1)+"]\")).click();");
								f.format("\n		validarEdicaoTabela(dadosComErro, "+valorColunaId.get(l).toLowerCase()+", \"O valor do campo "+label+" n�o pode ser maior que "+ regra[2] +"\");");
							}
						}
					}
				}
				else if (valorColunaTipo.get(l).toUpperCase().equals("CHECKBOX")) {
					//f.format("\n		driver.findElement(By.xpath(xpathElemento)).sendKeys(Keys.TAB);");
					f.format("\n		driver.findElement(By.xpath("+xpathCelula+")).click();");
					f.format("\n		xpathElemento = \".//*[@id='1_"+valorColunaId.get(l)+"']\";");
					f.format("\n		assertTrue(\" ERRO: O checkbox '"+label+"' n�o foi Marcado/Desmarcado.\", util.checkboxMarcarDesmarcarXpath(xpathElemento, "+valorColunaId.get(l).toLowerCase()+"));");
				}
				else if (valorColunaTipo.get(l).toUpperCase().equals("SELECT")) {
					f.format("\n		driver.findElement(By.xpath("+xpathCelula+")).click();");
					f.format("\n		new Select(driver.findElement(By.xpath(\".//*[@id='"+idGrid+"']/tbody/tr[2]/td["+(l+2)+"]/select\"))).selectByIndex("+valorColunaId.get(l).toLowerCase()+");");
					f.format("\n		driver.findElement(By.xpath(\".//*[@id='"+idGrid+"']/tbody/tr[2]/td["+(l+2)+"]/select\")).sendKeys(Keys.ENTER);");
				}
				f.format("\n");
			}
		}
	}
	
	// Fim ///////////////////////////////////////////////////// M�todos SOURCER /////////////////////////////////////////////////////////////////

	private boolean permiteEdicao() {
		boolean permiteEdicao = false; // Indica se existe algum campo que permite ser editado
		
		for (int i = 0; (i < valorColunaEditavel.size() && !permiteEdicao ); i++) {
			if (valorColunaEditavel.get(i).toUpperCase().equals("YES"))
				permiteEdicao = true;
		}
		return permiteEdicao;
	}
	
	private void abrirModalEditar(Formatter f) {
		
		String idTabelaDetalhe = dadosTabela.get(colunaSectionCode).toLowerCase();
		String idTabelaMestre = sectionCodeMestre.toLowerCase();
		
		if (tabelaMestre) {
			f.format("\n\n		"+idTabelaMestre+".clicarBotaoEditarMestre();");
		} else {
			f.format("\n\n		"+idTabelaMestre+".selecionarRegistroTabela();");
			if (nomeAbasIntermediarias.size()>0) {
				f.format("\n		"+idTabelaDetalhe+".selecionarAbaIntermediaria();");
			}
			f.format("\n		"+idTabelaDetalhe+".selecionarAba();");
			f.format("\n		"+idTabelaDetalhe+".clicarBotaoEditarDetalhe();");
		}

		f.format("\n\n		util.pegarTituloModal();"
				+"\n		assertTrue(\" ERRO: N�o foi exibido o modal '"+dadosTabela.get(colunaEditLabel)+"'. \", util.titulo.contains(\""+dadosTabela.get(colunaEditLabel)+"\"));\n");
	}
	
	private void preencherMetodoEditarSucesso (Formatter f, String local) {
		GeradorValores valores = new GeradorValores();		
		
		if (local.toUpperCase().equals("MODAL")) {
			f.format("\n		"+dadosTabela.get(colunaSectionCode).toLowerCase()+".editarRegistroModal(false, ");
			valores.preencherValoresParaEditar(f, valoresEditadosModal);
		} else {
			f.format("\n		"+dadosTabela.get(colunaSectionCode).toLowerCase()+".editarRegistroTabela(false, ");
			valores.preencherValoresParaEditar(f, valoresEditadosTabela);
		}
		f.format(");\n");
	}
	
	private void preencherMetodoEditarNulo (Formatter f, String local) {
		GeradorValores valores = new GeradorValores();		
		
		if (local.toUpperCase().equals("MODAL")) {
			f.format("\n		"+dadosTabela.get(colunaSectionCode).toLowerCase()+".editarRegistroModal(false, ");
			valores.preencherValoresNulosEditar(f);
		} else {
			f.format("\n		"+dadosTabela.get(colunaSectionCode).toLowerCase()+".editarRegistroTabela(false, ");
			valores.preencherValoresNulosEditar(f);
		}
		f.format(");");
	}
	
	private void preencherMetodoEditarInvalido (Formatter f, String local) {
		GeradorValores valores = new GeradorValores();		
		
		if (local.toUpperCase().equals("MODAL")) {
			f.format("\n		"+dadosTabela.get(colunaSectionCode).toLowerCase()+".editarRegistroModal(true, ");
			valores.preencherValoresParaEditar(f, valoresInvalidos);
		} else {
			f.format("\n		"+dadosTabela.get(colunaSectionCode).toLowerCase()+".editarRegistroTabela(true, ");
			valores.preencherValoresParaEditar(f, valoresInvalidos);
		}
		f.format(");\n");
	}
	
	private void clicarBotaoSalvarTabela(Formatter f, String idTabelaDetalhe) {
		if (tabelaMestre) {
			f.format("\n		FuncionalidadesCrud crud = new FuncionalidadesCrud(driver);"
					+ "\n		crud.clicarBotaoSalvarMaster(\""+sectionCodeMestre+"\");\n");
		} else {
			f.format("\n		"+idTabelaDetalhe+".clicarBotaoSalvarDetalhe();\n");
		}
	}
	
	private void selecionarRegistroTabela(Formatter f) {
		String idTabelaDetalhe = dadosTabela.get(colunaSectionCode).toLowerCase();
		String idTabelaMestre = sectionCodeMestre.toLowerCase();
		
		f.format("\n		"+sectionCodeMestre+" "+idTabelaMestre+" = new "+sectionCodeMestre+"(driver);\n");
		
		if (tabelaMestre) {
			// N�o necessita de nenhum comando adicional
		} else {
			f.format("\n		"+dadosTabela.get(colunaSectionCode)+ " "+idTabelaDetalhe+" = new "+dadosTabela.get(colunaSectionCode)+"(driver);");
			f.format("\n\n		"+idTabelaMestre+".selecionarRegistroTabela();");
			if (nomeAbasIntermediarias.size()>0) {
				f.format("\n		"+idTabelaDetalhe+".selecionarAbaIntermediaria();");
			}
			f.format("\n		"+idTabelaDetalhe+".selecionarAba();");
		}
	}
	
	private void metodoValidarEdicaoTabela (Formatter f) {
		f.format("\n	private void validarEdicaoTabela (boolean dadosComErro, String valorColuna, String msgDeErroParaValidar) {");
				f.format("\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);");
				f.format("\n		String msgRetornada;");
				f.format("\n		");
				f.format("\n		if(!valorColuna.equals(\"\")){");
				f.format("\n			try{");
				f.format("\n				if (dadosComErro) {");
				f.format("\n					if(driver.findElement(By.id(\"info_head\")).isDisplayed()){");
				f.format("\n						msgRetornada = util.pegarNovaMensagemErro();");
				f.format("\n						assertTrue(\"\\n ERRO: N�o foi exibido a mensagem '\"+msgDeErroParaValidar+\"'. \\n Mensagem retornada: \"+msgRetornada,");
				f.format("\n								msgRetornada.contains(msgDeErroParaValidar));");
				f.format("\n						driver.findElement(By.id(\"closedialog\")).click();");
				f.format("\n					}");
				f.format("\n				}");
				f.format("\n			}catch (NoSuchElementException e){");
				f.format("\n				assertTrue(\"\\n ERRO: N�o foi exibido o popup com a mensagem de Erro ou n�o foi encontrado o bot�o para fechar a mensagem.\", false);");
				f.format("\n			}");
				f.format("\n		}");
				f.format("\n	}\n");
	}
	
}
