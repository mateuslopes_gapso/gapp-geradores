package gapp.test.crud;

import gapp.GeradorValores;
import gapp.test.AssertMensagensErro;
import gapp.test.main.AbstractScript;

import java.util.Formatter;

public class ScriptsCadastrar extends AbstractScript {

	////////////////////////////////////////////////////////////// M�todos TEST ///////////////////////////////////////////////////////////////////
	
	public void cadastrarTest (Formatter f) {
		cadastrarErroTest (f);
		cadastrarSucessoTest (f);
	}
	
	private void cadastrarSucessoTest (Formatter f) {
		String secionCode = dadosTabela.get(colunaSectionCode);
		
		f.format("\n	@Test"
				+"\n	public void cadastrarComSucesso() {");
		
		f.format("\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o Teste de INCLUS�O SUCESSO porque a p�gina n�o foi carregada.\", paginaAcessada);"
				+ "\n		assertTrue(\"\\n ATEN��O: O teste n�o foi executado porque o teste de cadastro com dados inv�lidos n�o teve sucesso.\", cadastroSucesso);"
				+ "\n		cadastroSucesso = false;");
				
		f.format("\n\n		"+secionCode+" "+secionCode.toLowerCase()+" = new "+secionCode+"(driver);");
		f.format("\n		"+secionCode.toLowerCase()+".adicionarRegistroCadastro();");
		
		f.format("\n\n		System.out.println(\" "+secionCode+": Teste de INCLUS�O SUCESSO executado\");"
				+ "\n		cadastroSucesso = true;");
		
		f.format("\n	}\n");
	}

	private void cadastrarErroTest (Formatter f) {
		
		boolean possuiCampoObrigatorio = false;
		boolean possuiCampoQuePermiteDadosInvalido = false;
		
		for (int i = 0; (i < valorColunaObrigatoria.size() && !possuiCampoObrigatorio ); i++) {
			if (valorColunaObrigatoria.get(i).toUpperCase().equals("YES")) {
				possuiCampoObrigatorio = true;
			}
		}
		
		for (int i = 0; (i < valorColunaTipo.size() && !possuiCampoQuePermiteDadosInvalido ); i++) {
			if ( !(valorColunaTipo.get(i).toUpperCase().equals("SELECT")) && !(valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")) ) {
					possuiCampoQuePermiteDadosInvalido = true;
			}
		}

		if (possuiCampoObrigatorio) testCadastrarComErroNulo(f);
		if (possuiCampoQuePermiteDadosInvalido) testCadastrarComErroInvalido(f);
	}

	private void testCadastrarComErroNulo(Formatter f) {
		String secionCode = dadosTabela.get(colunaSectionCode);
		AssertMensagensErro msgErro = new AssertMensagensErro();
		GeradorValores valores = new GeradorValores();
		
		f.format("\n	@Test"
				+ "\n	public void cadastrarComErro1Nulo() {");
		
		f.format("\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o Teste porque a p�gina n�o foi carregada.\", paginaAcessada);"
				+ "\n		cadastroSucesso = false;");
		
		f.format("\n\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		FuncionalidadesCrud crud = new FuncionalidadesCrud(driver);"
				+ "\n		String mensagemErro;");
		
		f.format("\n		"+sectionCodeMestre+" "+sectionCodeMestre.toLowerCase()+" = new "+sectionCodeMestre+"(driver);");
		if (!tabelaMestre) 
			f.format("\n		"+secionCode+ " "+secionCode.toLowerCase()+" = new "+secionCode+"(driver);");
		
		abrirModal(f);
		
		f.format("\n		"+secionCode.toLowerCase()+".preencherCampos(false, ");
		valores.preencherValoresNulosCadastrar(f);
		f.format(");\n");		
		
		f.format("\n		crud.clicarBotaoSalvarModalCadastro();\n");
		
		msgErro.assertMensagemErroNulo(f, "Cadastro");

		f.format("\n\n		util.fecharMensagem(); // Fechar mensagens de sucesso ou alerta");
		
		f.format("\n\n		System.out.println(\" "+secionCode+": Teste de INCLUS�O DADOS NULOS executado\");"
				+ "\n		cadastroSucesso = true;");
		f.format("\n	}\n");
	}

	private void testCadastrarComErroInvalido(Formatter f) {
		String secionCode = dadosTabela.get(colunaSectionCode);
		GeradorValores valores = new GeradorValores();
		AssertMensagensErro msgErro = new AssertMensagensErro();
		
		f.format("\n	@Test"
				+ "\n	public void cadastrarComErro2Invalido() {");
		
		f.format("\n		assertTrue(\"\\n ATEN��O: N�o � poss�vel executar o Teste porque a p�gina n�o foi carregada.\", paginaAcessada);"
				+ "\n		assertTrue(\"\\n ATEN��O: O teste n�o foi executado porque o teste de cadastro com dados nulos n�o teve sucesso.\", cadastroSucesso);"
				+ "\n		cadastroSucesso = false;");
		
		f.format("\n		"+sectionCodeMestre+" "+sectionCodeMestre.toLowerCase()+" = new "+sectionCodeMestre+"(driver);");
		if (!tabelaMestre) 
			f.format("\n		"+secionCode+ " "+secionCode.toLowerCase()+" = new "+secionCode+"(driver);");
		
		f.format("\n\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		FuncionalidadesCrud crud = new FuncionalidadesCrud(driver);"
				+ "\n		String mensagemErro;");
		
		f.format("\n\n		"+secionCode.toLowerCase()+".preencherCampos(true, ");
		valores.preencherValoresParaCadastro(f, valoresInvalidos);
		f.format(");\n");		
		
		f.format("\n		crud.clicarBotaoSalvarModalCadastro();\n");
		
		msgErro.assertMensagemErroInvalido(f, "Cadastro");

		f.format("\n\n		util.fecharMensagem(); // Fechar mensagens de sucesso ou alerta");
		f.format("\n		util.clicarBotaoCancelarModalCadastrar();");
		f.format("\n\n		System.out.println(\" "+secionCode+": Teste de INCLUS�O DADOS INV�LIDOS executado\");"
				+ "\n		cadastroSucesso = true;"
				+"\n	}\n");
	}
	
	// Fim /////////////////////////////////////////////////////// M�todos TEST ///////////////////////////////////////////////////////////////////
	
	
	// Inicio ////////////////////////////////////////////////// M�todos SOURCER /////////////////////////////////////////////////////////////////
	
	public void adicionarRegistro (Formatter f, String nomeSessao, String nomeSistema) {
		metodoPreencherCampos(f);
		metodoAdicionarRegistroValido(f);
		metodosAdicionar(f, nomeSessao);
	}
	
	private void metodoPreencherCampos (Formatter f) {
		f.format("\n	public void preencherCampos (boolean dadosComErro, ");
		criarParametros(f);
		f.format(") {");
		f.format("\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);\n");

		for (int i=0; i<valorColunaId.size(); i++) {
			
			if (	valorColunaTipo.get(i).toUpperCase().equals("STRING") ||
					valorColunaTipo.get(i).toUpperCase().equals("NUMERIC") ||
					valorColunaTipo.get(i).toUpperCase().equals("INTEGER") ) {
				f.format("\n		driver.findElement(By.id(\""+valorColunaId.get(i)+"\")).sendKeys("+valorColunaId.get(i).toLowerCase()+");");
				
			} else if (valorColunaTipo.get(i).toUpperCase().equals("DATE")) {
				f.format("\n		driver.findElement(By.id(\""+valorColunaId.get(i)+"\")).sendKeys("+valorColunaId.get(i).toLowerCase()+");");
				f.format("\n		driver.findElement(By.id(\""+valorColunaId.get(i)+"\")).sendKeys(Keys.TAB);");
				f.format("\n		if (dadosComErro) {"
						+ "\n			assertTrue( \" ERRO: N�o foi exibido o alert de Erro de formato para o campo '"+valorColunaLabel.get(i)+"'.\", driver.switchTo().alert().getText().contains(\"Erro de formato\"));"
						+ "\n			driver.switchTo().alert().accept();"
						+ "\n		}");
				
			} else if (valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")) {
				f.format("\n		assertTrue(\" ERRO: O checkbox '"+valorColunaLabel.get(i)+"' n�o foi Marcado/Desmarcado.\", util.checkboxMarcarDesmarcarId(\""+valorColunaId.get(i)+"\", "+valorColunaId.get(i).toLowerCase()+"));");
			
			} else if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")) {
				f.format("\n		new Select(driver.findElement(By.id(\""+valorColunaId.get(i)+"\"))).selectByIndex("+valorColunaId.get(i).toLowerCase()+");");
			}
			
			f.format("\n");
		}
		
		f.format("\n	}\n");
	}
	
	// TODO implementar uma valida��o diferente para o segundo registro cadastrado, pois pode ter situa��es que o 2� registro n�o fique na 2� linha
	private void metodoAdicionarRegistroValido(Formatter f) {
		GeradorValores gerador = new GeradorValores();
		
		f.format("\n	private void adicionarRegistro(int nroRegistro, ");
		criarParametros(f);
		f.format(") {");
		f.format( "\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);"
				+ "\n		FuncionalidadesCrud crud = new FuncionalidadesCrud(driver);"
				+ "\n		MensagensRetornadas msg = new MensagensRetornadas();");		
		
		f.format("\n		"+sectionCodeMestre+" "+sectionCodeMestre.toLowerCase()+" = new "+sectionCodeMestre+"(driver);");
		if (!tabelaMestre) {
			f.format("\n		"+dadosTabela.get(colunaSectionCode)+ " "+dadosTabela.get(colunaSectionCode).toLowerCase()+" = new "+dadosTabela.get(colunaSectionCode)+"(driver);");
		}
		
		//Encontrar a posi��o do campo code quando ele existe
		int td = 0;
		for(int i = 0; i < valorColunaId.size(); i++){
			if(valorColunaId.get(i).equals("Code")){
				td = valorColunaId.indexOf(valorColunaId.get(i)) + 2;
				break;
			}
		}
		//Caso ColunaId n�o tenha nenhum Code
		if(td == 0){
			td = 2;
		}
		
		f.format("\n		int numtd = driver.findElements(By.xpath(\".//*[@id='"+sectionCodeMestre+"_grid']/tbody/tr\")).size();");
		f.format("\n\n		ArrayList<String> table = new ArrayList<String>();");
		f.format("\n		for(int i = 2; i <= numtd; i++){	");		
		f.format("\n			table.add(driver.findElement(By.xpath(\".//*[@id='"+sectionCodeMestre+"_grid']/tbody/tr[\"+i+\"]/td["+ td +"]\")).getText());");
		f.format("\n		}");		
		
		
		if (valorColunaId.contains("Code")){
			f.format("\n\n		if(!table.contains(code)){");
		}else{
			f.format("\n\n		if(!table.contains("+valorColunaId.get(0).toLowerCase()+")){");
		}
		
		abrirModal(f);

		// Chama o m�todo para preencher os campos do modal passando os par�metros do m�todo 'adicionarRegistroValido'
		f.format("\n		preencherCampos(false, ");
		for (int i=0; i<valorColunaId.size(); i++) {
			f.format(valorColunaId.get(i).toLowerCase());				
			if ( i<valorColunaId.size()-1 ) {
				f.format(", ");
			}
		}
		f.format(");\n");
		
		gerador.criarVariavelComboboxModal(f);
		
		f.format("\n		crud.clicarBotaoSalvarModalCadastro();");
		f.format("\n		assertTrue(\"\\n ERRO: N�o foi exibido a mensagem '\"+msg.msgSucessoItemAdicionado+\"'. \\n Mensagem retornada: \"+util.pegarMensagemSucesso(), util.pegarMensagemSucesso().equals(msg.msgSucessoItemAdicionado));");
		f.format("\n		util.fecharMensagem();");
		
		f.format("\n\n		validarRegistroTabela(nroRegistro, ");
		passarValoresParaValidar(f);
		f.format(");");
		f.format("\n		}\n");
		f.format("\n	}\n");
	}
	
	// Cria os par�metros de um m�todo conforme o tipo da coluna
	private void criarParametros (Formatter f) {
		
		for (int i=0; i<valorColunaId.size(); i++) {
			
			if (valorColunaTipo.get(i).toUpperCase().equals("STRING")) {
				f.format("String "+valorColunaId.get(i).toLowerCase());				
			}
			else if ( (valorColunaTipo.get(i).toUpperCase().equals("NUMERIC")) || (valorColunaTipo.get(i).equals("INTEGER")) )  {
				f.format("String "+valorColunaId.get(i).toLowerCase());		
			}
			else if (valorColunaTipo.get(i).toUpperCase().equals("DATE")) {
				f.format("String "+valorColunaId.get(i).toLowerCase());					
			}
			else if (valorColunaTipo.get(i).toUpperCase().equals("CHECKBOX")) {
				f.format("String "+valorColunaId.get(i).toLowerCase());	
			}
			else if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")) {
				f.format("int "+valorColunaId.get(i).toLowerCase());	
			}
			if ( i<valorColunaId.size()-1 ) {
				f.format(", ");
			}
		}
	}

	// Preenche os par�metros para serem validados
	private void passarValoresParaValidar (Formatter f) {
		for (int i=0; i<valorColunaId.size(); i++) {
			if (valorColunaTipo.get(i).toUpperCase().equals("SELECT")) {
				f.format("c"+valorColunaId.get(i).toLowerCase());
			} else {
				f.format(valorColunaId.get(i).toLowerCase());
			}
			
			if ( i<valorColunaId.size()-1 ) {
				f.format(", ");
			}
		}
	}
	
	private void metodosAdicionar (Formatter f, String nomeSessao) {
		GeradorValores valores = new GeradorValores();
		
		f.format("\n\n	public void adicionarRegistroCadastro() {");

		if (existeTabelaDependente) {
			f.format("\n		cadastrarDependencias();");
			f.format("\n        "+nomeSessao+"Test " + nomeSessao.toLowerCase() + " = new " + nomeSessao+"Test();");
			f.format("\n        "+nomeSessao.toLowerCase()+".acessarPagina();");
		}
		
		f.format("\n		adicionarRegistro(1, ");
		valores.preencherValoresParaCadastro(f, valoresCadastrados);
		f.format(");");
		f.format("\n	}\n");
		
		f.format("\n	public void adicionarRegistrosFiltros() {");
		f.format("\n		// Adiciona novos registros para valida��o dos filtros (B�sico e Avan�ado)");
		f.format("\n		adicionarRegistro(2, ");
		valores.preencherValoresParaCadastro(f, valoresCadastradosFiltro1);
		f.format(");");
		f.format("\n		adicionarRegistro(3, ");
		valores.preencherValoresParaCadastro(f, valoresCadastradosFiltro2);
		f.format(");");
		f.format("\n	}\n");
		
	}
	
	// Fim ///////////////////////////////////////////////////// M�todos SOURCER /////////////////////////////////////////////////////////////////
	
	private void abrirModal(Formatter f) {
		
		String idTabelaDetalhe = dadosTabela.get(colunaSectionCode).toLowerCase();

		if (tabelaMestre) {
			f.format("\n\n		crud.clicarBotaoNovoMaster(\""+sectionCodeMestre+"\");");
		} else {
			f.format("\n\n		"+sectionCodeMestre.toLowerCase()+".selecionarRegistroTabela();");
			if (nomeAbasIntermediarias.size()>0) {
				f.format("\n	"+idTabelaDetalhe+".selecionarAbaIntermediaria();");
			}
			f.format("\n		"+idTabelaDetalhe+".selecionarAba();");
			f.format("\n		"+idTabelaDetalhe+".clicarBotaoNovoDetalhe();");
		}
		
		f.format("\n\n		util.pegarTituloModal();"
				+"\n		assertTrue(\"\\n ERRO: N�o foi exibido o modal '"+dadosTabela.get(colunaCreateLabel)+"'.\", util.titulo.equals(\""+dadosTabela.get(colunaCreateLabel)+"\"));\n");
	}
}
