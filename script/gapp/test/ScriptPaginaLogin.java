package gapp.test;

import gapp.test.main.AbstractScript;

import java.io.File;
import java.util.Formatter;

public class ScriptPaginaLogin extends AbstractScript {

	private static String diretorioArquivoCriado;
	private static String diretorioSalvarArquivos;
	private static Formatter arquivo;
	private static String localCompletoAbstract;

	public static void criarPaginaLogin (String localArquivo, String nomeSistema) {
		
		criarDiretorios(localArquivo, nomeSistema);
		
		try {
			
			File diretorio = new File(diretorioSalvarArquivos);
			if (!diretorio.exists()) diretorio.mkdir();
			
			diretorioSalvarArquivos = diretorioSalvarArquivos.replaceAll("\\s+", "") + "/";
		    diretorioArquivoCriado = diretorioSalvarArquivos.replace("./", "");
		    
    	    arquivo = new Formatter(diretorioArquivoCriado  + "/PaginaLogin.java");
    	    textoDataCriacao(arquivo);
    	    codigoPaginaLogin(arquivo, nomeSistema);
    	    arquivo.close();
			
    	    localCompletoAbstract = new File(diretorioArquivoCriado).getAbsolutePath();
			logScriptsSucesso += "%nSUCESSO! Arquivo PaginaLogin.java criado em " + localCompletoAbstract;
			qtdeScriptSucesso++;
    	    
		} catch (Exception e) {
			logScriptsErro += "%nERRO! N�o foi criado o arquivo \\scr\\PaginaLogin.java";
			qtdeScriptErro++;
			e.printStackTrace();
		}
	}
	
	private static void criarDiretorios (String localArquivo, String nomeSistema) {
		
		diretorioSalvarArquivos = localArquivo + "/src/";
		criarDiretorio(diretorioSalvarArquivos);
		
	    diretorioSalvarArquivos += "/"+nomeSistema+"/";
		criarDiretorio(diretorioSalvarArquivos);
		
	}

	private static void codigoPaginaLogin (Formatter f, String sistema) {
		f.format("package "+sistema+";");
		f.format("\n");
		f.format("\nimport org.openqa.selenium.By;");
		f.format("\nimport org.openqa.selenium.WebDriver;");
		f.format("\nimport org.openqa.selenium.support.ui.Select;");
		f.format("\n");
		
		f.format("\npublic class PaginaLogin {");
		f.format("\n");
		f.format("\n	private WebDriver driver = null;		");
		f.format("\n");
		
		f.format("\n	public PaginaLogin (WebDriver driver) {"
				+ "\n		this.driver = driver;"
				+ "\n	}"
				+ "\n");
		
		f.format("\n	public void efetuarLoginSucesso (String usuario, String senha, String modulo) {"
				+ "\n		driver.findElement(By.id(\"Login\")).sendKeys(usuario);"
				+ "\n		driver.findElement(By.id(\"Password\")).sendKeys(senha);"
				+ "\n		new Select(driver.findElement(By.id(\"Module\"))).deselectByVisibleText(modulo);"
				+ "\n		driver.findElement(By.cssSelector(\"input[value='Entrar']\")).click();"
				+ "\n	}"
				+ "\n");
		f.format("\n}");
	}
}
