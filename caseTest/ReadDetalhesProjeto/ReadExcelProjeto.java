package ReadDetalhesProjeto;

import gapp.test.main.AbstractScript;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import WriteCT.WriteExcelCasoDeTeste;
import WriteCT.WriteExcelHistorico;
import WriteCT.WriteItensCT;
import WriteCT.WriteItensHistorico;

import BeanCT.HistoricoBean;

public class ReadExcelProjeto extends AbstractScript{
	
	public void readProjetoWriteCT (String diretorioSalvarArquivos, File arquivoDados, String nomeSistema) {
		String prefixSheet = "gapp";
		HistoricoBean historico = new HistoricoBean();		

		try {
			//L� a planilha padr�o
			FileInputStream file = new FileInputStream(new File(arquivoDados.getAbsolutePath()));
			XSSFWorkbook readFileExcel = new XSSFWorkbook(file);
			WriteItensHistorico writeItensHistorico = new WriteItensHistorico();
						
			// Ir� percorrer todas as abas
			for (int i = 0; i < readFileExcel.getNumberOfSheets(); i++) {
				// Abri cada aba
				XSSFSheet sheet = readFileExcel.getSheetAt(i);
				String nomeSheet = sheet.getSheetName();
				// Verificar se � a aba certa
				if (sheet.getSheetName().startsWith(prefixSheet)) {
					int h = 0;
					do {
							int idParametros = 18;//Linha que inicia a lista de tabela de Par�metros
							int qtdeTabDetalhe = 1;								
							try{
									// Ir� criar o arquivo
									XSSFWorkbook writeFileExcel = null;
									writeFileExcel = new XSSFWorkbook();
									
									WriteExcelCasoDeTeste sheetCT = new WriteExcelCasoDeTeste();
									sheetCT.criaSheetCT(writeFileExcel);

									WriteExcelHistorico sheetH = new WriteExcelHistorico();
									sheetH.criaSheetHistorico(writeFileExcel, nomeSistema);

									if(sheet.getRow(h).getCell(colunaSectionCode).getStringCellValue().equals("<Section>")){									
										//Preenche o arquivo com os dados da tabela mestre
										historico.setMenuAcesso(sheet.getRow(h + 2).getCell(colunaPath).getStringCellValue());
										writeItensHistorico.writeItemMenu(writeFileExcel, idParametros,historico.getMenuAcesso());
										idParametros = idParametros + 1; //Passa para a proxima linha
										
										historico.setTabMestre(sheet.getRow(h + 2).getCell(colunaSectionCode).getStringCellValue());
										historico.setTabDetalheIndex(sheet.getRow(h + 2).getCell(colunaIndexLabel).getStringCellValue());
										writeItensHistorico.writeNomeCT(writeFileExcel, historico.getTabDetalheIndex(), nomeSistema);
										writeItensHistorico.writeItemTabMestre(writeFileExcel, idParametros, historico.getTabMestre(), historico.getTabDetalheIndex());			
										idParametros = idParametros + 1; //Passa para a proxima linha
									}
									h++;
									//Preenche o arquivo com os dados das tabelas detalhes
									while (h < sheet.getLastRowNum() &&
											!sheet.getRow(h).getCell(colunaSectionCode).getStringCellValue().equals("<Section>")){
										if (sheet.getRow(h).getCell(colunaSectionCode).getStringCellValue().equals("<DetailSection>") &&
												sheet.getRow(h +2).getCell(colunaDetailOf).getStringCellValue().equals(historico.getTabMestre())){
											//Se houver mais um n�vel de abas, escreve a coluna path sen�o escreve a coluna indexLabel
											if(!sheet.getRow(h +2).getCell(colunaPath).getStringCellValue().equals("")){
												historico.setTabDetalheIndex(sheet.getRow(h + 2).getCell(colunaPath).getStringCellValue());
												historico.setTabDetalheSection(sheet.getRow(h + 2).getCell(colunaSectionCode).getStringCellValue());
												writeItensHistorico.writeItemTabDetalhe(writeFileExcel, idParametros, historico.getTabDetalheSection(), historico.getTabDetalheIndex(), qtdeTabDetalhe);
												idParametros = idParametros + 1; //Passa para a proxima linha	
												qtdeTabDetalhe = qtdeTabDetalhe +1;
											}else{
												historico.setTabDetalheIndex(sheet.getRow(h + 2).getCell(colunaIndexLabel).getStringCellValue());
												historico.setTabDetalheSection(sheet.getRow(h + 2).getCell(colunaSectionCode).getStringCellValue());
												writeItensHistorico.writeItemTabDetalhe(writeFileExcel, idParametros, historico.getTabDetalheSection(), historico.getTabDetalheIndex(), qtdeTabDetalhe);
												idParametros = idParametros + 1; //Passa para a proxima linha	
												qtdeTabDetalhe = qtdeTabDetalhe +1;
											}											
										}	
									h++;
									}	
									
									writeItensHistorico.writeLinhaInferior(writeFileExcel, idParametros);
									
									//Escreve itens da aba Casos de Testes
									int idCT = 5; //Linha que inicia a lista de tabela de CT
									int idTable = 20; //Linha que inicia a lista de tabelas mestre e detalhe
									WriteItensCT writeItensCT = new WriteItensCT();
									writeItensCT.writeDadosCT(writeFileExcel);
									for(int j = 0; j < qtdeTabDetalhe; j++){										
										writeItensCT.writeItensCTAcessar(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTInserirCorreto(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTInserirIncorreto(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTEditarCorreto(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTEditarIncorreto(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTEditarCelulaCorreto(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTEditarCelulaIncorreto(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTRemover(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTFiltrarCorreto(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTFiltrarIncorreto(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTFiltrarVazio(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTLimparFiltro(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTSalvar(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTImportarCorreto(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTImportarIncorreto(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										writeItensCT.writeItensCTExportar(writeFileExcel, idCT, idTable);
										idCT = idCT + 1;
										idTable = idTable + 1;
									}
									writeItensCT.writeBordaInferior(writeFileExcel, idCT);
									
									FileOutputStream out = new FileOutputStream(new File(diretorioSalvarArquivos + "CT-" + nomeSheet +"-"+historico.getTabMestre()+".xlsx"));
									writeFileExcel.write(out);
									out.close();
								} catch (FileNotFoundException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}							
						}while (h < sheet.getLastRowNum());
					}
				}	
			readFileExcel.close();
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
}