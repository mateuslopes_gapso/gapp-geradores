package Formatacoes;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FormatacaoTable {

	public Font fonteNegrito(Workbook workbook) {
		// Formatação da Fonte
		Font fonteCabeçalhoNegrito = workbook.createFont();
		fonteCabeçalhoNegrito.setFontHeight((short) 200);
		fonteCabeçalhoNegrito.setFontName("Calibri");
		fonteCabeçalhoNegrito.setBold(true);

		return fonteCabeçalhoNegrito;
	}

	public Font fonte(Workbook workbook) {
		// Formatação da Fonte
		Font fonteCabeçalho = workbook.createFont();
		fonteCabeçalho.setFontHeight((short) 200);
		fonteCabeçalho.setFontName("Calibri");

		return fonteCabeçalho;
	}

	public CellStyle styleCelulaProjeto(Workbook workbook) {
		// Formatação Celula Nome do Projeto
		CellStyle cellStyleProjeto = workbook.createCellStyle();
		Font fonteProjeto = workbook.createFont();
		fonteProjeto.setColor((short) HSSFColor.BLUE.index);
		fonteProjeto.setFontHeight((short) 240); // Tamanho 12
		fonteProjeto.setFontName("Calibri");
		cellStyleProjeto.setFont(fonteProjeto);
		cellStyleProjeto.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cellStyleProjeto.setVerticalAlignment(CellStyle.VERTICAL_CENTER); // Centraliza na vertical

		return cellStyleProjeto;
	}

	public CellStyle styleCelulaCT(Workbook workbook) {
		// Formatação Celula Caso de Teste
		CellStyle cellStyleCT = workbook.createCellStyle();
		Font fonteCT = workbook.createFont();
		fonteCT.setFontHeight((short) 480); // Tamanho 24
		fonteCT.setFontName("Calibri");
		fonteCT.setBold(true);
		cellStyleCT.setFont(fonteCT);
		cellStyleCT.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		return cellStyleCT;
	}

	public CellStyle styleCelulaFalha(Workbook workbook) {
		// Formatação Celula Falha
		CellStyle cellStyleFalha = workbook.createCellStyle();
		cellStyleFalha.setFont(fonte(workbook));
		cellStyleFalha.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleFalha.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleFalha.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleFalha.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleFalha.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		cellStyleFalha.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleFalha;
	}
	
	public CellStyle styleCelulaNomeCT(Workbook workbook){
		// Formatação Celula Nome do CT
		CellStyle cellStyleNomeCT = workbook.createCellStyle();
		Font fonteNomeCT = workbook.createFont();
		fonteNomeCT.setColor((short) HSSFColor.BLUE.index);
		fonteNomeCT.setFontHeight((short) 240); // Tamanho 12
		fonteNomeCT.setFontName("Calibri");
		fonteNomeCT.setBold(true);
		cellStyleNomeCT.setFont(fonteNomeCT);
		cellStyleNomeCT.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cellStyleNomeCT.setVerticalAlignment(CellStyle.VERTICAL_CENTER); //Centraliza na vertical
		
		return cellStyleNomeCT;
	}
	
	public CellStyle styleBordaRightMediumVerde(XSSFWorkbook workbook){
		//Formatação Cabeçalho da Tabela Centrais
		CellStyle cellStyleTableRight = workbook.createCellStyle();
		cellStyleTableRight.setFont(fonteNegrito(workbook));
		cellStyleTableRight.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cellStyleTableRight.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyleTableRight.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleTableRight.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableRight.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleTableRight.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableRight.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleTableRight.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableRight.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		cellStyleTableRight.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleTableRight.setWrapText(true);//Habilita a quebra de texto
				
		return cellStyleTableRight;
	}
	
	public CellStyle styleFundoVerdeRight(XSSFWorkbook workbook){
		// Formatação celula Cabeçalho
		CellStyle cellStyleCabeçalho = workbook.createCellStyle();
		cellStyleCabeçalho.setFont(fonteNegrito(workbook));
		cellStyleCabeçalho.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		cellStyleCabeçalho.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleCabeçalho.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		
		return cellStyleCabeçalho;
	}
	
	public CellStyle styleAlinhamentoLeft(Workbook workbook){
		// Formatação celula Item Cabeçalho
		CellStyle cellStyleItemCabeçalho = workbook.createCellStyle();
		cellStyleItemCabeçalho.setFont(fonteNegrito(workbook));
		cellStyleItemCabeçalho.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		
		return cellStyleItemCabeçalho;
	}
	
	public CellStyle styleFundoVerdeLeftMedium(XSSFWorkbook workbook){
		//Formatação Cabeçalho da Tabela Esquerdo
		CellStyle cellStyleTableLeft = workbook.createCellStyle();
		cellStyleTableLeft.setFont(fonteNegrito(workbook));
		cellStyleTableLeft.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cellStyleTableLeft.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyleTableLeft.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleTableLeft.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableLeft.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleTableLeft.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableLeft.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		cellStyleTableLeft.setFillPattern(CellStyle.SOLID_FOREGROUND);
		
		return cellStyleTableLeft;		
	}
	
	public CellStyle styleFundoVerdeCentralMedium(XSSFWorkbook workbook){
		//Formatação Cabeçalho da Tabela Centrais
		CellStyle cellStyleTableCenter = workbook.createCellStyle();
		cellStyleTableCenter.setFont(fonteNegrito(workbook));
		cellStyleTableCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cellStyleTableCenter.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyleTableCenter.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleTableCenter.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableCenter.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleTableCenter.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableCenter.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		cellStyleTableCenter.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleTableCenter.setWrapText(true);
				
		return cellStyleTableCenter;
	}
	
	public CellStyle styleFundoVerdeRightMedium(XSSFWorkbook workbook){
		//Formatação Cabeçalho da Tabela Centrais
		CellStyle cellStyleTableRight = workbook.createCellStyle();
		cellStyleTableRight.setFont(fonteNegrito(workbook));
		cellStyleTableRight.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cellStyleTableRight.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyleTableRight.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleTableRight.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableRight.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleTableRight.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableRight.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleTableRight.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableRight.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		cellStyleTableRight.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleTableRight.setWrapText(true);				
		
		return cellStyleTableRight;
	}

	// Formatação com fonte em negrito
	public CellStyle styleLeftNegrito(Workbook workbook, int linha) {
		// Formatação Cabeçalho da Tabela Esquerdo
		CellStyle cellStyleTableLeft = workbook.createCellStyle();
		cellStyleTableLeft.setFont(fonte(workbook));
		cellStyleTableLeft.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleTableLeft.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableLeft.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleTableLeft.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleTableLeft;
	}

	// Formatação com borda esquerda mais espessa
	public CellStyle styleLeftMedium(Workbook workbook, int linha) {
		// Formatação Cabeçalho da Tabela Esquerdo
		CellStyle cellStyleTableLeftMedium = workbook.createCellStyle();
		cellStyleTableLeftMedium.setFont(fonte(workbook));
		cellStyleTableLeftMedium.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyleTableLeftMedium.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleTableLeftMedium.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableLeftMedium.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleTableLeftMedium.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableLeftMedium.setWrapText(true);

		return cellStyleTableLeftMedium;
	}

	public CellStyle styleCentral(Workbook workbook, int linha) {
		// Formatação Cabeçalho da Tabela celulas Centrais
		CellStyle cellStyleTableCentral = workbook.createCellStyle();
		cellStyleTableCentral.setFont(fonte(workbook));
		cellStyleTableCentral.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyleTableCentral.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleTableCentral.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableCentral.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleTableCentral.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableCentral.setWrapText(true);

		return cellStyleTableCentral;
	}
	
	public CellStyle styleCentralParametros(Workbook workbook, int linha) {
		// Formatação Cabeçalho da Tabela celulas Centrais
		CellStyle cellStyleTableCentral = workbook.createCellStyle();
		cellStyleTableCentral.setFont(fonte(workbook));
		cellStyleTableCentral.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyleTableCentral.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleTableCentral.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableCentral.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleTableCentral.setBottomBorderColor(HSSFColor.BLACK.index);
		
		return cellStyleTableCentral;
	}

	// Formatação com borda direita simples
	public CellStyle styleRight(Workbook workbook, int linha) {
		// Formatação Cabeçalho da Tabela Direita
		CellStyle cellStyleTableRight = workbook.createCellStyle();
		cellStyleTableRight.setFont(fonte(workbook));
		cellStyleTableRight.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		cellStyleTableRight.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleTableRight.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableRight.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleTableRight.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableRight.setBorderRight(HSSFCellStyle.BORDER_THIN);
		cellStyleTableRight.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleTableRight;
	}

	// Formatação com borda direita mais espessa
	public CellStyle styleRightMedium(Workbook workbook) {
		// Formatação Cabeçalho da Tabela Direita
		CellStyle cellStyleTableRightMedium = workbook.createCellStyle();
		cellStyleTableRightMedium.setFont(fonte(workbook));
		cellStyleTableRightMedium.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyleTableRightMedium.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleTableRightMedium.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableRightMedium.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleTableRightMedium.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleTableRightMedium.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleTableRightMedium.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleTableRightMedium;
	}

	public CellStyle styleBordaInferior(Workbook workbook) {
		// Borda Inferior
		CellStyle cellStyleBordaInferior = workbook.createCellStyle();
		cellStyleBordaInferior.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleBordaInferior.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleBordaInferior;
	}
	
	public CellStyle styleBordaInferiorMedium(Workbook workbook) {
		// Borda Inferior
		CellStyle cellStyleBordaInferior = workbook.createCellStyle();
		cellStyleBordaInferior.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleBordaInferior.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleBordaInferior;
	}

	public CellStyle styleBordaSuperior(Workbook workbook) {
		// Borda Superior
		CellStyle cellStyleBordaSuperior = workbook.createCellStyle();
		cellStyleBordaSuperior.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleBordaSuperior.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleBordaSuperior;
	}

	public CellStyle styleBordaCentral(XSSFWorkbook workbook) {
		CellStyle cellStyleCentral = workbook.createCellStyle();
		cellStyleCentral.setFont(fonte(workbook));
		cellStyleCentral.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleCentral.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleCentral.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleCentral.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleCentral;
	}

	public CellStyle styleCabecalhoCentralAzulMedium(XSSFWorkbook workbook) {
		CellStyle cellStyleCabecalhoAzul = workbook.createCellStyle();
		cellStyleCabecalhoAzul.setFont(fonteNegrito(workbook));
		cellStyleCabecalhoAzul.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		cellStyleCabecalhoAzul.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleCabecalhoAzul.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cellStyleCabecalhoAzul.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		cellStyleCabecalhoAzul.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleCabecalhoAzul.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleCabecalhoAzul.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleCabecalhoAzul;
	}

	public CellStyle styleCabecalhoLeftAzul(XSSFWorkbook workbook) {
		CellStyle cellStyleCabecalhoLeftAzul = workbook.createCellStyle();
		cellStyleCabecalhoLeftAzul.setFont(fonteNegrito(workbook));
		cellStyleCabecalhoLeftAzul.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		cellStyleCabecalhoLeftAzul.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleCabecalhoLeftAzul.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cellStyleCabecalhoLeftAzul.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleCabecalhoLeftAzul.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleCabecalhoLeftAzul.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleCabecalhoLeftAzul.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleCabecalhoLeftAzul;
	}

	public CellStyle styleCabecalhoRightAzul(XSSFWorkbook workbook) {
		CellStyle cellStyleCabecalhoRightAzul = workbook.createCellStyle();
		cellStyleCabecalhoRightAzul.setFont(fonteNegrito(workbook));
		cellStyleCabecalhoRightAzul.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		cellStyleCabecalhoRightAzul.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleCabecalhoRightAzul.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cellStyleCabecalhoRightAzul.setBorderTop(HSSFCellStyle.BORDER_THIN);
		cellStyleCabecalhoRightAzul.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleCabecalhoRightAzul.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyleCabecalhoRightAzul.setBottomBorderColor(HSSFColor.BLACK.index);
		cellStyleCabecalhoRightAzul.setBorderRight(HSSFCellStyle.BORDER_THIN);
		cellStyleCabecalhoRightAzul.setBottomBorderColor(HSSFColor.BLACK.index);

		return cellStyleCabecalhoRightAzul;
	}

	public CellStyle styleCabecalhoRightAzulCenter(XSSFWorkbook workbook) {
		// Formatação Celula Descrição
		CellStyle styleCabecalhoRightAzulCenter = workbook.createCellStyle();
		styleCabecalhoRightAzulCenter.setFont(fonteNegrito(workbook));
		styleCabecalhoRightAzulCenter.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		styleCabecalhoRightAzulCenter.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleCabecalhoRightAzulCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		styleCabecalhoRightAzulCenter.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		styleCabecalhoRightAzulCenter.setBottomBorderColor(HSSFColor.BLACK.index);
		styleCabecalhoRightAzulCenter.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleCabecalhoRightAzulCenter.setBottomBorderColor(HSSFColor.BLACK.index);
		styleCabecalhoRightAzulCenter.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		styleCabecalhoRightAzulCenter.setBottomBorderColor(HSSFColor.BLACK.index);

		return styleCabecalhoRightAzulCenter;
	}

	public CellStyle styleCabecalhoLeftAzulCenter(XSSFWorkbook workbook) {
		// Formatação Celula Versão
		CellStyle styleCabecalhoLeftAzulCenter = workbook.createCellStyle();
		styleCabecalhoLeftAzulCenter.setFont(fonteNegrito(workbook));
		styleCabecalhoLeftAzulCenter.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		styleCabecalhoLeftAzulCenter.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleCabecalhoLeftAzulCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		styleCabecalhoLeftAzulCenter.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		styleCabecalhoLeftAzulCenter.setBottomBorderColor(HSSFColor.BLACK.index);
		styleCabecalhoLeftAzulCenter.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		styleCabecalhoLeftAzulCenter.setBottomBorderColor(HSSFColor.BLACK.index);

		return styleCabecalhoLeftAzulCenter;
	}
}
