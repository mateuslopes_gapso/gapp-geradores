package gapp.test;

import gapp.test.main.AbstractScript;

import java.io.File;
import java.util.Formatter;

public class ScriptsMain extends AbstractScript {

	public static void criarAbstractTest (String localArquivo, String nomeSistema) {
		
		String diretorioArquivoCriado; //Diret�rio e Nome do arquivo de script criado.
		
		String diretorioSalvarArquivos = localArquivo + "/test/";
	    criarDiretorio(diretorioSalvarArquivos);
	    
	    diretorioSalvarArquivos += "/"+nomeSistema+"/";
		criarDiretorio(diretorioSalvarArquivos);
		
		diretorioSalvarArquivos += "/main/";
	    criarDiretorio(diretorioSalvarArquivos);

    	try {
    		diretorioSalvarArquivos = diretorioSalvarArquivos.replaceAll("\\s+", "") + "/";
    		
    	    diretorioArquivoCriado = diretorioSalvarArquivos.replace("./", "") + "/AbstractTest.java";
    	    Formatter arquivoAbstract = new Formatter(diretorioArquivoCriado);
    	    textoDataCriacao(arquivoAbstract);
    	    codigoAbstractTest(arquivoAbstract, nomeSistema);
    	    arquivoAbstract.close();
    	    
    	    String localCompletoAbstract = new File(diretorioSalvarArquivos).getAbsolutePath();
			logScriptsSucesso += "%n%nSUCESSO! Arquivo AbstractTest.java criado em " + localCompletoAbstract;
			qtdeScriptSucesso++;
    	    
    	} catch (Exception e) {
			e.printStackTrace();
			logScriptsErro += "%nERRO! N�o foi criado o arquivo \\main\\AbsctracTest.java";
			qtdeScriptErro++;
		}
	}
	
	private static void codigoAbstractTest (Formatter f, String sistema) {
		
		f.format("package "+sistema+".main;");
		f.format("\n");
		f.format("\nimport org.junit.AfterClass;");
		f.format("\nimport org.junit.BeforeClass;");
		f.format("\nimport org.openqa.selenium.WebDriver;");
		f.format("\nimport org.openqa.selenium.chrome.ChromeDriver;");
		f.format("\nimport org.openqa.selenium.firefox.FirefoxDriver;");
		f.format("\nimport org.openqa.selenium.remote.DesiredCapabilities;");
		f.format("\n");
		f.format("\nimport "+sistema+".PaginaLogin;");
		f.format("\n");
		f.format("\npublic abstract class AbstractTest {");
		f.format("\n");
		f.format("\n	protected static String usuario = \"admin\";");
		f.format("\n	protected static String senha = \"123mudar\";");
		f.format("\n	protected static String modulo = \"MPT\";");
		f.format("\n	protected static WebDriver driver = null;");
		f.format("\n");
		f.format("\n	@BeforeClass");
		f.format("\n	public static void configuracoes () {");
		f.format("\n");
		f.format("\n		abrirGoogleChorme();");
		f.format("\n");
		f.format("\n		driver.get(\"http://184.106.40.142/LoomaAracruzDev\");");
		f.format("\n		driver.manage().window().maximize();");
		f.format("\n");
		f.format("\n		PaginaLogin pgLogin = new PaginaLogin(driver);");
		f.format("\n		pgLogin.efetuarLoginSucesso(usuario, senha, modulo);");
		f.format("\n	}");
		f.format("\n");
		f.format("\n	@AfterClass");
		f.format("\n	public static void fecharBrowser() {");
		f.format("\n		driver.quit();");
		f.format("\n	}");
		f.format("\n");
		f.format("\n	public static void abrirGoogleChorme () {");
		f.format("\n		DesiredCapabilities cp = DesiredCapabilities.chrome();");
		f.format("\n");		
		f.format("\n		cp.setJavascriptEnabled(true);");
		f.format("\n");
		f.format("\n		// Local do interpretador (server) do Google Chrome");
		f.format("\n		System.setProperty(\"webdriver.chrome.driver\", \"./lib/chromedriver.exe\");");
		f.format("\n");
		f.format("\n		// Local do execut�vel do Google Chrome");
		f.format("\n		cp.setCapability(\"chrome.binary\", \"C:/Program Files (x86)/Google/Chrome/Application/chrome.exe\");");
		f.format("\n");
		f.format("\n		driver = new ChromeDriver(cp);");
		f.format("\n	}");
		f.format("\n");
		f.format("\n	public static void abrirFirefox () {");
		f.format("\n		driver = new FirefoxDriver();");
		f.format("\n	}");
		f.format("\n");
		f.format("\n}");

	}
	
}
