package gapp.funcionalidades;

import gapp.test.main.AbstractScript;

import java.io.File;
import java.util.Formatter;

public class ScriptFuncCRUD extends AbstractScript {

	private static String diretorioArquivoCriado;
	private static String diretorioSalvarArquivos;
	private static Formatter arquivo;
	private static String localCompletoAbstract;
	
	public static void criarFuncionalidadesCrud (String localArquivo, String nomeSistema) {
		
		criarDiretorios(localArquivo, nomeSistema);
		
		try {
			
			File diretorio = new File(diretorioSalvarArquivos);
			if (!diretorio.exists()) diretorio.mkdir();
			
			diretorioSalvarArquivos = diretorioSalvarArquivos.replaceAll("\\s+", "") + "/";
		    diretorioArquivoCriado = diretorioSalvarArquivos.replace("./", "");
			
    	    arquivo = new Formatter(diretorioArquivoCriado  + "/FuncionalidadesCrud.java");
    	    textoDataCriacao(arquivo);
    	    codigoFuncionalidadesCrud(arquivo, nomeSistema);
    	    arquivo.close();
    	    
    	    localCompletoAbstract = new File(diretorioArquivoCriado).getAbsolutePath();
			logScriptsSucesso += "%nSUCESSO! Arquivo FuncionalidadesCrud.java criado em " + localCompletoAbstract;
			qtdeScriptSucesso++;
			
		} catch (Exception e) {
			logScriptsErro += "%nERRO! N�o foi criado o arquivo \\main\\FuncionalidadesUteis.java";
			qtdeScriptErro++;
			e.printStackTrace();
		}
	}

	private static void codigoFuncionalidadesCrud (Formatter f, String sistema) {
		f.format("package "+sistema+";");
		f.format("\n");
		f.format("\nimport org.openqa.selenium.By;"
				+ "\nimport org.openqa.selenium.WebDriver;"
				+ "\n");
		
		f.format("\npublic class FuncionalidadesCrud {"
				+ "\n"
				+ "\n	private WebDriver driver = null;"
				+ "\n");
		
		f.format("\n	public FuncionalidadesCrud (WebDriver driver) {");
		f.format("\n		this.driver = driver;");
		f.format("\n	}");
		f.format("\n");
//		Foi necess�rio colocar par�metro para todos abrirModalMestre funcionarem
		f.format("\n	public void clicarBotaoSalvarMaster (String sectionCodeMestre) {");
//		Existem dois ids btnSaveMaster na tela.
//		f.format("\n		driver.findElement(By.id(\"btnSaveMaster\")).click();");
		f.format("\n		driver.findElement(By.xpath(\".//*[@id='gview_\"+sectionCodeMestre+\"_grid']/div/ul/li/a[@id='btnSaveMaster']/i\")).click();");
		f.format("\n	}");
		f.format("\n");
		
		f.format("\n	public void clicarBotaoNovoMaster (String sectionCodeMestre) {");
		f.format("\n		FuncionalidadesUteis util = new FuncionalidadesUteis(driver);");
		f.format("\n");
//		f.format("\n		util.aguardarElementoId(\"btnNewMaster\");");
//		f.format("\n		driver.findElement(By.id(\"btnNewMaster\")).click();");
		f.format("\n		util.aguardarElementoXPath(\".//*[@id='gview_\"+sectionCodeMestre+\"_grid']/div/ul/li/a[@id='btnNewMaster']/i\");");
		f.format("\n		driver.findElement(By.xpath(\".//*[@id='gview_\"+sectionCodeMestre+\"_grid']/div/ul/li/a[@id='btnNewMaster']/i\")).click();");
		f.format("\n	}");
		f.format("\n");
		
//		f.format("\n	public void clicarBotaoExcluirMaster () {");
//		f.format("\n		driver.findElement(By.id(\"btnDeleleMaster\")).click();");
//		f.format("\n	}");
//		f.format("\n");
//		
//		f.format("\n	public void clicarBotaoFiltrarMaster () {");
//		f.format("\n		driver.findElement(By.id(\"btnFilterMaster\")).click();");
//		f.format("\n	}");
//		f.format("\n");
		
		f.format("\n	public void clicarBotaoImportarMaster () {");
		f.format("\n		driver.findElement(By.id(\"btnImportMaster\")).click();");
		f.format("\n	}");
		f.format("\n");
		
		f.format("\n	public void clicarBotaoExportarMaster () {");
		f.format("\n		driver.findElement(By.id(\"btnExportMaster\")).click();");
		f.format("\n	}");
		f.format("\n");
		
//		f.format("\n	public void clicarBotaoFiltrarDetalhe () {");
//		f.format("\n		driver.findElement(By.id(\"btnFilterDetail\")).click();");
//		f.format("\n	}");
//		f.format("\n");
		
		f.format("\n	public void clicarBotaoImportarDetalhe () {");
		f.format("\n		driver.findElement(By.id(\"btnImportDetail\")).click();");
		f.format("\n	}");
		f.format("\n");
		
		f.format("\n	public void clicarBotaoExportarDetalhe () {");
		f.format("\n		driver.findElement(By.id(\"btnExportDetail\")).click();");
		f.format("\n	}");
		f.format("\n");

		// TODO Atualizar para ID 'okEditAction' ap�s atualiza��o do Looma
		//O id � actionSave �ra cadastro e okEditAction para edi��o. Evelyn
		f.format("\n	public void clicarBotaoSalvarModalCadastro () {"
				+ "\n\n		driver.findElement(By.id(\"actionSave\")).click();"
				+ "\n	}\n");
		
		f.format("\n	public void clicarBotaoSalvarModalEdicao () {"
				+ "\n\n		driver.findElement(By.id(\"okEditAction\")).click();"
				+ "\n	}\n");
		
		f.format("\n}");
	}
	
	private static void criarDiretorios (String localArquivo, String nomeSistema) {
		
		diretorioSalvarArquivos = localArquivo + "/src/";
		criarDiretorio(diretorioSalvarArquivos);
		
	    diretorioSalvarArquivos += "/"+nomeSistema+"/";
		criarDiretorio(diretorioSalvarArquivos);
		
	}
	
}
