package gapp.test;

import gapp.test.main.AbstractScript;

import java.util.Formatter;

public class AssertMensagensErro extends AbstractScript  {

	public void assertMensagemErroNulo (Formatter f, String cadastroEdicao) {
		String label;
		
		f.format("\n		mensagemErro = util.pegarMensagemErro();");
		for (int i = 0; i < valorColunaTipo.size(); i++) {
			if (valorColunaLabelGroupHeader.get(i).isEmpty()) {
				label = valorColunaLabel.get(i).replace("%", "%%"); // TODO remover o IF e deixar apenas este c�digo ap�s corre��o de LOOM-203
			} else {
				label = valorColunaLabelGroupHeader.get(i);
			}
			
			if (valorColunaObrigatoria.get(i).toUpperCase().equals("YES")) {
				if (cadastroEdicao.toUpperCase().equals("CADASTRO")) {
					mensagemErroNulo(f, valorColunaTipo.get(i), label);
				} else {
					if (valorColunaEditavel.get(i).toUpperCase().equals("YES")) {
						mensagemErroNulo(f, valorColunaTipo.get(i), label);
					}
				}
			} else {
				f.format("\n		// O campo '"+label+"' n�o � obrigat�rio.");
			}
		}
	}

	private void mensagemErroNulo(Formatter f, String colunaTipo, String label) {
			colunaTipo = colunaTipo.toUpperCase();
			if ( (colunaTipo.equals("NUMERIC")) || (colunaTipo.equals("DATE")) || (colunaTipo.equals("STRING")) || (colunaTipo.equals("INTEGER")) ) {
				f.format("\n		assertTrue(\"\\n ERRO: N�o foi exibido a mensagem '"+label+": N�o pode ficar em branco'. \\n Mensagem retornada: \\\"\"+mensagemErro+\"\\\"\", mensagemErro.contains(\""+label+": N�o pode ficar em branco\"));");
			} else if (colunaTipo.equals("SELECT")) {
				f.format("\n		assertTrue(\"\\n ERRO: N�o foi exibido a mensagem '"+label+": N�o pode ficar em branco'. \\n Mensagem retornada: \\\"\"+mensagemErro+\"\\\"\", mensagemErro.contains(\""+label+": N�o pode ficar em branco\")); // TODO � um campo do tipo SELECT. Verificar se vem preenchido por padr�o.");
			} else {
				f.format("\n		// O campo '"+label+"' vem preenchido por padr�o, n�o � poss�vel ser nulo ou n�o � edit�vel.");
			}
	}
	
	// Valida os campos inv�lidos da tabela, exceto String e Numeric
	public void assertMensagemErroInvalidoTabela (Formatter f, String cadastroEdicao) {
		String[] regra = null;
		String label;
		String textoParaValidar;
		
		f.format("\n		mensagemErro = util.pegarMensagemErro();"); // Utilizado para fazer os asserts
		for (int i = 0; i < valorColunaDeRegra.size(); i++) {
			textoParaValidar = "X"; // Ser� atualizado caso tenha regra
			
			if (valorColunaLabelGroupHeader.get(i).isEmpty()) {
				label = valorColunaLabel.get(i).replace("%", "%%"); // TODO remover o IF e deixar apenas este c�digo ap�s corre��o de LOOM-203
			} else {
				label = valorColunaLabelGroupHeader.get(i);
			}
			
			if (!valorColunaDeRegra.get(i).isEmpty()) { // TODO verificar est� sendo utilizado
				regra = valorColunaDeRegra.get(i).split("#");
//				if (regra[0].equals("string")) {
//					textoParaValidar = "O tamanho do campo "+label+" deve ser menor que "+regra[2];
//				} else {
//					if (regra.length == 2) {
//						textoParaValidar = "O valor do campo "+label+" n�o pode ser menor que "+regra[1];
//					} else if (regra.length == 3) {
//						textoParaValidar = "O valor do campo "+label+" n�o pode ser maior que "+regra[2];
//					}
//				}
				if (valorColunaTipo.get(i).toUpperCase().equals("INTEGER")){
					textoParaValidar = valorColunaLabel.get(i)+": Formato inv�lido";
				}
			}
			
			if (cadastroEdicao.toUpperCase().equals("CADASTRO")) {
				mensagemErroInvalido(f, valorColunaTipo.get(i), label, textoParaValidar);
			} else {
				if (valorColunaEditavel.get(i).toUpperCase().equals("YES")) {
					mensagemErroInvalido(f, valorColunaTipo.get(i), label, textoParaValidar);
				} else {
					f.format("\n		// O campo '"+label+"' n�o � edit�vel.");
				}
			}
		}
	}

	public void assertMensagemErroInvalido (Formatter f, String cadastroEdicao) {
		String[] regra = null;
		String label;
		String textoParaValidar;
		
		f.format("\n		mensagemErro = util.pegarMensagemErro();");
		for (int i = 0; i < valorColunaDeRegra.size(); i++) {
			textoParaValidar = "X"; // Ser� atualizado caso tenha regra
			
			if (valorColunaLabelGroupHeader.get(i).isEmpty()) {
				label = valorColunaLabel.get(i).replace("%", "%%"); // TODO remover o IF e deixar apenas este c�digo ap�s corre��o de LOOM-203
			} else {
				label = valorColunaLabelGroupHeader.get(i);
			}
			
			if (!valorColunaDeRegra.get(i).isEmpty()) {
				regra = valorColunaDeRegra.get(i).split("#");
				if (regra[0].equals("string")) {
					textoParaValidar = "O tamanho do campo "+label+" deve ser menor que "+regra[2];
				} else {
					if (regra.length == 2) {
						textoParaValidar = "O valor do campo "+label+" n�o pode ser menor que "+regra[1];
					} else if (regra.length == 3) {
						textoParaValidar = "O valor do campo "+label+" n�o pode ser maior que "+regra[2];
					}
				}
			} else if ( (valorColunaTipo.get(i).toUpperCase().equals("NUMERIC")) || (valorColunaTipo.get(i).toUpperCase().equals("INTEGER"))  ) {
				textoParaValidar = valorColunaLabel.get(i)+": Formato inv�lido";
			}
			
			if (cadastroEdicao.toUpperCase().equals("CADASTRO")) {
				mensagemErroInvalido(f, valorColunaTipo.get(i), label, textoParaValidar);
			} else {
				if (valorColunaEditavel.get(i).toUpperCase().equals("YES")) {
					mensagemErroInvalido(f, valorColunaTipo.get(i), label, textoParaValidar);
				} else {
					f.format("\n		// O campo '"+label+"' n�o � edit�vel.");
				}
			}
		}
	}

	private void mensagemErroInvalido(Formatter f, String tipoColuna, String label, String textoParaValidar) {
		tipoColuna = tipoColuna.toUpperCase();
		if (textoParaValidar.equals("X")) {
			if (tipoColuna.toUpperCase().equals("CHECKBOX")) {
				f.format("\n		// O campo '"+label+"' � um checkbox.");
			} else if (tipoColuna.toUpperCase().equals("SELECT")) {
				f.format("\n		// O campo '"+label+"' � um Combobox, s� aceita valores pr�-selecionados.");
			} else if (tipoColuna.equals("DATE")) {
				f.format("\n		// A valor do campo '"+label+"' � validado durante o preenchimento.");
			} else {
				f.format("\n		// assertTrue(\"\\n ERRO: N�o foi exibido a mensagem 'TEXTO_PARA_VALIDAR'.\", mensagemErro.contains(\"TEXTO_PARA_VALIDAR\")); // TODO Cadastrar uma regra para o campo '"+label+"'");
			}
		} else {
			f.format("\n		assertTrue(\"\\n ERRO: N�o foi exibido a mensagem '"+textoParaValidar+"'.\", mensagemErro.contains(\""+textoParaValidar+"\"));");
		} 
	}
}
