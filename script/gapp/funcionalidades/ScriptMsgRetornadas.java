package gapp.funcionalidades;

import gapp.test.main.AbstractScript;

import java.io.File;
import java.util.Formatter;

public class ScriptMsgRetornadas extends AbstractScript {

	private static String diretorioArquivoCriado;
	private static String diretorioSalvarArquivos;
	private static Formatter arquivo;
	private static String localCompletoAbstract;
	
	public static void criarMensagensRetornadas (String localArquivo, String nomeSistema) {
		
		criarDiretorios(localArquivo, nomeSistema);

		try {
			
			diretorioSalvarArquivos = diretorioSalvarArquivos.replaceAll("\\s+", "") + "/";
		    diretorioArquivoCriado = diretorioSalvarArquivos.replace("./", "");
			
    	    arquivo = new Formatter(diretorioArquivoCriado + "/MensagensRetornadas.java");
    	    textoDataCriacao(arquivo);
    	    codigoMensagensRetornadas(arquivo, nomeSistema);
    	    arquivo.close();
    	    
    	    localCompletoAbstract = new File(diretorioArquivoCriado).getAbsolutePath();
			logScriptsSucesso += "%nSUCESSO! Arquivo MensagensRetornadas.java criado em " + localCompletoAbstract;
			qtdeScriptSucesso++;
    	    
		} catch (Exception e) {
			logScriptsErro += "%nERRO! N�o foi criado o arquivo \\scr\\MensagensRetornadas.java";
			qtdeScriptErro++;
			e.printStackTrace();
		}
	}
	
	private static void codigoMensagensRetornadas (Formatter f, String sistema) {
		f.format("package "+sistema+";");
		
		f.format("\n	public class MensagensRetornadas {"
				+ "\n		public String msgSucessoItemAdicionado = \"Item adicionado com sucesso\";"
				+ "\n		public String msgSucessoItemRemovido = \"Item removido com sucesso\";"
				+ "\n		public String msgSucessoItemAtualizado = \"Item atualizado com sucesso\";"
				+ "\n		public String msgConfirmacaoRemover = \"Tem certeza que deseja remover este registro? Aten��o! Todos os dados relacionados a ele tamb�m ser�o removidos.\";"
				+ "\n	}");
	}
	
	
	private static void criarDiretorios (String localArquivo, String nomeSistema) {
		
		diretorioSalvarArquivos = localArquivo + "/src/";
		criarDiretorio(diretorioSalvarArquivos);
		
	    diretorioSalvarArquivos += "/"+nomeSistema+"/";
		criarDiretorio(diretorioSalvarArquivos);
		
	}
}
